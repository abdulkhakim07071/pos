<?php

namespace App\Http\Controllers\BackOffice;

use Illuminate\Http\Request;
use Kreait\Firebase\Contract\Auth;
use App\Http\Controllers\Controller;
use Kreait\Firebase\Contract\Database;
// use Kreait\Firebase\Database;



class KaryawanController extends Controller
{

    protected $database;
    protected $tablename;
    protected $auth;
    protected $users;

    public function __construct(Database $database, Auth  $auth)
    {
        
        $this->database = $database;
        $this->tablename = 'employee';
        $this->auth = $auth;
        $this->users = 'users';


    }
    
    
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $url = $this->backOfficeCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();

        $employee = $this->database->getReference($this->users)->getValue();
        return view('BackOffice.HakAkses.Index',[
            'title' => 'Daftar Karywan',
            'active'=> 'Daftar Karywan'
            ], compact('employee', 'userData'));
    }

    public function formTambahKaryawan(){
        $url = $this->backOfficeCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();


        return view('BackOffice.HakAkses.Create',[
            'title' => 'Tambah Karywan',
            'active'=> 'Daftar Karywan'
            ], compact('userData'));
    }
    
public function store(Request $request)
{
    $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();

    try{
        $userProperties = [
            'email' => $request->email,
            'password' => $request->password,
        ];
        
        $createdUser = $this->auth->createUser($userProperties);
            $userData = [
                'idAuth'=> $createdUser->uid,
                'name' => $request->name,
                'noTelphone' => $request->HP,
                'email' => $request->email,
                'nameStore' => $userData['nameStore'],
                'alamat' => $userData['alamat'],
                'role' => $request->role,
            ];
        $this->database->getReference('users/' . $createdUser->uid)->set($userData);


        if($request->management){
            return redirect('management-karyawan')->with('status','Karyawan berhasil di tambahkan');
        }else{
            return redirect('daftar-karyawan')->with('status','Karyawan berhasil di tambahkan');
        }
    }catch(\Exception $e){
        if($request->management){
            return redirect('input-karyawan')->with('status','Karyawan Gagal di tambahkan');
        }else{
            return redirect('tambah-karyawan')->with('status', $e->getMessage());
        }
    };
    

    

      

}
public function edit($id)
    {
        $url = $this->backOfficeCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };

         // Mengambil data produk dari Firebase Realtime Database
         $reference = $this->database->getReference($this->users. '/' .  $id);
         $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();

        //  $auth = $this->database->getReference($this->auth. '/' .  session('user_id'))->getValue();
         $snapshot = $reference->getSnapshot();
 
         if (!$snapshot->exists()) {
             // Handle jika produk tidak ditemukan
             return abort(404);
         }
 
         $karyawan = $snapshot->getValue();
        //  dd($produk);
         return view('BackOffice.HakAkses.Edit',[
            'title' => 'Hak Akses Edit',
            'active'=> 'Hak Akses',
            
            ], compact('karyawan','id', 'userData',) ); 
 
    }
public function update(Request $request, string $id)
{
    if(!isset($request->password)){
        $user= [
            'email' => $request->email,
        ];
    }else{
        $user= [
            'email' => $request->email,
            'password' => $request->password,
        ];
    }

    
    $updatedUser = $this->auth->updateUser($id, $user);




    // Data karyawan
    $dataKaryawan = [
        'name' => $request->name,
        'email' => $request->email,
        'noTelphone' => $request->HP,
        'role' => $request->role,
    ];

    

       $postData= $this->database->getReference($this->users . '/' . $id)->update($dataKaryawan);
         if($postData){
            if($request->management){
                return redirect('management-karyawan')->with('status','Karyawan berhasil di tambahkan');
            }else{
                return redirect('daftar-karyawan')->with('status','Karyawan berhasil di tambahkan');
            }
        }
   

}

    
    public function delete(string $id )
    {
        try{

            $this->auth->deleteUser($id);
            $postData = $this->database->getReference($this->users . '/' . $id)->remove();
            return redirect('daftar-karyawan')->with('status', 'Karyawan berhasil dihapus');
        }catch(\Exception $e){
            return redirect('daftar-karyawan')->with('status', 'Karyawan gagal dihapus');
        }


    }
    /**
     * Store a newly created resource in storage.
     */
    

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
    }

    /**
     * Update the specified resource in storage.
     */
    

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
