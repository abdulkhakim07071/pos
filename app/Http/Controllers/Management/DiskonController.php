<?php

namespace App\Http\Controllers\Management;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kreait\Firebase\Contract\Database;

class DiskonController extends Controller
{
    protected $database;
    protected $tablediskon;
    protected $users;
    public function __construct(Database $database)
    {
        $this->database = $database;
        $this->tablediskon = 'discount';
        $this->users = 'users';
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $url = $this->managementCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();

        $discounts = $this->database->getReference($this->tablediskon)->getValue();

        return view('Management.Diskon.Index',[
            'title' => 'Diskon',
            'active'=> 'Diskon'
            ],compact('discounts', 'userData')); 
    }
    public function formTambahDiskon()
    {
        $url = $this->managementCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();

        return view('Management.Diskon.Create',[
            'title' => 'Tambah Diskon',
            'active'=> 'Tambah Diskon'
            ],compact('userData')); 
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
       
        $data= [
            'name' => $request->name,
            'type' => $request->type,
            'nilai' => $request->nilai
        ];
        $postData =$this->database->getReference($this->tablediskon)->push($data);
        
        if($postData){
            return redirect('diskon')->with('status','Diskon berhasil di tambahkan');
        }else{
            return redirect('tambah.diskon')->with('status','Diskon Gagal di tambahkan');
        }   
    }

    public function edit($id)
    {
      
        $url = $this->managementCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };  $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();


         // Mengambil data produk dari Firebase Realtime Database
         $reference = $this->database->getReference('discount/' . $id);
         $snapshot = $reference->getSnapshot();
 
        //  if (!$snapshot->exists()) {
        //      return abort(404);
        //  }
 
         $discount = $snapshot->getValue();
        // dd($produk);
         return view('Management.Diskon.Edit',[
            'title' => 'Edit Diskon',
            'active'=> 'Diskon',
            ], compact('discount','id', 'userData') ); 
 
    }
    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $data= [
            'name' => $request->name,
            'type' => $request->type,
            'nilai' => $request->nilai
        ];
        $postData = $this->database->getReference($this->tablediskon . '/' . $id)->update($data);
        
        if($postData){
            return redirect('diskon')->with('status','Produk berhasil di tambahkan');
        }else{
            return redirect('discount.edit')->with('status','Produk Gagal di tambahkan');
        }   
    }
    public function delete(string $id )
    {
       

        // Append $id to the reference path
        $postData = $this->database->getReference($this->tablediskon . '/' . $id)->remove();

        if ($postData){
            return redirect('diskon')->with('status', 'Diskon berhasil dihapus');
        } else {
            return redirect('diskon')->with('status', 'Diskon gagal dihapus');
        }
    }
    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
