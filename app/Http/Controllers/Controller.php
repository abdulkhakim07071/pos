<?php

namespace App\Http\Controllers;

use Closure;
use Carbon\Carbon;
use Kreait\Firebase\Contract\Auth;
use Kreait\Firebase\Contract\Database;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, ValidatesRequests;
    protected $database;
    protected $users;
    protected $shift;
    protected $auth;
    

    public function __construct(Database  $database,  Auth  $auth)
    {
        
        $this->database = $database;
        $this->users = 'users';       
        $this->shift = 'shift';       
        $this->auth = $auth;

    }
    
   public function findShift($id, $tanggal, $time){
    $shift = $this->database->getReference($this->shift)->getValue();

    foreach ($shift as $shiftId => $shiftData) {
        // Jika ID pengguna cocok dengan ID pengguna yang sedang diautentikasi
        if ($shiftData['id'] == $id &&  $shiftData['tanggal'] == $tanggal && $shiftData['login'] == $time) {
            return $shiftId;
        }
    }
    return $shiftId = null;
   }
   function findShiftLogin($timestamp, $id){
    $timestampUser = strtotime($timestamp);
                    // Memisahkan tanggal
    $tanggal = date('Y-m-d', $timestampUser);
    $time = date('H:i:s', $timestampUser);

    $shiftId = $this->findShift($id , $tanggal, $time);
    if($tanggal == Carbon::now()->toDateString() && isset($shiftId) ){
        $shiftDetail = $this->database->getReference($this->shift. '/' .  $shiftId)->getValue();
        return $shiftDetail;
    }else if($tanggal != Carbon::now()->toDateString() && isset($shiftId)){
        $login =[
            'lastLogin' => null,
        ];
        $this->database->getReference($this->users. '/' . $id)->update($login);
    }
    return null;

   }
   public function handle()
   {
       $firebaseUid = session('user_id');
       if (!$firebaseUid) {
           return redirect('/login-kasir')->with('status', 'Unauthorized');
       }

   }
   public function backOfficeCheck()
    {
        
        $firebaseUid = session('user_id') ?? null;
        if (!$firebaseUid) {
            $url = 'login-kasir';
            return $url;
        }
        $userRole = session('role');
        
        
        // Periksa peran pengguna
        if ($userRole == 'POS') {
            $url = 'kasir-transaksi';
            return $url;
        }
        if ($userRole == 'Management') {
            $url = 'produk';

            return $url;
        }

        return true;

        // Logika untuk rute 'ringkasan-penjualan' hanya untuk peran 'Backoffice'
    }
   public function managementCheck()
    {
        $firebaseUid = session('user_id') ?? null;
        if (!$firebaseUid) {
            $url = 'login-kasir';

            return $url;
        }
        $userRole = session('role');

        // Periksa peran pengguna
        if ($userRole == 'POS') {
            $url = 'kasir-transaksi';

            return $url;
        }
        if ($userRole == 'Backoffice') {
            $url = 'ringkasan-penjualan';

            return $url;
        }
        return true;


    }
   public function kasirCheck()
    {
        $firebaseUid = session('user_id') ?? null;
        if (!$firebaseUid) {
            $url = 'login-kasir';
            return $url;
        }
        $userRole = session('role');

        // Periksa peran pengguna
        if ($userRole == 'Management') {
            $url = 'produk';
            return $url;
        }
        if ($userRole == 'Backoffice') {
            $url = 'ringkasan-penjualan';
            return $url;
        }
        return true;


    }
   public function loginCheck()
    {
        $firebaseUid = session('user_id') ?? null;
        $userRole = session('role');
        if ($firebaseUid) {
            if ($userRole == 'Management') {
                $url = 'produk';
                return $url;
            }
            if ($userRole == 'Backoffice') {
                $url = 'ringkasan-penjualan';
                return $url;
            }
            if ($userRole == 'POS') {
                $url = 'kasir-transaksi';
                return $url;
            }
        }

        // Periksa peran pengguna
        return true;


    }
}
