@extends('BackOffice.Layouts.Main')

@section('container')


<div class="content-wrapper">

    <!-- Main content -->
    <section class="content">
        {{-- filter --}}
        <!-- /.form group -->
        <form action="{{ route('laporantanggalbarang.index') }}" method="GET">
          <div class="row text-center">
            <div class="col-lg-3 col-4">
              <br>
              <div class="input-group date">
                  <input id="start-datepicker" type="date" class="form-control" placeholder="Pilih Tanggal Awal" name="startDate">
              </div>
          </div>
          <div class="col-lg-3 col-4">
              <br>
              <div class="input-group date">
                  <input id="end-datepicker" type="date" class="form-control" placeholder="Pilih Tanggal Akhir" name="endDate">
              </div>
          </div>
          <div class="col-lg-3 col-4">
              <br>
              <button type="submit" id="filter-button" class="btn btn-primary">Filter</button>
              <a href="{{ route('laporantanggalbarang.index') }}" class="btn btn-danger">Reset Filter</a>
          </div>
        </form>
          <!-- /.col -->
          {{-- <div class="col-lg-3 col-3">
            <br>
            
              <a href="{{ route('laporan.index') }}" class="btn btn-danger">Reset Filter</a>
          </div> --}}
        </div>
        {{-- end filter --}}


          {{-- chart  --}}
          <!-- Left col -->
          <section class="Chart" style="padding-top: 15px">
            {{-- <div class="row"> --}}
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
              <div class="row">
              <div class="col-lg-4 col-6 ">

                <div class="card-header">
                  <h3 class="card-title">
                    Penjualan Teratas
                  </h3>
                  <div class="card-tools">
                    <ul class="nav nav-pills ml-auto">
                      <li class="nav-item">
                        <a class="nav-link">Penjualan Bersih</a>
                      </li>
                    </ul>
                  </div>
                </div><!-- /.card-header -->
                <div class="card-body">
                  @foreach($topSales as $product)
                  <div class="row">
                      <div class="col">
                          {{ $product['name'] }} <!-- Nama Produk -->
                      </div>
                      <div class="col-4">
                        <?php
                        $penjualanBersih = 0;
                        foreach ($transaction as $item) {
                            foreach ($item['products'] as $transProduct) {
                                if ($transProduct['name'] === $product['name']) {
                                    $penjualanBersih += $transProduct['subtotal']; //* $transProduct['kuantitas'] 
                                }
                            }
                        }
                      ?>
                       Rp. {{ number_format($penjualanBersih, 2) }}
                      </div>
                  </div>
                  @endforeach
                </div><!-- /.card-body -->
              </div>
              <div class="col-lg-8 col-6 ">

                <div class="card-header">
                  <h3 class="card-title">
                    <i class="fas fa-chart-pie mr-1"></i>
                    Sales
                  </h3>
                  <div class="card-tools">
                    <ul class="nav nav-pills ml-auto">
                      <li class="nav-item">
                        <a class="nav-link active" href="#revenue-chart" data-toggle="tab">Area</a>
                      </li>
                    </ul>
                  </div>
                </div><!-- /.card-header -->
                <div class="card-body">
                  
                    <div class="tab-content p-0">
                      <!-- Morris chart - Sales -->
                      <div class="chart tab-pane active" id="revenue-chart" style="position: relative; width: 100%; height: 300px;">
                        <canvas id="transactionChart" style="width: 100%; height: 100%;"></canvas>
                    </div>
                      <div class="chart tab-pane" id="sales-chart" style="position: relative; height: 300px;">
                        <canvas id="sales-chart-canvas" height="300" style="height: 300px;"></canvas>
                      </div>
                    </div>
                </div><!-- /.card-body -->
              </div>
              </div>
            </div>
            <!-- /.card -->

            

          {{-- </div> --}}
          </section>
         
          <!-- right col -->
        {{-- end chart --}}

        <div class="row">
          <div class="col-12">
            

            <div class="card">
              <div class="card-header">
                <h3 class="card-title">EKSPOR</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="example1" class="table table-bordered table-striped">
                  <thead>
                  <tr>
                    <th>Barang</th>
                    <th>Barang Terjual</th>
                    <th>Penjualan Bersih</th>
                    <th>Harga Pokok</th>
                    <th>Laba Kotor</th>
                  </tr>
                  </thead>
                  <tbody>
                    @foreach($products as $product)
                  <tr>
                    <td>{{ $product['name'] }}</td>
                    <td>
                      <!-- Hitung jumlah barang terjual-->
                      <?php
                      $jumlahBarangTerjual = 0;
                      foreach ($transaction as $item) {
                          foreach ($item['products'] as $transProduct) {
                              if ($transProduct['name'] === $product['name']) {
                                  $jumlahBarangTerjual += $transProduct['kuantitas'];
                              }
                          }
                      }
                      echo $jumlahBarangTerjual;
                      ?>
                    </td>
                    <td>
                      <?php
                        $penjualanBersih = 0;
                        foreach ($transaction as $item) {
                            foreach ($item['products'] as $transProduct) {
                                if ($transProduct['name'] === $product['name']) {
                                    $penjualanBersih += $transProduct['subtotal']; //* $transProduct['kuantitas'] 
                                }
                            }
                        }
                        echo $penjualanBersih;
                      ?>
                    </td>
                    <td>
                      <?php
                      $HargaPokok = 0;
                      foreach ($transaction as $item) {
                          foreach ($item['products'] as $transProduct) {
                              if ($transProduct['name'] === $product['name']) {
                                  $HargaPokok += $transProduct['price']; //* $transProduct['kuantitas'] 
                              }
                          }
                      }
                      echo $HargaPokok;
                    ?>
                    </td>
                    <td>
                      <?php
                      $labaKotor = $penjualanBersih - $HargaPokok;
                      echo $labaKotor;
                      ?>
                    </td>
                  </tr>
                    @endforeach
                  </tbody>

                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      
    </section>
    <!-- /.content -->
  </div>

 
 

@endsection

@section('script')
<script>
  $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": [ "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    
  });
</script>

<script>

 
$(function () {
  //Initialize Select2 Elements
  $('.select2').select2()

  //Initialize Select2 Elements
  $('.select2bs4').select2({
    theme: 'bootstrap4'
  })

 

  //Date picker
  $('#reservationdate').datetimepicker({
      format: 'L'
  });

  //Date and time picker
  $('#reservationdatetime').datetimepicker({ icons: { time: 'far fa-clock' } });

  //Date range picker
  $('#reservation').daterangepicker()
  //Date range picker with time picker
  $('#reservationtime').daterangepicker({
    timePicker: true,
    timePickerIncrement: 30,
    locale: {
      format: 'MM/DD/YYYY hh:mm A'
    }
  })
  //Date range as a button
  $('#daterange-btn').daterangepicker(
    {
      ranges   : {
        'Today'       : [moment(), moment()],
        'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
        'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
        'This Month'  : [moment().startOf('month'), moment().endOf('month')],
        'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
      },
      startDate: moment().subtract(29, 'days'),
      endDate  : moment()
    },
    function (start, end) {
      $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
    }
  )

  //Timepicker
  $('#timepickerstart').datetimepicker({
    format: 'LT'
  })
  //Timepicker
  $('#timepickerend').datetimepicker({
    format: 'LT'
  })

  //Bootstrap Duallistbox
  $('.duallistbox').bootstrapDualListbox()

  //Colorpicker
  $('.my-colorpicker1').colorpicker()
  //color picker with addon
  $('.my-colorpicker2').colorpicker()

  $('.my-colorpicker2').on('colorpickerChange', function(event) {
    $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
  })

  $("input[data-bootstrap-switch]").each(function(){
    $(this).bootstrapSwitch('state', $(this).prop('checked'));
  })

})

</script>

{{-- CHART --}}

<script>
  document.addEventListener("DOMContentLoaded", function() {
      var ctx = document.getElementById('transactionChart').getContext('2d');
      var transactionChart = new Chart(ctx, {
          type: 'line',
          data: {
              labels: {!! $labels !!},
              datasets: {!! $datasets !!}
          },
          options: {
              scales: {
                  yAxes: [{
                      ticks: {
                          beginAtZero: true
                      }
                  }]
              }
          }
      });
  });
</script>

{{-- <script>
  // Tambahkan event listener untuk tombol reset filter
  document.getElementById("reset-filter-button").addEventListener("click", function() {
      // Redirect kembali ke halaman semula
      window.location.href = "{{ route('laporan.index') }}";
  });
</script> --}}
@endsection
