@extends('Management.Layouts.Main')



@section('container')

<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4 col-8">
                <br>
                @if(session('status'))
                <h5 class="alert alert-warning mb-3">{{ session('status') }}</h5>
                @endif
                <div class="card">
                    <div class="card-header text-center">
                        <br>
                        <br><br>
                        <svg xmlns="http://www.w3.org/2000/svg" height="46" width="46" viewBox="0 0 512 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2023 Fonticons, Inc.--><path d="M345 39.1L472.8 168.4c52.4 53 52.4 138.2 0 191.2L360.8 472.9c-9.3 9.4-24.5 9.5-33.9 .2s-9.5-24.5-.2-33.9L438.6 325.9c33.9-34.3 33.9-89.4 0-123.7L310.9 72.9c-9.3-9.4-9.2-24.6 .2-33.9s24.6-9.2 33.9 .2zM0 229.5V80C0 53.5 21.5 32 48 32H197.5c17 0 33.3 6.7 45.3 18.7l168 168c25 25 25 65.5 0 90.5L277.3 442.7c-25 25-65.5 25-90.5 0l-168-168C6.7 262.7 0 246.5 0 229.5zM144 144a32 32 0 1 0 -64 0 32 32 0 1 0 64 0z"/></svg>
                        <br>
                        <br>
                        <br>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('discount.update', ['id' => $id]) }}" method="POST">
                            @csrf
                        <div class="form-floating mb-3 row">
                            <label for="name" class="font">Nama</label>
                            <input type="text" class="form-control" id="name" value="{{ $discount['name'] }}" name="name" placeholder="Masukkan Nama Diskon" required>
                        </div>
                        <div class="row mb-3">
                            <div class="col-lg-4 col-12">

                                <p>Jenis</p>
                            </div>
                            <div class="col-lg-4 col-6">
                                <input class="form-check-input" type="radio" name="type" @if($discount['type'] == 'Persent') checked @endif value="Persent" id="percent" value="Persen">
                                <label class="form-check-label" for="percent">
                                  Persentase
                                </label>
                            </div>
                            <div class="col-lg-4 col-6">
                                <input class="form-check-input" type="radio" name="type" @if($discount['type'] == 'jumlah') checked @endif value="jumlah" id="jumlah" value="Jumlah">
                                <label class="form-check-label" for="jumlah">
                                  Jumlah
                                </label>
                            </div>
                          
                        </div>
                        <div class="form-floating mb-3 row">
                            <label for="name" class="font">Nilai</label>
                            <input type="number" class="form-control" id="name" name="nilai" value="{{ $discount['nilai'] }}" placeholder="Masukkan Nilai" required>
                        </div> 
                        <div class="row">
                            <br>
                            <div class="col-lg-8 ml-auto">
                                <div class="row">
                                    <div class="col">
                                        <button class="btn-utama"> Simpan </button>
                                    </div>
                                </form>
                                    <div class="col">
                                        <button href="{{ route('diskon') }}" class="btn-utama"> Batal </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection