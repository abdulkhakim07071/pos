@extends('BackOffice.Layouts.Main')

@section('container')
<div class="content-wrapper" style="font-family: Poppins;">
    <div class="container-fluid">
        <section class="content">
            <div class="row">
                <div class="col">
                    <br>
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">
                                <h2 style="font-weight:bold; ">Data Transaksi</h2>
                            </div>
                        </div>
                        <div class="card-body">
                            <table id="tabelBarang" class="table table-striped" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Tanggal</th>
                                    <th>Diskon</th>
                                    <th>Pajak</th>
                                    <th>Total Bayar</th>
                                </tr>
                            </thead>
                            <tbody>
                                
                                {{-- {{ dd($transaksi) }} --}}
                                @if(isset($transaksi))
                                    @foreach($transaksi AS $transaksi => $item)
                                    @if($item['status'] == "Lunas")
                                        <tr>
                                            <td>{{ $item['tanggal'] }}</td>
                            
                                            @if (isset($item['discount']))
                                                @foreach ($item['discount'] as $key => $list)
                                                <td>{{ $list['nominal'] }}</td>
                                                @endforeach 
                                            @else
                                                <td>0</td>
                                            @endif                      
                                            @foreach ($item['tax'] as $key => $list)
                                                <td>{{ $list['nominal'] }}</td>
                                            @endforeach
                                            <td>@if(isset($item['totalBayar'])){{ $item['totalBayar'] }} @endif</td>
                                             
                                        </tr>
                                    @endif
                                    @endforeach
                                @endif
                                
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

@endsection