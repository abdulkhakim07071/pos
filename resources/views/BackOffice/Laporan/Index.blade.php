@extends('BackOffice.Layouts.Main')



@section('container')


  <!-- Preloader -->
  <!-- <div class="preloader flex-column justify-content-center align-items-center">
    <img class="animation__shake" src="dist/img/AdminLTELogo.png" alt="AdminLTELogo" height="60" width="60">
  </div> -->

  


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

        {{-- filter --}}
        <form action="{{ route('laporan.index') }}" method="GET">
          <div class="row text-center">
            <div class="col-lg-3 col-3">
              <br>
              <div class="input-group date">
                  <input id="start-datepicker" type="date" class="form-control" placeholder="Pilih Tanggal Awal" name="startDate">
              </div>
          </div>
          <div class="col-lg-3 col-3">
              <br>
              <div class="input-group date">
                  <input id="end-datepicker" type="date" class="form-control" placeholder="Pilih Tanggal Akhir" name="endDate">
              </div>
          </div>
          <div class="col-lg-3 col-3">
              <br>
              <button type="submit" id="filter-button" class="btn btn-primary">Filter</button>
              <a href="{{ route('laporan.index') }}" class="btn btn-danger">Reset Filter</a>
          </div>
        </form>
          <!-- /.form group -->
          
        </div>
        {{-- end filter --}}


        <!-- Small boxes (Stats box) -->
        <div class="row">
          <div class="col-lg-3 col-6">
            <br>
            <!-- small box -->
            <div class="small-box bg-info">
              <div class="inner">
                <p>Penjualan Kotor</p>

                <h4 class="poppins">Rp. {{ number_format($totalPenjualanKotor, 2) }}</h4>
              </div>
              
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <br>
            <!-- small box -->
            <div class="small-box bg-success">
              <div class="inner">
                  
                  <p>Diskon</p>
                  <h4 class="poppins">Rp. {{ number_format($totalDiskon, 2) }}</h4>
              </div>
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <br>
            <!-- small box -->
            <div class="small-box bg-warning">
              <div class="inner">
                  
                  <p>Penjualan Bersih</p>
                  <h4 class="poppins">Rp. {{ number_format($totalLabaKotor2, 0, ',', '.') }}</h3>
              </div>

              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
          <div class="col-lg-3 col-6">
            <br>
            <!-- small box -->
            <div class="small-box bg-danger">
              <div class="inner">
                  
                  <p>Laba Kotor</p>
                  <h4 class="poppins">Rp. {{ number_format($totalLabaKotor, 0, ',', '.') }}</h4>
              </div>
              
              <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
            </div>
          </div>
          <!-- ./col -->
        </div>

        <!-- end stats box -->
        <!-- /.row -->
        <!-- Main row -->

        <div class="row">
          <!-- Left col -->
          <section class="col connectedSortable">
            <!-- Custom tabs (Charts with tabs)-->
            <div class="card">
              <div class="card-header">
                <h3 class="card-title">
                  <i class="fas fa-chart-pie mr-1"></i>
                  Penjualan
                </h3>
                <div class="card-tools">
                  <ul class="nav nav-pills ml-auto">
                    <li class="nav-item">
                      <a class="nav-link active" href="#revenue-chart" data-toggle="tab">Area</a>
                    </li>
                    <li class="nav-item">
                      <a class="nav-link" href="#sales-chart" data-toggle="tab">Donut</a>
                    </li>
                  </ul>
                </div>
              </div><!-- /.card-header -->
              <div class="card-body">
                <div class="tab-content p-0">
                  <!-- Morris chart - Sales -->
                  <div class="chart tab-pane active" id="revenue-chart" style="position: relative; width: 100%; height: 300px;">
                    <canvas id="transactionChart" style="width: 100%; height: 100%;"></canvas>
                </div>
                              
                  <div class="chart tab-pane" id="sales-chart" style="position: relative; height: 300px;">
                    <canvas id="sales-chart-canvas" height="300" style="height: 300px;"></canvas>
                  </div>
                </div>
              </div><!-- /.card-body -->
            </div>
            <!-- /.card -->

            

          </section>
         
          <!-- right col -->
        </div>
                      @php
if (!function_exists('calculateSubtotal')) {
    function calculateSubtotal($products) {
        $subtotal = 0;
        foreach ($products as $product) {
            if (isset($product['subtotal'])) {
                $subtotal += $product['subtotal'];
            }
        }
        return $subtotal;
    }
}
@endphp
        <!-- Tabel Riwayat penjualan -->

        <div class="row">
            <section class="col">
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                            <h5 class="poppins">EKSPOR </h5>
                        </div>
                    </div>
                    
                    <div class="card-body">
                        <table id="Penjualan" class="table table-striped" style="width:100%">
                          <thead>
                            <tr>
                                <th>Tanggal</th>
                                <th>Diskon</th>
                                <th>Margin</th>
                                <th>Penjualan Kotor</th>
                                <th>Penjualan Bersih</th>
                                <th>Harga Pokok</th>
                                <th>Laba Kotor</th>
                                <th>Pajak</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($transaction as $item)
                            <tr>
                                <td>{{ $item['tanggal'] }}</td>
                                <td>
                                  @php
                                  $totalDiskon = 0;
                                  if(isset($item['discount'])){
                                  foreach($item['discount'] as $keydiscount => $diskon){
                                  $totalDiskon = $diskon['nominal'] ;
                                  }
                                
                              }
                              @endphp
                                {{number_format($totalDiskon, 2)   }}
                                </td>
                                <td>Rp {{ isset($item['totalBayar']) && isset($item['grandtotal']) ?number_format( $item['totalBayar'] - $item['grandtotal'] , 2): '0' }}</td>
                                <td>Rp {{ isset($item['totalBayar']) ?  number_format($item['totalBayar'], 2) : '0' }}</td>
                                <td>Rp {{ isset($item['grandtotal']) ?  number_format($item['grandtotal'], 2) : '0' }}</td>
                                <td>Rp {{ calculateSubtotal($item['products']) }}</td> <!-- Menggunakan fungsi untuk menghitung subtotal -->
                                <td>Rp {{ isset($item['grandtotal']) ? number_format($item['grandtotal'], 2)  : '0' }}</td>
                                <td>

                                  @php
                                      $totalPajak = 0 ;
                                  @endphp
                                  @if(isset($item['tax']))
    
                                    @foreach($item['tax'] as $keydiscount => $pajak)
                                      @php
                                          $totalPajak += intval($pajak['nominal']);
                                      @endphp
                                    @endforeach
                                    
                                    @endif
                                    {{ number_format($totalPajak, 2)  }}
                                </td> 
                            </tr>
                            @endforeach
                        </tbody>
                        </table>
                    </div>

                   
                </div>
            </section>
        </div>
<!-- end tabel riwayat penjualan -->


        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
     </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 


  @endsection




  
  @section('script')
  



  <script>
    $(function () {
      $("#Penjualan").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": [ "colvis"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
      
    });
  </script>

  <script>
$(document).ready(function(){
    // Ambil data dari Blade dan konversi ke objek JavaScript
    var labels = {!! $labels !!};
    var data = {!! $data !!};

    // Inisialisasi chart dengan data dan label
    var ctx = document.getElementById('transactionChart').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: labels,
            datasets: [{
                label: 'Penjualan Bersih',
                data: data,
                backgroundColor: 'rgba(75, 192, 192, 0.2)',
                borderColor: 'rgba(75, 192, 192, 1)',
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true,
                    title: {
                        display: true,
                        text: 'Penjualan Bersih'
                    }
                },
                x: {
                    title: {
                        display: true,
                        text: 'Tanggal Transaksi'
                    }
                }
            }
        }
    });


});
  </script>
  

<!-- Tambahkan script untuk DatePicker -->
<script>
    $(function () {
        $('.datepicker').datepicker({
            format: 'dd MM yyyy',
            autoclose: true,
            todayHighlight: true
        });

        $('.datepicker').on('focus', function () {
            $(this).datepicker('show');
        });
    });
</script>
{{-- End DatePicker --}}
@endsection

