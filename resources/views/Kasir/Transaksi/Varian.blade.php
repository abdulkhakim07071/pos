<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $title }}</title>
    
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css')}}">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />

  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="{{asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{ asset('plugins/jqvmap/jqvmap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{ asset('plugins/summernote/summernote-bs4.min.css') }}">
  <!-- Custom style -->
  <link rel="stylesheet" type="text/css" href="{{asset('css/StyleBackOffice.css')}}">
  <!-- font Poppins -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Poppins" />
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">

   <!-- daterange picker -->
   <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}">
   <!-- iCheck for checkboxes and radio inputs -->
   <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">

   <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
  <style>
    hr {
      margin: 5px;    /* Menghapus margin atas dan bawah */
      padding: 0;   /* Menghapus padding */
    }
  </style>
</head>
<body>
    <div class="container-fluid">
     <!-- Tabel Karyawan -->
     <form action="{{ route('kasir.varians',['id' => $id]) }}" method="POST">
        @csrf
            <section class="content">
    
                       <!-- Tabel Riwayat penjualan -->
    
            <div class="row">
                <div class="col">
                    <br><br><br>
                    <div class="card">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col text-center">

                                    <h1>Pesanan</h1>
                                    <br>
                                </div>

                            </div>
                            <div class="row">
                                 <div class="col">
                                    <p>Nama Produk</p>
                                </div> 
                                 <div class="col">
                                    <p>Kuantitas</p>
                                </div> 
                                 <div class="col-3">
                                    <p>harga</p>
                                </div> 
                            </div>
                        </div>
                    
                        <div class="card-body">
                            @foreach($dataproduk['products'] as $index => $produkdetail)
                            <div class="row">
                                <hr>
                                <div class="col"><p>{{ $produkdetail['name'] }}</p></div>
                                <input type="hidden" name="produkdetail[]" value="{{ $produkdetail['name']}}" >
                                <input type="hidden" name="keys[]" value="{{ $produkdetail['key']}}" >
                                <input type="hidden" name="indexs[]" value="{{ $index}}" >
                                <div class="col"><p>{{ $produkdetail['kuantitas'] }}</p></div>
                                <input type="hidden" name="kuantitas[]" value="{{ $produkdetail['kuantitas']}}" >
                                <div class="col-3"><p>{{ $produkdetail['subtotal'] }}</p></div>
                            </div>
                            <hr>
                            @endforeach
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col"><p>Total</p> </div>
                                <div class="col-3"><p>{{ $dataproduk['grandtotal'] }}</p> </div>
                            </div>
                            <hr>
                            @if(isset($dataproduk['tax']))
                            @foreach($dataproduk['tax'] as $key => $pajak)

                            <div class="row">

                                <div class="col"><p>{{ $pajak['namaPajak'] }}</p> </div>
                                <div class="col"><p>{{ $pajak['nilai'] }}</p> </div>
                                <div class="col-3"><p>{{ $pajak['nominal'] }} </p></div>
                            </div>
                            {{-- <hr> --}}
                            @endforeach
                            @endif
                            @if(isset($dataproduk['discount']))
                            @foreach($dataproduk['discount'] as $key => $diskon)

                            <div class="row">

                                <div class="col"><p>{{ $diskon['namaDiskon'] }} </p></div>
                                <div class="col"><p>{{ $diskon['nilai'] }}</p> </div>
                                <div class="col-3"><p>{{ $diskon['nominal'] }}</p> </div>
                            </div>
                            {{-- <hr> --}}
                            @endforeach
                            @endif
                            
                        </div>

                    </div>
                </div>
                <div class="col">
                    <br><br><br>
                    <div class="card">
                        <div class="card-body">
                            @if(session('alert'))
                                <script>alert('{{ session('alert') }}');</script>
                            @endif

                            
                          
                            @foreach($dataproduk['products'] as $indexs => $produk)

                            @if(isset($produk['varians']))
                            @for($i=0; $i < $produk['kuantitas']; $i++)
                            <input type="hidden" name="nameProduk[]" value="{{ $produk['name'] }}">

                            <div class="row">
                                <div class="col">{{ $produk['name'] }} </div>

                                <div class="col">
                                <select class="custom-select" name="pilihVarian[]" onchange="handleVarianChange(this , {{ $i }}, '{{ $indexs }}')">
                                    <option value="0" data-varian="">--- Pilih Varian ---</option>
                                    @foreach($produk['varians'] as $index => $varians)
                                    <option value="{{ $varians['harga'] }}" data-namaVarian="{{ $varians['varian'] }}">{{ $varians['varian'] }}</option>
                                    @endforeach
                                    
                                </select>
                                </div>
                                <div class="col tambahVarian{{ $indexs }}{{ $i }}">
                                    <input type="hidden" name="namevar[]" value="original"/>
                                </div>
                            </div>

                            <hr>
                            @endfor
                            @else
                            @for($i=0; $i < $produk['kuantitas']; $i++)
                            <input type="hidden" name="nameProduk[]" value="{{ $produk['name'] }}">
                            <input type="hidden" name="pilihVarian[]" value="0">
                            <input type="hidden" name="namevar[]" value="Original">
                            @endfor
                            @endif
                            @endforeach
                            <br><br>
                            <div class="row">
                                <div class="div col-9">
                                    Harga Varian
                                </div>
                                <div class="div totalHarga">

                                </div>
                            </div>
                            <br><br>
                            
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col">
                                    <div class="kembalian">
    
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <p class="text-center">
                                        Kembalian
                                    </p>
                                </div>
                            </div>
                            <div class="row items-align-center">
                                <div class="col text-center">

                                    <button class="btn-utama" type="submit"> Selesai </button>
                                </div>
                            </div>
                            
                        </div>

                    </div>
                </form>

                    <div class="row">
                        <br>
                        <br>
                        <div class="col text-center">
                            <form method="POST" action="{{ route('transaksi.delete', ['id' => $key ]) }}">
                                @csrf
                                @method('delete')
                                <!-- Isi formulir untuk mengupdate produk -->
                                <button type="submit" class="btn-utama">Delete</button>
                            </form>
                        </div>
                        
                    </div>
                </div>
               
            </div>   
            </section> 
  
    </div>
    
</body>

<script>
    var products = @json($dataproduk);
    // var products = Object.values(arr);
    var $uangMasukInput = document.getElementById('uang_masuk');
    var $uangMasukDescription = document.getElementById('uang_masuk_description');


    function handleVarianChange(selectElement, index, namaProduk) {
        var selectedOption = selectElement.options[selectElement.selectedIndex];

        let selectedIndex = selectElement.value;
        let namaVarian = selectedOption.getAttribute('data-namaVarian');

        console.log(namaVarian);
        if(selectedIndex){
            varianTambah(selectedIndex, index, namaProduk, namaVarian);
        }
    }
    var totalHarga = 0;
    var hargaPerProduk = {};  // Objek untuk menyimpan harga per produk dan indeks

    function varianTambah(selectedIndex, index ,namaProduk, namaVarian)
    {
        totalHarga = 0;

        var $varianprice = document.querySelector('.tambahVarian'+namaProduk+index);
        $varianprice.innerHTML =`<input type="hidden" name="namevar[]" value="${namaVarian}"/>

                                ${selectedIndex}`;

        hargaPerProduk[namaProduk+index]= {
            harga: parseFloat(selectedIndex)
        };
        Object.keys(hargaPerProduk).forEach(function(kunci) {
                var harga = hargaPerProduk[kunci].harga;
                totalHarga += parseFloat(harga);

            });
            console.log(hargaPerProduk);
            updateTotalHarga();
    }
    function updateTotalHarga() {
    $totalHarga = document.querySelector('.totalHarga')
    $totalHarga.innerHTML = ` ${totalHarga}`;
}



    var $price = document.querySelector('.price');
    var $uangbayar = document.querySelector('.uangbayar');
    var $kembalian = document.querySelector('.kembalian');
    var $kembalianstruk = document.querySelector('.kembalianstruk');
    
    $price.innerHTML = `<h1 class="text-center">${products.totalBayar}</h1>`;
    
    // var kembalian = 0;

    function hitungKembalian() {
    var uangMasuk = parseFloat($uangMasukInput.value) || 0; // Get the entered amount or default to 0
    var kembalian = uangMasuk - products.totalBayar;
    $kembalian.innerHTML = `<input type="hidden" name="kkk" value="${kembalian.toFixed(2)}"><h1 class="text-center">${kembalian.toFixed(2)}</h1> `;

    $uangMasukDescription.innerHTML = `Jumlah Uang customer: ${uangMasuk}`;
    $uangbayar.innerHTML = `<p> ${uangMasuk} </p>`;

    $kembalianstruk.innerHTML = `<p>${kembalian.toFixed(2)}</p>`;
}



</script>
</html>




