<?php

namespace App\Http\Controllers\Management;

use Illuminate\Http\Request;
use Kreait\Firebase\Contract\Auth;
use App\Http\Controllers\Controller;
use Kreait\Firebase\Contract\Database;

class KaryawanController extends Controller
{
    protected $tablename;
    protected $users;
    protected $auth;

    public function __construct(Database $database, Auth $auth)
    {
        
        $this->database = $database;
        $this->tablename = 'employee';
        $this->users = 'users';
        $this->auth = $auth;

    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $url = $this->managementCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();

        $employee = $this->database->getReference($this->users)->getValue();

        return view('Management.HakAkses.Index',[
            'title' => 'Management Karywan',
            'active'=> 'Management Karywan'
            ],compact('employee', 'userData'));    }

    public function formTambahKaryawan(){
        $url = $this->managementCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();

    return view('Management.HakAkses.Create',[
                    'title' => 'Tambah Karywan',
                    'active'=> 'Management Karywan'
                ],compact('userData'));
    }
    
            /**
             * Store a newly created resource in storage.
             */
            public function store(Request $request)
    {
        //
    }
    public function delete(string $id )
    {
        try{

            $this->auth->deleteUser($id);
            $postData = $this->database->getReference($this->users . '/' . $id)->remove();
            return redirect('management-karyawan')->with('status', 'Karyawan berhasil dihapus');
        }catch(\Exception $e){
            return redirect('management-karyawan')->with('status', 'Karyawan gagal dihapus');
        }
    }
    public function edit($id)
    {
        $url = $this->managementCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };

         // Mengambil data produk dari Firebase Realtime Database
         $reference = $this->database->getReference($this->users. '/' .  $id);
         $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();

        //  $auth = $this->database->getReference($this->auth. '/' .  session('user_id'))->getValue();
         $snapshot = $reference->getSnapshot();
 
         if (!$snapshot->exists()) {
             // Handle jika produk tidak ditemukan
             return abort(404);
         }
 
         $karyawan = $snapshot->getValue();
        //  dd($produk);
         return view('Management.HakAkses.Edit',[
            'title' => 'Hak Akses Edit',
            'active'=> 'Hak Akses',
            
            ], compact('karyawan','id', 'userData',) ); 
 
    }
    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
