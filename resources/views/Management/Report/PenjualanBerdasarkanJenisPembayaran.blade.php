@extends('Management.Layouts.Main')

@section('container')

<div class="content-wrapper">
<div class="container-fluid">
 <!-- Tabel Karyawan -->
        <section class="content">

{{-- filter --}}
<form action="{{ route('laporan.jenis.pembayaran.management') }}" method="GET">
  <div class="row text-center">
    <div class="col-lg-3 col-4">
      <br>
      <div class="input-group date">
          <input id="start-datepicker" type="date" class="form-control" placeholder="Pilih Tanggal Awal" name="startDate">
      </div>
  </div>
  <div class="col-lg-3 col-4">
      <br>
      <div class="input-group date">
          <input id="end-datepicker" type="date" class="form-control" placeholder="Pilih Tanggal Akhir" name="endDate">
      </div>
  </div>
  <div class="col-lg-3 col-4">
      <br>
      <button type="submit" id="filter-button" class="btn btn-primary">Filter</button>
      <a href="{{ route('laporan.jenis.pembayaran.management') }}" class="btn btn-danger">Reset Filter</a>
  </div>
</form>
          <!-- /.form group -->
          
          {{-- <div class="col-lg-3 col-6">
            <br>
            
              <a href="{{ route('laporan.barang') }}" class="btn btn-danger">Reset Filter</a> --}}
  
            {{-- <p class="btn-third"  data-toggle="modal" data-target="#modalWaktu">
              <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 256 256"><path fill="currentColor" d="M128 24a104 104 0 1 0 104 104A104.11 104.11 0 0 0 128 24Zm0 192a88 88 0 1 1 88-88a88.1 88.1 0 0 1-88 88Zm64-88a8 8 0 0 1-8 8h-56a8 8 0 0 1-8-8V72a8 8 0 0 1 16 0v48h48a8 8 0 0 1 8 8Z"/></svg>
              Sepanjang hari
            </p>
            <!-- Modal -->
              <div class="modal fade" id="modalWaktu" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body text-left">
                      <div class="form-group clearfix">
                        <div class="icheck-primary d-inline ">
                          <input type="radio" id="sepanjangHari" name="r1" checked>
                          <label for="sepanjangHari" class="poppins">
                            Sepanjang Hari
                          </label>
                        </div>
                        <br><br>
                        <div class="icheck-primary ">
                          <input type="radio" id="waktuTertentu" name="r1">
                          <label class="poppins" for="waktuTertentu">
                            <!-- time Picker -->
                              <div class="bootstrap-timepicker">
                                <div class="form-group">
                                  <label>Waktu Mulai</label>

                                  <div class="input-group date" id="timepickerstart" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" data-target="#timepickerstart"/>
                                    <div class="input-group-append" data-target="#timepickerstart" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="far fa-clock"></i></div>
                                    </div>
                                    </div>
                                  <!-- /.input group -->
                                </div>
                                <!-- /.form group -->
                              </div>
                              <div class="bootstrap-timepicker">
                                <div class="form-group">
                                  <label>Waktu Akhir</label>

                                  <div class="input-group date" id="timepickerend" data-target-input="nearest">
                                    <input type="text" class="form-control datetimepicker-input" data-target="#timepickerend"/>
                                    <div class="input-group-append" data-target="#timepickerend" data-toggle="datetimepicker">
                                        <div class="input-group-text"><i class="far fa-clock"></i></div>
                                    </div>
                                    </div>
                                  <!-- /.input group -->
                                </div>
                                <!-- /.form group -->
                              </div>
                          </label>
                        </div>
                      </div>
                      
                  </div>
                    <div class="modal-footer">
                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary">Save changes</button>
                    </div>
                  </div>
                </div>
              </div> --}}
              {{-- end modal --}}

          </div>
           <!-- /.col -->

            <!-- /.form-group -->
          <!-- /.col -->


        </div>
        {{-- end filter --}}

                   <!-- Tabel Riwayat penjualan Pembayaran-->

        <div class="row">
            <section class="col">
                <br>
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                        <h5 class="poppins">EKSPOR </h5>
                        </div>
                    </div>
                      <div class="card-body">
                          <table id="tabelPembayaran" class="table table-striped" style="width:100%">
                              <thead>
                              <tr>
                                  <th>Jenis Pembayaran</th>
                                  <th>Jumlah Pembayaran</th>
                                  <th>Total Pembayaran</th>
                                  
                              </tr>
                          </thead>
                          <tbody>
                            @php
                            $transaksiGrandtotal = 0;
                            $transaksiGrandtotalNon = 0;
                            $transaksiTotalbayar = 0;
                            $transaksiTotalbayarNon = 0;
                            @endphp
                            {{-- @dd($transaction) --}}
                            @foreach($transaction as $transaksi)
                            @php
                              if(isset($transaksi['payment'])){
                              if($transaksi['payment'] == "Tunai"){
                                $transaksiGrandtotal += $transaksi['grandtotal'];
                                $transaksiTotalbayar += $transaksi['totalBayar'];
                              }
                              if($transaksi['payment'] == "Non Tunai"){
                                $transaksiGrandtotalNon += $transaksi['grandtotal'];
                                $transaksiTotalbayarNon += $transaksi['totalBayar'];
                              }}
                            @endphp
                            @endforeach
                            <tr>
                              {{-- <td>{{ $transaksi['payment'] ?? 'N/A' }}</td> --}}
                              <td>Tunai</td>
                              <td>{{ $transaksiGrandtotal }}</td>
                              <td>{{ $transaksiTotalbayar }}</td>
                            </tr>
                            <tr>
                              {{-- <td>{{ $transaksi['payment'] ?? 'N/A' }}</td> --}}
                              <td>Non Tunai</td>
                              <td>{{ $transaksiGrandtotalNon }}</td>
                              <td>{{ $transaksiTotalbayarNon}}</td>
                            </tr>
                            </tbody>
                            
                            {{-- Ketambahan NA pada progress terbaru 23 02 --}}

                          </table>
                      </div>
                </div>
            </section>
        </div>      
        </section>
        </div>
<!-- end tabel riwayat penjualan karyawan -->
    </div>
</div>
 

@endsection

  
@section('script')
<script>
  $(function () {
    $("#tabelPembayaran").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": [ "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    
  });
</script>

<script>

 
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
  
    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })
  
   
  
    //Date picker
    $('#reservationdate').datetimepicker({
        format: 'L'
    });
  
    //Date and time picker
    $('#reservationdatetime').datetimepicker({ icons: { time: 'far fa-clock' } });
  
    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )
  
    //Timepicker
    $('#timepickerstart').datetimepicker({
      format: 'LT'
    })
    //Timepicker
    $('#timepickerend').datetimepicker({
      format: 'LT'
    })
  
    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()
  
    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()
  
    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    })
  
    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    })
  
  })
  
  </script>
@endsection
