<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Nota Belanja</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <style>
        body {
            font-family: 'Arial', sans-serif;
        }

        table{
            width: 100%;
            margin-bottom: 20px;
        }
        thead>tr>td{
                border-collapse: collapse;
                border: none #949292; /* Menghilangkan border pada sel */
                text-align: center;

            }

        th.head , td.data {
            border: 1px solid #ddd;
            padding: 8px;
            text-align: center;
        }

        th {
            background-color: #f2f2f2;
        }

        .total-row td {
            font-weight: bold;
        }

        hr {
            margin: 5px 0;
        }
    </style>
</head>
<body>
    <div class="container-fluid">
        <!-- Tabel Karyawan -->
        <section class="content">
            <div class="row">
                <table>
                    <thead>
                        <tr>
                            <td colspan="3">
                                --------------------------------------------------------------------------------------------------------------------------
                            </td>
                        </tr>
                       
                        <tr>
                            <td colspan="3"><h1>{{ $userData['nameStore'] }} </h1></td>
                        </tr>
                       
                        <tr>
                            <td colspan="3">
                                --------------------------------------------------------------------------------------------------------------------------
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">{{ $userData['alamat'] }}</td>
                        </tr>
                        <tr>
                            <td colspan="3">
<br>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">
                                --------------------------------------------------------------------------------------------------------------------------
                            </td>
                        </tr>
                        <tr>
                            <td>Nama Kasir : {{ $userData['name']  }}</td>
                            <td></td>
                            <td>Nama Pelanggan : @if(isset($dataproduk['nama'])){{ $dataproduk['nama']  }}@endif</td>
                        </tr>
                        <tr>
                            <td colspan="3">
<br>
                            </td>
                        </tr>
                        <tr>
                            <th class="head" >Nama Produk</th>
                            <th class="head" >Kuantitas</th>
                            <th class="head" >Harga</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($dataproduk['products'] as $key => $produkdetail)
                            <tr>
                                <td class="data">{{ $produkdetail['name'] }}</td>
                                <td class="data">{{ $produkdetail['kuantitas'] }}</td>
                                <td class="data">{{ $produkdetail['subtotal'] }}</td>
                            </tr>
                        @endforeach
                        <tr class="total-row">
                            <td colspan="2" class="data">Total</td>
                            <td class="data">{{ $dataproduk['grandtotal'] }}</td>
                        </tr>
                        @if(isset($dataproduk['tax']))
                            @foreach($dataproduk['tax'] as $key => $pajak)
                                <tr>
                                    <td class="data">{{ $pajak['namaPajak'] }}</td>
                                    <td class="data">{{ $pajak['nilai'] }}</td>
                                    <td class="data">{{ $pajak['nominal'] }}</td>
                                </tr>
                            @endforeach
                        @endif
                        @if(isset($dataproduk['discount']))
                            @foreach($dataproduk['discount'] as $key => $diskon)
                                <tr>
                                    <td class="data">{{ $diskon['namaDiskon'] }}</td>
                                    <td class="data">{{ $diskon['nilai'] }}</td>
                                    <td class="data">{{ $diskon['nominal'] }}</td>
                                </tr>
                            @endforeach
                        @endif
                        <tr class="total-row">
                            <td colspan="2" class="data">Total Bayar</td>
                            <td class="data">{{ $dataproduk['totalBayar'] }}</td>
                        </tr>
                        <tr>
                            <td colspan="2" class="data">Uang</td>
                            <td class="data">{{ $dataproduk['uangCustomer'] }}</td>
                        </tr>
                        <tr>
                            <td colspan="2" class="data">Kembalian</td>
                            <td class="data">@if(isset($dataproduk['kembalian'] )){{ $dataproduk['kembalian'] }} @else {{ $dataproduk['uangCustomer']-$dataproduk['totalBayar'] }} @endif</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </section>
    </div>
</body>
</html>
