<?php

namespace App\Http\Controllers\Management;

use App\Http\Controllers\Controller;
use App\Models\TransferStok;
use Illuminate\Http\Request;
use Kreait\Firebase\Contract\Database;

class TransferStokController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    protected $database;
    protected $tabletransfer;
    protected $users;
    public function __construct(Database $database)
    {
        $this->database = $database;
        $this->tabletransfer = 'transfers';
        $this->users = 'users';
    }
    
    
    public function index()
    {
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();

        $transfer = $this->database->getReference($this->tabletransfer)->getValue();
        return view('Management.Transfer.Index',[
            'title' => 'Transfer Stok',
            'active'=> 'Transfer Stok'
            ], compact('transfer', 'userData'));
        
    }

    public function Create()
    {
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();

        return view('Management.Transfer.Create',[
            'title' => 'Tambah Transfer Stok',
            'active'=> 'Tambah Transfer Stok'
            ],compact('userData'));
    }

    public function detail($id)
    {
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();


        $transfers = $this->database->getReference($this->tabletransfer)->getValue();

         // Mengambil data produk dari Firebase Realtime Database
        $reference = $this->database->getReference('transfers/' . $id);
        $snapshot = $reference->getSnapshot();

        if (!$snapshot->exists()) {
             // Handle jika produk tidak ditemukan
            return abort(404);
        }

        $transfer = $snapshot->getValue();
        //  dd($produk);
        return view('Management.Transfer.Detail',[
            'title' => 'Rincian Transfer Stok',
            'active'=> 'Rincian Transfer Stok',
            'data' => $transfer,

            ], compact('transfers','transfer', 'userData') ); 
    }

    
    public function edit($id)
    {
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();

        $transfers = $this->database->getReference($this->tabletransfer)->getValue();

        $reference = $this->database->getReference('transfers/' . $id);
        $snapshot = $reference->getSnapshot();

        $editdata = $snapshot->getValue();
        // dd($produk);
         return view('Management.Transfer.Edit',[
            'title' => 'Ubah Transfer Stok',
            'active'=> 'Ubah',
            ], compact('transfers','editdata', 'userData') ); 
    }
    
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $postData = [
            'tasal' => $request->toko_asal,
            'ttujuan' => $request->toko_tujuan,
            'tanggal' => $request->date,
            'catatan' => $request->notes,
            'namaBarang' => $request->nama_barang,
            'kodeBarang' => $request->kode_barang,
            'asalStok' => $request->jml_asal,
            'tujuanStok' => $request->jml_tujuan,
            'kuantitas' => $request->jml_barang,
        ];

        // Simpan data ke Firebase
        $postRef = $this->database->getReference($this->tabletransfer)->push($postData);

        if ($postRef) {
            // Jika berhasil disimpan, Anda dapat menambahkan logika atau pengalihan rute
            return redirect()->route('transfer.stok', ['id' => $postRef->getKey()])
                ->with('status', 'Transfer Stok Berhasil Ditambahkan');
        } else {
            return redirect()->route('transfer.stok')
                ->with('status', 'Transfer Stok Gagal Ditambahkan');
        }
    }


   

    /**
     * Display the specified resource.
     */
    public function show()
    {

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, $id)
    {

        $updateData= [
            'tasal' => $request->toko_asal,
            'ttujuan' => $request->toko_tujuan,
            'tanggal' => $request->date,
            'catatan' => $request->notes,
            'namaBarang' => $request->nama_barang,
            'kodeBarang' => $request->kode_barang,
            'asalStok' => $request->jml_asal,
            'tujuanStok' => $request->jml_tujuan,
            'kuantitas' => $request->jml_barang,
        ];
        $postData = $this->database->getReference($this->tabletransfer . '/' . $id)->update($updateData);
        
        if($postData){
            return redirect('detail')->with('status','Produk Berhasil Diubah');
        }else{
            return redirect('transfer.edit')->with('status','Produk Gagal Diubah');
        }   
    }

    /** 
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
