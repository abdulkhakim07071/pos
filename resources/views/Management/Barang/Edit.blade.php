@extends('Management.Layouts.Main')



@section('container')

<div class="content-wrapper">
    <div class="container-fluid">
        <section class="content">
            <div class="row">
                <div class="col-lg-8 col-12">
                    <br>
                    @if(session('status'))
                    <h5 class="alert alert-warning mb-3">{{ session('status') }}</h5>
                    @endif
                <form method="POST" action="{{ route('product.update',['id' => $id ]) }}"  enctype="multipart/form-data">
                        @csrf
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="form-floating mb-3 col">
                                    <label for="name" class="font">Nama</label>
                                    <input type="text" class="form-control" id="name" name="name" value="{{ $produk['name'] }}" placeholder="Masukkan Nama Produk" required>
                                </div>
                                <div class="form-group mb-3 col">
                                    <label for="kategoridropdown" class="font">Masukkan Kategori</label>
                                    <select class="form-control"name="categorie" id="kategoridropdown">
                                        @foreach($categories AS $categori => $item)
                                        <option name="categorie" value="{{ $item['name'] }}"  @if($produk['category'] == $item['name'] ) selected @endif>{{ $item['name'] }}</option>
                                        @endforeach
                                      
                                    </select>
                                  </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col">
                                    <input type="text" class="form-control" name="description" value="{{ $produk['description'] }}" placeholder="Masukkan Deskripsi">
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col">
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" name="ready" @if($produk['ready'] == true) checked @endif  value="true" id="produkcheck1">
                                        <label class="form-check-label poppins" for="produkcheck1">
                                          Produk ini siap untuk di jual
                                        </label>
                                      </div>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="form-floating col-6">
                                    <label for="harga" class="font">Harga</label>
                                    <input type="number" class="form-control" id="harga" name="price"  value="{{ $produk['price'] }}" placeholder="Masukkan Harga Produk" required>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="form-floating col">
                                    <label for="skuproduk" class="font">SKU</label>
                                    <input type="text" class="form-control" id="skuproduk" name="sku" @if(isset($produk['sku'])) 
                                     value="{{ $produk['sku'] }}" @endif
                                      placeholder="Masukkan SKU Produk">
                                </div>
                                <div class="form-floating col">
                                    <label for="barcode" class="font">Barcode</label>
                                    <input type="number" class="form-control" id="barcode" name="barcode" @if(isset($produk['barcode'])) 
                                    value="{{ $produk['barcode'] }}" @endif placeholder="Masukkan Barcode Produk">
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">
                               <h4>
                                Inventaris
                                </h4> 
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row mb-3">
                                <div class="col">
                                    <p>Barang Komposit</p>
                                </div>
                                <div class="col-2 ml-auto" >
                                    <p id="inventarisKomposit"></p>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col">
                                    <p>Lacak Stok</p>
                                </div>
                                <div class="col-2 ml-auto">
                                    <p id="inventarisLacakStok"></p>
                                </div>
                            </div>
                            <div id="formInventaris">
                                
                            </div>
                            <div id="formtambahkomposit">
                                
                            </div>
                        </div>
                    </div>
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">
                                   <h4>
                                    HPP
                                    </h4> 
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="row mb-3">
                                    <div class="col">
                                        <p>Biaya</p>
                                    </div>
                                    <div class="col-2 ml-auto" id="hppBiaya">
                                    </div>
                                </div>
                                <div class="row mb-3">
                                    <div class="col">
                                        <p>Detail</p>
                                    </div>
                                    <div class="col-2 ml-auto" id="hppDetail">
                                    </div>
                                </div>
                                <div id="formHPP">
                                    
                                </div>
                                <div id="tambahDetail">
                                    
                                </div>
                            </div>
                        </div>
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">
                               <h4>
                                Varian
                                </h4> 
                            </div>
                        </div>
                        <div class="card-body">
                            <div class="row mb-3">
                                <p>Gunakan varian jika barang memiliki ukuran, warna atau pilihan yang berbeda</p>
                            </div>
                            <div id="tambahVarian">
                                @if(isset($produk['varians']))
                                @foreach($produk['varians'] as $ay => $item)


                                <div class="row mb-3">
                                    <div class="form-floating mb-3 col">
                                        <label for="varian" class="font">Varian</label>
                                        <input type="text" class="form-control" id="varian"name="varian[]" value="{{ $item['varian'] }}" placeholder="Varian"  required>
                                    </div>
                                    <div class="form-group mb-3 col">
                                        <label for="harga" class="font">Harga</label>
                                        <input type="number" class="form-control" id="harga" name="priceVarian[]" @if(isset($item['harga'])) value="{{ $item['harga'] }}" @endif placeholder="Harga" required>
                                      </div>
                                    <div class="form-floating mb-3 col">
                                        <label for="SKU" class="font">SKU</label>
                                        <input type="text" class="form-control" id="SKU" name="skuVarian[]" @if(isset($item['sku'])) value="{{ $item['sku'] }}" @endif placeholder="SKU">
                                    </div>
                                    <div class="form-group mb-3 col">
                                        <label for="barkode" class="font">Barkode</label>
                                        <input type="text" class="form-control" id="barkode" name="barkodeVarian[]" @if(isset($item['barkode'])) value="{{ $item['barkode'] }}" @endif placeholder="Barkode">
                                      </div>
                                </div>
                                @endforeach
                                @endif


                            </div>
                            <div class="row mb-3">

                                <h5 class="poppins" onclick="varianTambah()" >
                                    <svg xmlns="http://www.w3.org/2000/svg" height="24" width="24" viewBox="0 0 512 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2023 Fonticons, Inc.--><path d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z"/></svg>
                                    Tambahkan Varian 
                                </h5>
                            </div>
                            
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">
                               <h4>
                                Tampilan Produk
                                </h4> 
                            </div>
                        </div>
                        <div class="card-body">
                            
                            <div class="row mb-3">
                                <div class="col">

                                    <p>Gambar</p>
                                </div>
                            </div>
                            <div class="row mb-3">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="formFile" class="form-label">Gambar</label>
                                        <input class="form-control" name="foto_produk" type="file" id="formFile">                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <div class="row">
                        <br>
                        <div class="col-lg-8 ml-auto">
                            <div class="row">
                                <div class="col">
                                    <button class="btn-utama"> Simpan </button>
                                </div>
                                <div class="col">
                                    <a href="{{ route('produk') }}" class="btn-second"> Batal </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
                    
                </div>
            </div>
        </section>
    </div>
</div>
@endsection

@section('script')

<script>
    var data = @json($data);
    
    console.log(data); // Access categories array
    console.log(data.name); 

    var inventKomposit = document.getElementById('inventarisKomposit');
    var inventLacakStok = document.getElementById('inventarisLacakStok');
    var formInvent = document.getElementById('formInventaris');
    var tambahKomposit = document.getElementById('formtambahkomposit');
    htmlInvenKompositNonActive = ` 
    <svg xmlns="http://www.w3.org/2000/svg" onclick="setToogleInventaris(true)" height="36" width="36" viewBox="0 0 576 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2023 Fonticons, Inc.--><path d="M384 128c70.7 0 128 57.3 128 128s-57.3 128-128 128H192c-70.7 0-128-57.3-128-128s57.3-128 128-128H384zM576 256c0-106-86-192-192-192H192C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192zM192 352a96 96 0 1 0 0-192 96 96 0 1 0 0 192z"/></svg>
    `;
    htmlInvenKompositActive = ` 
    <svg xmlns="http://www.w3.org/2000/svg" onclick="setToogleInventaris(false)" height="36" width="36" viewBox="0 0 576 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2023 Fonticons, Inc.--><path d="M192 64C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192s-86-192-192-192H192zm192 96a96 96 0 1 1 0 192 96 96 0 1 1 0-192z"/></svg>
    `;
    htmlInvenLacakStokNonActive = ` 
    <svg xmlns="http://www.w3.org/2000/svg" onclick="setToogleInventaris(false)" height="36" width="36" viewBox="0 0 576 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2023 Fonticons, Inc.--><path d="M384 128c70.7 0 128 57.3 128 128s-57.3 128-128 128H192c-70.7 0-128-57.3-128-128s57.3-128 128-128H384zM576 256c0-106-86-192-192-192H192C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192zM192 352a96 96 0 1 0 0-192 96 96 0 1 0 0 192z"/></svg>
    `;
    htmlInvenLacakStokActive = ` 
    <svg xmlns="http://www.w3.org/2000/svg" onclick="setToogleInventaris(true)" height="36" width="36" viewBox="0 0 576 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2023 Fonticons, Inc.--><path d="M192 64C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192s-86-192-192-192H192zm192 96a96 96 0 1 1 0 192 96 96 0 1 1 0-192z"/></svg>
    `;
    htmlHPPBiayaNonActive = ` 
    <svg xmlns="http://www.w3.org/2000/svg" onclick="setToogleHPP(true)" height="36" width="36" viewBox="0 0 576 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2023 Fonticons, Inc.--><path d="M384 128c70.7 0 128 57.3 128 128s-57.3 128-128 128H192c-70.7 0-128-57.3-128-128s57.3-128 128-128H384zM576 256c0-106-86-192-192-192H192C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192zM192 352a96 96 0 1 0 0-192 96 96 0 1 0 0 192z"/></svg>
    `;
    htmlHPPBiayaActive = ` 
    <svg xmlns="http://www.w3.org/2000/svg" onclick="setToogleHPP(false)" height="36" width="36" viewBox="0 0 576 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2023 Fonticons, Inc.--><path d="M192 64C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192s-86-192-192-192H192zm192 96a96 96 0 1 1 0 192 96 96 0 1 1 0-192z"/></svg>
    `;
    htmlHPPDetailNonActive = ` 
    <svg xmlns="http://www.w3.org/2000/svg" onclick="setToogleHPP(true)" height="36" width="36" viewBox="0 0 576 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2023 Fonticons, Inc.--><path d="M384 128c70.7 0 128 57.3 128 128s-57.3 128-128 128H192c-70.7 0-128-57.3-128-128s57.3-128 128-128H384zM576 256c0-106-86-192-192-192H192C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192zM192 352a96 96 0 1 0 0-192 96 96 0 1 0 0 192z"/></svg>
    `;
    htmlHPPDetailActive = ` 
    <svg xmlns="http://www.w3.org/2000/svg" onclick="setToogleHPP(false)" height="36" width="36" viewBox="0 0 576 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2023 Fonticons, Inc.--><path d="M192 64C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192s-86-192-192-192H192zm192 96a96 96 0 1 1 0 192 96 96 0 1 1 0-192z"/></svg>
    `;
    htmlLacakStokForm=`
    <div class="row mb-3">
                                <div class="form-floating mb-3 col">
                                    <label for="stoktersedia" class="font">Stok Tersedia</label>
                                    <input type="number" class="form-control" id="stoktersedia" name="stok" @if(isset($produk['stok'])) value="{{ $produk['stok'] }}" @endif placeholder="Masukkan Stok Tersedia" required>
                                </div>
                                <div class="form-group mb-3 col">
                                    <label for="stokrendah" class="font">Stok Rendah</label>
                                    <input type="number" class="form-control" id="stokrendah" name="minStok" @if(isset($produk['minStok'])) value="{{ $produk['minStok'] }}" @endif placeholder="Masukkan Stok Rendah" required>
                                    <span>jika barang telah sampai atau kurang dari 
                                        stok rendah maka akan muncul pemberitahuan</span>
                                  </div>
                            </div>
    `;
    htmlKompositForm=`
    
    @if(isset($produk['composites']))
    @foreach($produk['composites'] as $key => $komposit)
    @foreach($komposit as $keys => $item)
    <div class="row mb-3">
                                    <div class="form-floating mb-3 col">
                                        <label for="namabarangkomposit" class="font">Masukkan Kategori</label>
                                        <select class="form-control"name="nameKomposit[]" id="namabarangkomposit" required>
                                        @foreach($products AS $key => $kom)
                                        <option name="categorie" value="{{ $kom['name'] }}" @if($item['Komposit'] == $kom['name'])selected @endif>{{ $kom['name'] }}</option>
                                        @endforeach
                                      
                                        </select>
                                       
                                    </div>
                                    <div class="form-group mb-3 col">
                                        <label for="kuantitaskomposit" class="font">Kuantitas</label>
                                        <input type="number" class="form-control" id="kuantitaskomposit" @if(isset($item['kuantitas'])) value="{{ $item['kuantitas'] }}" @endif name="kuantityKomposit[]"  placeholder="kuantitas" required>
                                        
                                    </div>
                                    <div class="form-group mb-3 col">
                                        <label for="hargakomposit" class="font">Harga</label>
                                        <input type="number" class="form-control" id="hargakomposit" @if(isset($item['harga'])) value="{{ $item['harga'] }}" @endif name="priceKomposit[]" placeholder="harga" required>
                                        
                                    </div>

                            </div>

                            @endforeach
                            @endforeach
                            @else
                            <div class="row mb-3">
                                    <div class="form-floating mb-3 col">
                                        <label for="namabarangkomposit" class="font">Nama Barang</label>
                                        <input type="text" class="form-control" id="namabarangkomposit" name="nameKomposit[]" @if(isset($item['Komposit'])) value="{{ $item['Komposit'] }}" @endif placeholder="Nama" required>
                                    </div>
                                    <div class="form-group mb-3 col">
                                        <label for="kuantitaskomposit" class="font">Kuantitas</label>
                                        <input type="number" class="form-control" id="kuantitaskomposit" @if(isset($item['kuantitas'])) value="{{ $item['kuantitas'] }}" @endif name="kuantityKomposit[]"  placeholder="kuantitas" required>
                                        
                                    </div>
                                    <div class="form-group mb-3 col">
                                        <label for="hargakomposit" class="font">Harga</label>
                                        <input type="number" class="form-control" id="hargakomposit" @if(isset($item['harga'])) value="{{ $item['harga'] }}" @endif name="priceKomposit[]" placeholder="harga" required>
                                        
                                    </div>

                            </div>
                            @endif



    
                            
    `;
    htmlKompositTambah=`
                         <div class="row mb-3">
                                <h5 class="poppins" onclick="tambah()">
                                    <svg xmlns="http://www.w3.org/2000/svg" height="24" width="24" viewBox="0 0 512 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2023 Fonticons, Inc.--><path d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z"/></svg>
                                    Tambahkan Komposit 
                                </h5>
                        </div>
    `;


    console.log(data.minStok);
    if (typeof data.minStok !== 'undefined') {
    console.log('minStok is defined');
    // setItem('set', 'active')  
    setToogleInventaris(false);
} else {
    console.log('minStok is not defined');
    setToogleInventaris(true);
}


    // if(localStorage.getItem('set') == 'Active'){
    //         setToogleInventaris(false)
    //     }else{
    //         setToogleInventaris(true)
    //     }

    // switch inventaris 
    function setToogleInventaris(isActive){
        if(isActive){
            inventKomposit.innerHTML = htmlInvenKompositActive
            localStorage.setItem('set', 'active')
            inventLacakStok.innerHTML = htmlInvenLacakStokNonActive
            formInvent.innerHTML = htmlKompositForm
            tambahKomposit.innerHTML = htmlKompositTambah

        }else{
            inventKomposit.innerHTML = htmlInvenKompositNonActive
            localStorage.setItem('set','')
            inventLacakStok.innerHTML = htmlInvenLacakStokActive
            tambahKomposit.innerHTML = ``;
            formInvent.innerHTML = htmlLacakStokForm
            
            


        }
    }
    // end switch 

    // tambah komposit 
    function tambah(){
    var text = `                
    <div class="row mb-3">

                                    <div class="form-floating mb-3 col">
                                        <label for="namabarangkomposit" class="font">Nama Barang</label>
                                        <input type="text" class="form-control" id="namabarangkomposit" name="nameKomposit[]" placeholder="Nama" required>
                                    </div>
                                    <div class="form-group mb-3 col">
                                        <label for="kuantitaskomposit" class="font">Kuantitas</label>
                                        <input type="number" class="form-control" id="kuantitaskomposit"name="kuantityKomposit[]" placeholder="Kuantitas" required>
                                        
                                    </div>
                                    <div class="form-group mb-3 col">
                                        <label for="hargakomposit" class="font">Harga</label>
                                        <input type="number" class="form-control" id="hargakomposit" name="priceKomposit[]" placeholder="Harga" required>
                                        
                                    </div>
`;
            formInvent.innerHTML += text;

        }
    //  end tambahKomposit
    // tambah varian 
    var varianInput = document.getElementById('tambahVarian');


    function varianTambah(){
        text=`
        <div class="row mb-3">
                                <div class="form-floating mb-3 col">
                                    <label for="varian" class="font">Varian</label>
                                    <input type="text" class="form-control" id="varian"name="varian[]" placeholder="Varian"  required>
                                </div>
                                <div class="form-group mb-3 col">
                                    <label for="harga" class="font">Harga</label>
                                    <input type="number" class="form-control" id="harga" name="priceVarian[]" placeholder="Harga" required>
                                  </div>
                                <div class="form-floating mb-3 col">
                                    <label for="SKU" class="font">SKU</label>
                                    <input type="text" class="form-control" id="SKU" name="skuVarian[]" placeholder="SKU" >
                                </div>
                                <div class="form-group mb-3 col">
                                    <label for="barkode" class="font">Barkode</label>
                                    <input type="text" class="form-control" id="barkode" name="barkodeVarian[]" placeholder="Barkode" >
                                  </div>
                            </div>
        `;
        varianInput.innerHTML += text;


    }

    






</script>
<script>
    
    var biayaHPP = document.getElementById('hppBiaya');
    var detailHPP = document.getElementById('hppDetail');
    var formHpp = document.getElementById('formHPP');
    var tambahDetail = document.getElementById('tambahDetail');
    htmlHPPBiayaNonActive = ` 
    <svg xmlns="http://www.w3.org/2000/svg" onclick="setToogleHPP(true)" height="36" width="36" viewBox="0 0 576 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2023 Fonticons, Inc.--><path d="M384 128c70.7 0 128 57.3 128 128s-57.3 128-128 128H192c-70.7 0-128-57.3-128-128s57.3-128 128-128H384zM576 256c0-106-86-192-192-192H192C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192zM192 352a96 96 0 1 0 0-192 96 96 0 1 0 0 192z"/></svg>
    `;
    htmlHPPBiayaActive = ` 
    <svg xmlns="http://www.w3.org/2000/svg" onclick="setToogleHPP(false)" height="36" width="36" viewBox="0 0 576 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2023 Fonticons, Inc.--><path d="M192 64C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192s-86-192-192-192H192zm192 96a96 96 0 1 1 0 192 96 96 0 1 1 0-192z"/></svg>
    `;
    htmlHPPDetailNonActive = ` 
    <svg xmlns="http://www.w3.org/2000/svg" onclick="setToogleHPP(false)" height="36" width="36" viewBox="0 0 576 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2023 Fonticons, Inc.--><path d="M384 128c70.7 0 128 57.3 128 128s-57.3 128-128 128H192c-70.7 0-128-57.3-128-128s57.3-128 128-128H384zM576 256c0-106-86-192-192-192H192C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192zM192 352a96 96 0 1 0 0-192 96 96 0 1 0 0 192z"/></svg>
    `;
    htmlHPPDetailActive = ` 
    <svg xmlns="http://www.w3.org/2000/svg" onclick="setToogleHPP(true)" height="36" width="36" viewBox="0 0 576 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2023 Fonticons, Inc.--><path d="M192 64C86 64 0 150 0 256S86 448 192 448H384c106 0 192-86 192-192s-86-192-192-192H192zm192 96a96 96 0 1 1 0 192 96 96 0 1 1 0-192z"/></svg>
    `;
    htmlFormHPPBiaya =`
    @foreach($produk['hpp'] as $key => $hpp)

    <div class="row mb-3">

    <div class="form-group mb-3 col">
        <label for="pricehpp" class="font">Harga Pokok Produksi</label>
        <input type="number" class="form-control" name="priceHpp" id="pricehpp" @if(isset($hpp)) value="{{ $hpp }}" @endif placeholder="Harga Pokok Produksi">
    </div>
    </div>
    @endforeach
    `;
    htmlFormHPPDetail = `
    @foreach($produk['hpp'] as $keys => $item)
    <div class="row mb-3">
    <div class="form-group mb-3 col">
                                    <label for="namahpp" class="font">Nama</label>
                                    <input type="text" class="form-control" name="nameHpp[]" @if(isset($item['nama'])) value="{{ $item['nama'] }}" @endif id="namahpp" placeholder="Nama Barang" required>
                                  </div>
                                <div class="form-group mb-3 col">
                                    <label for="kuantitashpp" class="font">Kuantitas</label>
                                    <input type="number" class="form-control" name="kuantitasHpp[]" @if(isset($item['kuantitas'])) value="{{ $item['kuantitas'] }}" @endif id="kuantitashpp" placeholder="Kuantitas" required>
                                  </div>
                                <div class="form-group mb-3 col">
                                    <label for="satuanhpp" class="font">Satuan</label>
                                    <input type="text" class="form-control" name="satuanhpp[]" id="satuanhpp" @if(isset($item['satuan'])) value="{{ $item['satuan'] }}" @endif placeholder=" Satuan" required>
                                  </div>
                                <div class="form-group mb-3 col">
                                    <label for="hargadetailhpp" class="font">Harga</label>
                                    <input type="text" class="form-control" name="hargadetailhpp[]" @if(isset($item['harga'])) value="{{ $item['harga'] }}" @endif id="hargadetailhpp"  placeholder="Harga" required>
                                  </div>
                                  <div>
                                    @endforeach
                                    
                                    `
                                    ;
    htmlFormHPPDetailTambah = `

    <div class="row mb-3">
    <div class="form-group mb-3 col">
                                    <label for="namahpp" class="font">Nama</label>
                                    <input type="text" class="form-control" name="nameHpp[]" id="namahpp" placeholder="Nama Barang" required>
                                  </div>
                                <div class="form-group mb-3 col">
                                    <label for="kuantitashpp" class="font">Kuantitas</label>
                                    <input type="number" class="form-control" name="kuantitasHpp[]"id="kuantitashpp" placeholder="Kuantitas" required>
                                  </div>
                                <div class="form-group mb-3 col">
                                    <label for="satuanhpp" class="font">Satuan</label>
                                    <input type="text" class="form-control" name="satuanhpp[]" id="satuanhpp"placeholder=" Satuan" required>
                                  </div>
                                <div class="form-group mb-3 col">
                                    <label for="hargadetailhpp" class="font">Harga</label>
                                    <input type="text" class="form-control" name="hargadetailhpp[]" id="hargadetailhpp"  placeholder="Harga" required>
                                  </div>
                                  <div>

                                    `
                                    ;
    htmlDetailTambah=`
                         <div class="row mb-3">
                                <h5 class="poppins" onclick="detailTambah()">
                                    <svg xmlns="http://www.w3.org/2000/svg" height="24" width="24" viewBox="0 0 512 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2023 Fonticons, Inc.--><path d="M256 512A256 256 0 1 0 256 0a256 256 0 1 0 0 512zM232 344V280H168c-13.3 0-24-10.7-24-24s10.7-24 24-24h64V168c0-13.3 10.7-24 24-24s24 10.7 24 24v64h64c13.3 0 24 10.7 24 24s-10.7 24-24 24H280v64c0 13.3-10.7 24-24 24s-24-10.7-24-24z"/></svg>
                                    Tambahkan Detail 
                                </h5>
                        </div>
    `;
    // console.log(data.hpp);
    if (typeof data.hpp.name !== 'undefined') {
    console.log('minStok is defined');
    // setItem('set', 'active')  
        setToogleHPP(false)
} else {
    console.log('minStok is not defined');
    setToogleHPP(true)
}

    // if(localStorage.getItem('buat') == 'Biaya'){
    //     setToogleHPP(false)
    //     }else{
    //         setToogleHPP(true)
    //     }
        
    function setToogleHPP(isBiaya){
        if(isBiaya){
            biayaHPP.innerHTML = htmlHPPBiayaActive
            localStorage.setItem('buat', 'Biaya')
            detailHPP.innerHTML = htmlHPPDetailNonActive
            formHpp.innerHTML = htmlFormHPPBiaya
            tambahDetail.innerHTML = `` ;
        }else{
            biayaHPP.innerHTML = htmlHPPBiayaNonActive
            localStorage.setItem('buat','Detail')
            detailHPP.innerHTML = htmlHPPDetailActive
            tambahDetail.innerHTML = htmlDetailTambah ;
            formHpp.innerHTML = htmlFormHPPDetail
        }

    }


    function detailTambah(){
        formHPP.innerHTML += htmlFormHPPDetailTambah;


    }
</script>

@endsection