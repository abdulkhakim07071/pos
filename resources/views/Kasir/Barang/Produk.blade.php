@extends('Kasir.Layouts.Main')

@section('container')

<div class="content-wrapper" style="font-family: Poppins;">
    <div class="container-fluid">
        <section class="content">
            <div class="row">
                <div class="col">
                    <br>
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">
                                <h2 style="font-weight:bold; ">Data Produk</h2>
                            </div>
                        </div>
                        <div class="card-body">
                            <table id="tabelProdukBackoffice" class="table table-striped" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Barang</th>
                                    <th>Harga</th>
                                    <th>Biaya</th>
                                    <th>margin</th>
                                    <th>Stok</th>
                                    <th>status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($products))

                                
                                @foreach($products AS $produk => $item)
                                <tr>
                                    <td>{{ $item['name'] }}</td>
                                    <td>Rp.@if(isset($item['price'])){{ $item['price'] }} @endif</td>
                                    <td>
                                        Rp.
                                    @if(isset($item['hpp']['harga']) )
                                        {{ $item['hpp']['harga'] }}
                                    @else
                                            @php
                                        $totalHpp = 0 ;
                                        @endphp
                                            @foreach($item['hpp'] as $hppid => $hpp)
                                                @if($hpp['harga'])
            
                                                @php
                                                $totalHpp += intval($hpp['harga']);
                                                @endphp
                                                @endif
                                            @endforeach
                                                {{ $totalHpp }}
                                    @endif
                                        
                                        </td>
                                    <td>
                                        Rp.
                                    @if(isset($item['hpp']['harga']))
                                        {{$item['price'] - $item['hpp']['harga'] }}
                                    @else
                                        {{ $item['price'] - $totalHpp }}
                                    @endif
                                    </td>
                                <td>@if(isset($item['stok'])){{ $item['stok'] }} @endif</td>
                                @if(isset($item['stok']))
                                    @if($item['stok'] == 0)
                                        <td class="poppins" style="color:red;"> Stok Habis</td>
                                    @elseif($item['stok'] < $item['minStok'])
                                        <td class="poppins" style="color:rgb(244, 240, 10);"> Stok Menipis</td>
                                    @else
                                        <td class="poppins" style="color:green;"> Stok Aman </td>
                                    @endif
                                @else
                                <td></td>
                                @endif                                
                            </tr>
                            @endforeach
                            @endif                         
                            
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

@endsection

@section('script')
<script>
    $(function () {
      $("#tabelProdukBackoffice").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": [ "colvis"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
      
    });
   
  </script>
@endsection