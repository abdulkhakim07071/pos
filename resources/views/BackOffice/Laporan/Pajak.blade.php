@extends('BackOffice.Layouts.Main')

@section('container')

<div class="content-wrapper">
<div class="container-fluid">
 <!-- Tabel Karyawan -->
        <section class="content">
          {{-- filter --}}
          <div class="container-fluid">
            {{-- filter --}}
            <form action="{{ route('laporantanggalpajak.index') }}" method="GET">
                <div class="row text-center">
                <div class="col-lg-3 col-4">
                    <br>
                    <div class="input-group date">
                        <input id="start-datepicker" type="date" class="form-control" placeholder="Pilih Tanggal Awal" name="startDate">
                    </div>
                </div>
                <div class="col-lg-3 col-4">
                    <br>
                    <div class="input-group date">
                        <input id="end-datepicker" type="date" class="form-control" placeholder="Pilih Tanggal Akhir" name="endDate">
                    </div>
                </div>
                <div class="col-lg-3 col-4">
                    <br>
                    <button type="submit" id="filter-button" class="btn btn-primary">Filter</button>
                    <a href="{{ route('laporantanggalpajak.index') }}" class="btn btn-danger">Reset Filter</a>
                </div>
            </form>

                {{-- <div class="col-lg-3 col-6">
                    <br>
                        <a href="{{ route('laporan.pajak') }}" class="btn btn-danger">Reset Filter</a>
                </div> --}}
          </div>
        {{-- end filter --}}

         <!-- Small boxes (Stats box) -->
         <div class="row" style="padding-top: 15px">
          <div class="col-lg-4 col-6">
            <!-- small box -->
            <div class="small-box bg-color-primary" style="height:120px">
              <div class="inner">
                <p>Pendapatan Kena Pajak</p>

                <h4 class="poppins">Rp. {{ number_format($kenaPajak, 2  ) }}</h4>
              </div>

              
           </div>
          </div>
          
          <!-- ./col -->
          <div class="col-lg-4 col-6">
            <!-- small box -->
            <div class="small-box bg-color-primary" style="height:120px">
              <div class="inner">
                <p>Pendapatan Bebas Pajak</p>

                <h4 class="poppins">Rp. {{ number_format($totalPenjualanBersih, 2) }}</h3>
              </div>

              
           </div>
          </div>
          
          <!-- ./col -->
          <div class="col-lg-4 col-6">
            <!-- small box -->
            <div class="small-box bg-color-primary" style="height:120px">
              <div class="inner">
                <p>Pendapatan Bersih</p>

                <h4 class="poppins">Rp. {{ number_format($totalPenjualanBersih, 2) }}</h3>
              </div>

              
           </div>
          </div>
          
          <!-- ./col -->
        </div>

        <!-- end stats box -->
        <!-- /.row -->




                   <!-- Tabel Riwayat penjualan Pajak-->

        <div class="row">
            <section class="col">
                <br>
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                        <h5 class="poppins">EKSPOR </h5>
                        </div>
                    </div>
                    <div class="card-body">
                        <table id="tabelPajak" class="table table-striped" style="width:100%">
                            <thead>
                            <tr>
                                <th>Nama Pajak</th>
                                <th>Tarif Pajak (%)</th>
                                <th>Pendapatan Kena Pajak</th>
                            </tr>
                        </thead>
                        <tbody>
                          @if(isset($pajak))
                          @foreach($pajak as $key => $item)
                              <tr>
                                  <td>{{ $item['name'] }}</td>
                                  <td>{{ $item['nilai'] }}</td>
                                  <td>Rp. {{ number_format($kenaPajak, 2) }}</td> 
                              </tr>
                          @endforeach
                      @endif
                      </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>      
        </section>
        </div>
<!-- end tabel riwayat penjualan karyawan -->
    </div>
</div>
  

@endsection


  
@section('script')
<script>
  $(function () {
    $("#tabelPajak").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": [ "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    
  });
</script>

<script>

 
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
  
    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })
  
   
  
    //Date picker
    $('#reservationdate').datetimepicker({
        format: 'L'
    });
  
    //Date and time picker
    $('#reservationdatetime').datetimepicker({ icons: { time: 'far fa-clock' } });
  
    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )
  
    //Timepicker
    $('#timepickerstart').datetimepicker({
      format: 'LT'
    })
    //Timepicker
    $('#timepickerend').datetimepicker({
      format: 'LT'
    })
  
    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()
  
    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()
  
    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    })
  
    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    })
  
  })
  
  </script>
@endsection
