<?php

namespace App\Http\Controllers\Backoffice;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Kreait\Firebase\Contract\Database;
use Kreait\Firebase\Database as FirebaseDatabase;

class ViewController extends Controller
{
    protected $database;
    protected $tablecategorie;
    protected $tableproduct;
    protected $tablediskon;
    protected $tablekategori;
    protected $tablepajak;
    protected $users;
    protected $tabletransaksi;
    public function __construct(Database $database)
    {
        
        $this->database = $database;
        $this->tablecategorie = 'categories';
        $this->tableproduct = 'products';
        $this->tablediskon = 'discount';
        $this->tablekategori = 'categories';
        $this->tablepajak = 'pajak';
        $this->tabletransaksi = 'transaksi';
        $this->users = 'users';
    }
    public function produk()
    {
        $url = $this->backOfficeCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();

        $products = $this->database->getReference($this->tableproduct)->getValue();

        return view('BackOffice.View.viewProduk',[
            'title' => 'Produk',
            'active'=> 'Produk'
            ],compact('products', 'userData'));
    }
    public function diskon()
    {
        $url = $this->backOfficeCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };
        $transaksi = $this->database->getReference($this->tabletransaksi)->getValue();

        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();

        $discounts = $this->database->getReference($this->tablediskon)->getValue();

        return view('BackOffice.View.viewDiskon',[
            'title' => 'Diskon',
            'active'=> 'Diskon'
            ],compact('discounts','userData', 'transaksi')); 
    }
    public function kategori()
    {
        $url = $this->backOfficeCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };
        $transaksi = $this->database->getReference($this->tabletransaksi)->getValue();

        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();

        $categories = $this->database->getReference($this->tablekategori)->getValue();
        $products = $this->database->getReference($this->tableproduct)->getValue();
        return view('BackOffice.View.viewKategori',[
            'title' => 'Kategori',
            'active'=> 'Kategori'
            ],compact('categories','products', 'userData','transaksi')); 
    }
    public function pajak()
    {
        $url = $this->backOfficeCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();

        $taxs = $this->database->getReference($this->tablepajak)->getValue();
        $transaksi = $this->database->getReference($this->tabletransaksi)->getValue();


        return view('BackOffice.View.viewPajak',[
            'title' => 'Pajak',
            'active'=> 'Pajak'
            ],compact('taxs', 'userData', 'transaksi')); 
    }
    public function transaksi()
    {
        $url = $this->backOfficeCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();

        $transaksi = $this->database->getReference($this->tabletransaksi)->getValue();
        $pajak = $this->database->getReference($this->tablepajak)->getValue();
        $discounts = $this->database->getReference($this->tablediskon)->getValue();


        return view('BackOffice.View.viewTransaksi',[
            'title' => 'Transaksi',
            'active'=> 'Transaksi',
            'transaksi' => $transaksi,
            'pajak' => $pajak,
            'discounts' => $discounts,
            ],compact('transaksi', 'pajak', 'discounts', 'userData')); 
    }

}
