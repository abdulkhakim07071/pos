@extends('Management.Layouts.Main')



@section('container')

<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4 col-8">
                <br>
                @if(session('status'))
                <h5 class="alert alert-warning mb-3">{{ session('status') }}</h5>
                @endif
                <div class="card">
                    <div class="card-header text-center">
                        <br>
                        <br><br>
                        <svg xmlns="http://www.w3.org/2000/svg" height="46" width="46" viewBox="0 0 512 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2023 Fonticons, Inc.--><path d="M345 39.1L472.8 168.4c52.4 53 52.4 138.2 0 191.2L360.8 472.9c-9.3 9.4-24.5 9.5-33.9 .2s-9.5-24.5-.2-33.9L438.6 325.9c33.9-34.3 33.9-89.4 0-123.7L310.9 72.9c-9.3-9.4-9.2-24.6 .2-33.9s24.6-9.2 33.9 .2zM0 229.5V80C0 53.5 21.5 32 48 32H197.5c17 0 33.3 6.7 45.3 18.7l168 168c25 25 25 65.5 0 90.5L277.3 442.7c-25 25-65.5 25-90.5 0l-168-168C6.7 262.7 0 246.5 0 229.5zM144 144a32 32 0 1 0 -64 0 32 32 0 1 0 64 0z"/></svg>
                        <br>
                        <br>
                        <br>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('shift.update', ['id'=> $id ])}}" method="POST">
                            @csrf
                        <div class="form-floating mb-3 row">
                            <label for="name" class="font">Nama</label>
                            <input type="text" class="form-control" id="name" name="name" value={{ $shift['name'] }} placeholder="Masukkan Nama Diskon" required>
                        </div>
                        <div class="form-floating mb-3 row">
                            <label for="start" class="font">Waktu Mulai</label>
                            <input type="time" class="form-control" id="start" name="starttime" value={{ $shift['waktuMulai'] }} placeholder="Masukkan Waktu Mulai" required>
                        </div> 
                        <div class="form-floating mb-3 row">
                            <label for="end" class="font">Waktu Selesai</label>
                            <input type="time" class="form-control" id="end" name="endtime" value={{ $shift['waktuSelesai'] }}  placeholder="Masukkan Waktu Selesai" required>
                        </div> 
                        <div class="row">
                            <br>
                            <div class="col-lg-8 ml-auto">
                                <div class="row">
                                    <div class="col">
                                        <button class="btn-utama"> Simpan </button>
                                    </div>
                                    <div class="col">
                                        <a href="{{ route('shift.master') }}" class="btn-second"> Batal </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection