<?php

namespace App\Http\Controllers\Kasir;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kreait\Firebase\Contract\Database;

class ProductController extends Controller
{
    protected $database;
    protected $tablecategorie;
    protected $tableproduct;
    protected $tablediskon;
    protected $tablekategori;
    protected $tablepajak;
    protected $users;
    public function __construct(Database $database)
    {
        
        $this->database = $database;
        $this->tablecategorie = 'categories';
        $this->tableproduct = 'products';
        $this->tablediskon = 'discount';
        $this->tablekategori = 'categories';
        $this->tablepajak = 'pajak';
        $this->users = 'users';
    }
    public function produk()
    {
        $url = $this->kasirCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();

        $products = $this->database->getReference($this->tableproduct)->getValue();

        return view('Kasir.Barang.Produk',[
            'title' => 'Produk',
            'active'=> 'Produk'
            ],compact('products', 'userData'));
    }
    public function diskon()
    {
        $url = $this->kasirCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();

        $discounts = $this->database->getReference($this->tablediskon)->getValue();

        return view('Kasir.Barang.Diskon',[
            'title' => 'Diskon',
            'active'=> 'Diskon'
            ],compact('discounts', 'userData')); 
    }
    public function kategori()
    {
        $url = $this->kasirCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();

        $categories = $this->database->getReference($this->tablekategori)->getValue();
        $products = $this->database->getReference($this->tableproduct)->getValue();
        return view('Kasir.Barang.Kategori',[
            'title' => 'Kategori',
            'active'=> 'Kategori'
            ],compact('categories','products','userData')); 
    }
    public function pajak()
    {
        $url = $this->kasirCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();

        $taxs = $this->database->getReference($this->tablepajak)->getValue();

        return view('Kasir.Barang.Pajak',[
            'title' => 'Pajak',
            'active'=> 'Pajak'
            ],compact('taxs', 'userData')); 
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
