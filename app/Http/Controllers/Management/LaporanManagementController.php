<?php

namespace App\Http\Controllers\Management;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kreait\Firebase\Contract\Database;

class LaporanManagementController extends Controller
{
    protected $tableproduct;
    protected $tabletransaksi;
    protected $database;
    protected $users;
    protected $shift;
    protected $shiftmaster;
    protected $tablecategorie;
    protected $tablepajak;
    protected $tablediskon;


    public function __construct(Database $database)
    {
        
        $this->database = $database;
        $this->tableproduct = 'products';
        $this->tabletransaksi = 'transaksi';
        $this->tablecategorie = 'categories';
        $this->tablepajak = 'pajak';
        $this->tablediskon = 'discount';

        $this->users = 'users';
        $this->shift = 'shift';
        $this->shiftmaster = 'shiftMaster';
    }
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $url = $this->managementCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();


        $products = $this->database->getReference($this->tableproduct)->getValue();
        $categories = $this->database->getReference($this->tablecategorie)->getValue();
        $pajak = $this->database->getReference($this->tablepajak)->getValue();
        $discounts = $this->database->getReference($this->tablediskon)->getValue();
        $transaction = $this->database->getReference($this->tabletransaksi)->getValue();

        // Filter transaksi yang memiliki status "Bayar"
        $transaction = array_filter($transaction, function ($item) {
            return $item['status'] === 'Lunas';
        });


            // Jika ada tanggal awal dan akhir yang dikirimkan melalui request
            if ($request->filled('startDate') && $request->filled('endDate')) {
                $startDate = $request->input('startDate');
                $endDate = $request->input('endDate');

                // Filter transaksi berdasarkan rentang tanggal
                $filteredTransaction = [];
                foreach ($transaction as $item) {
                    $tanggalTransaksi = $item['tanggal'];
                    if ($tanggalTransaksi >= $startDate && $tanggalTransaksi <= $endDate) {
                        $filteredTransaction[] = $item;
                    }
                }

                // Gunakan transaksi yang sudah difilter
                $transaction = $filteredTransaction;
            }


        // hitung total penjualan kotor /totalBayar
        $totalPenjualanKotor = 0;
        foreach ($transaction as $item) {
            if (isset($item['totalBayar'])) {
                $totalPenjualanKotor += $item['totalBayar'];
            }
        }
        // hitung diskon
        $totalDiskon = 0;
        foreach ($transaction as $item) {
            if (isset($item['discount'])) {
                foreach ($item['discount'] as $discount) {
                    if (isset($discount['nominal'])) {
                        $totalDiskon += $discount['nominal'];
                    }
                }
            }
        }
        //hitung penjualan bersih
        $totalPenjualanBersih = 0;
        foreach ($transaction as $item) {
            if (isset($item['grandtotal'])) {
                $totalPenjualanBersih += $item['grandtotal'];
            }
        }

        // total harga pokok
        $totalHargaPokok = 0;
        foreach ($transaction as $item) {
            // Iterasi melalui setiap produk pada setiap transaksi dan tambahkan subtotalnya
            if (isset($item['products'])) {
                foreach ($item['products'] as $product) {
                    if (isset($product['subtotal'])) {
                        $totalHargaPokok += $product['subtotal'];
                    }
                }
            }
        }

        
        
        // dd($totalLabaKotor, $totalPenjualanBersih);
        
        //total laba kotor
        $totalLabaKotor1 = 0;
        foreach ($transaction as $item) {
            // Iterasi melalui setiap produk pada setiap transaksi dan tambahkan subtotalnya
            if (isset($item['products'])) {
                foreach ($item['products'] as $product) {
                    if (isset($product['subtotal'])) {
                        $totalLabaKotor1 += $product['subtotal'];
                    }
                }
            }
        }

        $totalLabaKotor = $totalLabaKotor1 - $totalPenjualanBersih; // - $totalHargaPokok
        $totalLabaKotor2 = $totalPenjualanBersih;
        //CHART 
        
        // data untuk chart
        $labels = [];
        $data = [];

        foreach ($transaction as $item) {
            // ambil tanggal transaksi
            $tanggalTransaksi = $item['tanggal'];
        
            // ambil total penjualan bersih jika kunci 'grandtotal' ada dalam item
            $totalPenjualanBersih = isset($item['grandtotal']) ? $item['grandtotal'] : 0;
        
            // tambah tanggal -> label chart
            $labels[] = $tanggalTransaksi;
        
            // tambah total penjualan bersih -> data chart
            $data[] = $totalPenjualanBersih;
        }

        return view('Management.Report.Index',[
            'title' => 'Ringkasan Penjualan',
            'active'=> 'Ringkasan Penjualan',
            'totalPenjualanKotor' => $totalPenjualanKotor,
            'totalDiskon' => $totalDiskon,
            'totalPenjualanBersih' => $totalPenjualanBersih,
            'totalLabaKotor' => $totalLabaKotor,
            'totalLabaKotor1' => $totalLabaKotor1,
            'totalLabaKotor2' => $totalLabaKotor2,
            'totalHargaPokok' => $totalHargaPokok,
            'products' => $products,
            'categories' => $categories,
            'pajak' => $pajak,
            'discounts' => $discounts,
            'transaction' => $transaction,
            'labels' => json_encode($labels),
            'data' => json_encode($data), 
            ],compact('userData'));
    }
    public function laporanKategori(Request $request)
    {
        $url = $this->managementCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();
        $products = $this->database->getReference($this->tableproduct)->getValue();
    $categories = $this->database->getReference($this->tablecategorie)->getValue();
    $pajak = $this->database->getReference($this->tablepajak)->getValue();
    $discounts = $this->database->getReference($this->tablediskon)->getValue();
    $transaction = $this->database->getReference($this->tabletransaksi)->getValue();

        // Filter transaksi yang memiliki status "Bayar"
        $transaction = array_filter($transaction, function ($item) {
            return $item['status'] === 'Lunas';
        });

           // Jika ada tanggal awal dan akhir yang dikirimkan melalui request
           if ($request->filled('startDate') && $request->filled('endDate')) {
            $startDate = $request->input('startDate');
            $endDate = $request->input('endDate');
    
            // Filter transaksi berdasarkan rentang tanggal
            $filteredTransaction = [];
            foreach ($transaction as $item) {
                $tanggalTransaksi = $item['tanggal'];
                if ($tanggalTransaksi >= $startDate && $tanggalTransaksi <= $endDate) {
                    $filteredTransaction[] = $item;
                }
            }
    
            // Gunakan transaksi yang sudah difilter
            $transaction = $filteredTransaction;
        }

        
        return view('Management.Report.PenjualanBerdasarkanKategori',[
            'title' => 'Penjualan Berdasarkan Kategori',
            'active'=> 'Penjualan Berdasarkan Kategori',
            'products' => $products,
            'categories' => $categories,
            'pajak' => $pajak,
            'discounts' => $discounts,
            'transaction' => $transaction,
            'userData' => $userData
            ]);
    }
    public function laporanBarang(Request $request)
    {
        $url = $this->managementCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();
        $products = $this->database->getReference($this->tableproduct)->getValue();
        $categories = $this->database->getReference($this->tablecategorie)->getValue();
        $pajak = $this->database->getReference($this->tablepajak)->getValue();
        $discounts = $this->database->getReference($this->tablediskon)->getValue();
        $transaction = $this->database->getReference($this->tabletransaksi)->getValue();

            // Filter transaksi yang memiliki status "Bayar"
            $transaction = array_filter($transaction, function ($item) {
                return $item['status'] === 'Lunas';
            });

        // Jika ada tanggal awal dan akhir yang dikirimkan melalui request
        if ($request->filled('startDate') && $request->filled('endDate')) {
            $startDate = $request->input('startDate');
            $endDate = $request->input('endDate');
    
            // Filter transaksi berdasarkan rentang tanggal
            $filteredTransaction = [];
            foreach ($transaction as $item) {
                $tanggalTransaksi = $item['tanggal'];
                if ($tanggalTransaksi >= $startDate && $tanggalTransaksi <= $endDate) {
                    $filteredTransaction[] = $item;
                }
            }
    
            // Gunakan transaksi yang sudah difilter
            $transaction = $filteredTransaction;
        }
    
        
        // Hitung jumlah barang terjual untuk setiap produk
        $salesData = [];
        foreach ($products as $product) {
            $jumlahBarangTerjual = 0;
            foreach ($transaction as $item) {
                foreach ($item['products'] as $transProduct) {
                    if ($transProduct['name'] === $product['name']) {
                        $jumlahBarangTerjual += $transProduct['kuantitas'];
                    }
                }
            }
            $salesData[] = ['name' => $product['name'], 'kuantitas' => $jumlahBarangTerjual];
        }
    
        // Urutkan produk berdasarkan jumlah barang terjual secara menurun
        usort($salesData, function ($a, $b) {
            return $b['kuantitas'] <=> $a['kuantitas'];
        });
    
        // Ambil 5 produk teratas
        $topSales = array_slice($salesData, 0, 5);
    
        //GRAFIK 
    
        // Persiapkan data penjualan barang
        $salesData = [];
        foreach ($transaction as $item) {
            $tanggalTransaksi = $item['tanggal']; // Sesuaikan dengan nama key pada data transaksi
            foreach ($item['products'] as $product) {
                $namaProduk = $product['name']; // Sesuaikan dengan nama key pada data produk
                $jumlahTerjual = $product['kuantitas']; // Sesuaikan dengan nama key pada data produk
                if (!isset($salesData[$tanggalTransaksi][$namaProduk])) {
                    $salesData[$tanggalTransaksi][$namaProduk] = 0;
                }
                $salesData[$tanggalTransaksi][$namaProduk] += $jumlahTerjual;
            }
        }
    
        // Persiapkan data untuk grafik
        $labels = array_keys($salesData);
        $datasets = [];
        foreach ($products as $product) {
            $data = [];
            foreach ($salesData as $tanggal => $sales) {
                $data[] = isset($sales[$product['name']]) ? $sales[$product['name']] : 0;
            }
            $datasets[] = [
                'label' => $product['name'],
                'data' => $data,
                'backgroundColor' => 'rgba(' . rand(0, 255) . ', ' . rand(0, 255) . ', ' . rand(0, 255) . ', 0.2)',
                'borderColor' => 'rgba(' . rand(0, 255) . ', ' . rand(0, 255) . ', ' . rand(0, 255) . ', 1)',
                'borderWidth' => 1,
            ];
        }

        return view('Management.Report.PenjualanBerdasarkanBarang',[
            'title' => 'Penjualan Berdasarkan Barang',
            'active'=> 'Penjualan Berdasarkan Barang',
            'products' => $products,
            'categories' => $categories,
            'pajak' => $pajak,
            'discounts' => $discounts,
            'transaction' => $transaction,
            'topSales' => $topSales,
            'labels' => json_encode($labels),
            'datasets' => json_encode($datasets),
            'userData' => $userData,
            ]);
    }
    public function laporanKaryawan()
    {
        $url = $this->managementCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();

        return view('Management.Report.PenjualanBerdasarkanKaryawan',[
            'title' => 'Penjualan Berdasarkan Karyawan',
            'active'=> 'Penjualan Berdasarkan Karyawan'
        ],compact('userData'));
    }
    public function laporanJenisPembayaran(Request $request)
    {
        $url = $this->managementCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();
        $products = $this->database->getReference($this->tableproduct)->getValue();
        $categories = $this->database->getReference($this->tablecategorie)->getValue();
        $pajak = $this->database->getReference($this->tablepajak)->getValue();
        $discounts = $this->database->getReference($this->tablediskon)->getValue();
        $transaction = $this->database->getReference($this->tabletransaksi)->getValue();

        // Filter transaksi yang memiliki status "Bayar"
        $transaction = array_filter($transaction, function ($item) {
            return $item['status'] === 'Lunas';
        });

                // Jika ada tanggal awal dan akhir yang dikirimkan melalui request
                if ($request->filled('startDate') && $request->filled('endDate')) {
                    $startDate = $request->input('startDate');
                    $endDate = $request->input('endDate');
            
                    // Filter transaksi berdasarkan rentang tanggal
                    $filteredTransaction = [];
                    foreach ($transaction as $item) {
                        $tanggalTransaksi = $item['tanggal'];
                        if ($tanggalTransaksi >= $startDate && $tanggalTransaksi <= $endDate) {
                            $filteredTransaction[] = $item;
                        }
                    }
            
                    // Gunakan transaksi yang sudah difilter
                    $transaction = $filteredTransaction;
                }

        return view('Management.Report.PenjualanBerdasarkanJenisPembayaran',[
            'title' => 'Penjualan Berdasarkan Jenis Pembayaran',
            'active'=> 'Penjualan Berdasarkan Jenis Pembayaran',
            'products' => $products,
            'categories' => $categories,
            'pajak' => $pajak,
            'discounts' => $discounts,
            'transaction' => $transaction,
            'userData' => $userData
            ]);
    }
    public function laporanDiskon(Request $request)
    {
        $url = $this->managementCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();
        $products = $this->database->getReference($this->tableproduct)->getValue();
        $categories = $this->database->getReference($this->tablecategorie)->getValue();
        $pajak = $this->database->getReference($this->tablepajak)->getValue();
        $discounts = $this->database->getReference($this->tablediskon)->getValue();
        $transaction = $this->database->getReference($this->tabletransaksi)->getValue();

            // Filter transaksi yang memiliki status "Bayar"
            $transaction = array_filter($transaction, function ($item) {
                return $item['status'] === 'Lunas';
            });

        //Filter
                // Jika ada tanggal awal dan akhir yang dikirimkan melalui request
                if ($request->filled('startDate') && $request->filled('endDate')) {
                    $startDate = $request->input('startDate');
                    $endDate = $request->input('endDate');
            
                    // Filter transaksi berdasarkan rentang tanggal
                    $filteredTransaction = [];
                    foreach ($transaction as $item) {
                        $tanggalTransaksi = $item['tanggal'];
                        if ($tanggalTransaksi >= $startDate && $tanggalTransaksi <= $endDate) {
                            $filteredTransaction[] = $item;
                        }
                    }
            
                    // Gunakan transaksi yang sudah difilter
                    $transaction = $filteredTransaction;
                }
        //End Filter

        // hitung diskon
        $totalDiskon = 0;
        foreach ($transaction as $item) {
            if (isset($item['discount'])) {
                foreach ($item['discount'] as $discount) {
                    if (isset($discount['nominal'])) {
                        $totalDiskon += $discount['nominal'];
                    }
                }
            }
        }

        //Hitung semua struk
        $totalPenjualanKotor = 0;
        foreach ($transaction as $item) {
            if (isset($item['totalBayar'])) {
                $totalPenjualanKotor += $item['totalBayar'];
            }
        }


        // Hitung penjualan
        $totalPenjualanBersih = 0;
        foreach ($transaction as $item) {
            if (isset($item['grandtotal'])) {
                $totalPenjualanBersih += $item['grandtotal'];
            }
        }

        // Hitung total pengembalian
        $totalPengembalian = 0;
        foreach ($transaction as $item) {
            if (isset($item['uangCustomer']) && isset($item['totalBayar'])) {
                $totalPengembalian += $item['uangCustomer'] - $item['totalBayar'];
            }
        }
        return view('Management.Report.Diskon',[
            'title' => 'Penjualan Berdasarkan Diskon',
            'active'=> 'Penjualan Berdasarkan Diskon',
            'products' => $products,
            'categories' => $categories,
            'pajak' => $pajak,
            'discounts' => $discounts,
            'transaction' => $transaction,
            'totalPenjualanBersih' => $totalPenjualanBersih,
            'totalPenjualanKotor' => $totalPenjualanKotor,
            'totalPengembalian' => $totalPengembalian,
            'totalDiskon' => $totalDiskon,
            'userData' => $userData
            ],compact('userData'));
    }
    public function laporanPajak(Request $request)
    {
        $url = $this->managementCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();
        $products = $this->database->getReference($this->tableproduct)->getValue();
        $categories = $this->database->getReference($this->tablecategorie)->getValue();
        $pajak = $this->database->getReference($this->tablepajak)->getValue();
        $discounts = $this->database->getReference($this->tablediskon)->getValue();
        $transaction = $this->database->getReference($this->tabletransaksi)->getValue();

            // Filter transaksi yang memiliki status "Bayar"
            $transaction = array_filter($transaction, function ($item) {
                return $item['status'] === 'Lunas';
            });
        
        //Filter
                // Jika ada tanggal awal dan akhir yang dikirimkan melalui request
                if ($request->filled('startDate') && $request->filled('endDate')) {
                    $startDate = $request->input('startDate');
                    $endDate = $request->input('endDate');
            
                    // Filter transaksi berdasarkan rentang tanggal
                    $filteredTransaction = [];
                    foreach ($transaction as $item) {
                        $tanggalTransaksi = $item['tanggal'];
                        if ($tanggalTransaksi >= $startDate && $tanggalTransaksi <= $endDate) {
                            $filteredTransaction[] = $item;
                        }
                    }
            
                    // Gunakan transaksi yang sudah difilter
                    $transaction = $filteredTransaction;
                }
        //End Filter

        // Hitung penjualan
        $totalPenjualanBersih = 0;
        foreach ($transaction as $item) {
            if (isset($item['grandtotal'])) {
                $totalPenjualanBersih += $item['grandtotal'];
            }
        }        


        // Hitung pendapatan kena pajak dan jumlah pajak
        $totalPenjualanKotor = 0;
        
        foreach ($transaction as $item) {
            if (isset($item['totalBayar'])) {
                $totalPenjualanKotor += $item['totalBayar'];
            }
        }
    
        // Hitung jumlah total pajak dari seluruh transaksi
        $totalPajak = 0;
        foreach ($transaction as $item) {
            if (isset($item['tax']) && isset($item['tax']['nominal'])) {
                $totalPajak += $item['tax']['nominal']; // Menambahkan nominal pajak dari setiap transaksi ke total pajak
            }
        }

        // hitung kena pajak
        $kenaPajak = 0;
        foreach ($transaction as $item) {
            if (isset($item['tax'])) {
                foreach ($item['tax'] as $tax) {
                    if (isset($tax['nominal'])) {
                        $kenaPajak += $tax['nominal'];
                    }
                }
            }
        }
        return view('Management.Report.Pajak',[
            'title' => 'Penjualan Berdasarkan Pajak',
            'active'=> 'Penjualan Berdasarkan Pajak',
            'products' => $products,
            'categories' => $categories,
            'pajak' => $pajak,
            'discounts' => $discounts,
            'transaction' => $transaction,
            'totalPenjualanBersih' => $totalPenjualanBersih,
            'totalPenjualanKotor' => $totalPenjualanKotor, // Total pendapatan kena pajak
            'totalPajak' => $totalPajak, // Menyertakan total pajak ke dalam view
            'kenaPajak' => $kenaPajak, 
            'userData' => $userData// 
            ]);
    }
    public function laporanShift()
    {
        $url = $this->managementCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };
       
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();
        $shifts = $this->database->getReference($this->shiftmaster)->getValue();
        $shiftDetails = $this->database->getReference($this->shift)->getValue();


        return view('Management.Report.Shift',[
            'title' => 'Penjualan Berdasarkan Shift',
            'active'=> 'Penjualan Berdasarkan Shift',
            
            ],compact('userData', 'shifts', 'shiftDetails'));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
