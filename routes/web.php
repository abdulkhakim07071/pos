<?php

use App\Http\Controllers\Management\ShiftController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\Kasir\HomeController;
use App\Http\Controllers\Kasir\KasirController;
use App\Http\Controllers\Kasir\ProductController;
use App\Http\Controllers\Management\HPPController;
use App\Http\Controllers\BackOffice\ViewController;
use App\Http\Controllers\Management\PajakController;
use App\Http\Controllers\Management\BarangController;
use App\Http\Controllers\Management\DiskonController;
use App\Http\Controllers\BackOffice\KaryawanController;
use App\Http\Controllers\Management\KategoriController;
use App\Http\Controllers\Management\PembayaranController;
use App\Http\Controllers\Management\TransferStokController;
use App\Http\Controllers\BackOffice\LaporanBackOfficeController;
use App\Http\Controllers\Management\LaporanManagementController;
use App\Http\Controllers\Management\LaporanPersediaanController;


//Login
Route::get('/login-backoffice', [LoginController::class, 'showLoginBackOffice'])->name('login.backoffice');
Route::get('/', [LoginController::class, 'showLoginManagement'])->name('login.management');
Route::get('/login-kasir', [LoginController::class, 'showLoginKasir'])->name('login.kasir');
Route::get('/register-management', [LoginController::class, 'showRegister'])->name('register.management');
Route::get('/logout', [LoginController::class, 'logout'])->name('logout');
Route::get('/cash', [LoginController::class, 'cash'])->name('cash');

Route::POST('/register', [LoginController::class, 'store'])->name('store.register');
Route::POST('/auth', [LoginController::class, 'auth'])->name('auth');
//get data byte
// Tambahkan rute untuk getDataByDate
Route::get('/laporan', [LaporanBackOfficeController::class, 'index'])->name('laporan.index');
Route::get('/laporan-tanggal-barang', [LaporanBackOfficeController::class, 'laporanBarang'])->name('laporantanggalbarang.index');
Route::get('/laporan-tanggal-kategori', [LaporanBackOfficeController::class, 'laporanKategori'])->name('laporantanggalkategori.index');
Route::get('/laporan-tanggal-pembayaran', [LaporanBackOfficeController::class, 'laporanJenisPembayaran'])->name('laporantanggalpembayaran.index');
Route::get('/laporan-tanggal-diskon', [LaporanBackOfficeController::class, 'laporanDiskon'])->name('laporantanggaldiskon.index');
Route::get('/laporan-tanggal-pajak', [LaporanBackOfficeController::class, 'laporanPajak'])->name('laporantanggalpajak.index');
Route::get('/laporan-tanggal-shift', [LaporanBackOfficeController::class, 'laporanPajak'])->name('laporantanggalshift.index');


// backoffice


Route::get('/ringkasan-penjualan', [LaporanBackOfficeController::class, 'index'])->name('ringkasan.penjualan');
Route::get('/laporan-kategori', [LaporanBackOfficeController::class, 'laporanKategori'])->name('laporan.kategori');
Route::get('/laporan-barang', [LaporanBackOfficeController::class, 'laporanBarang'])->name('laporan.barang');
Route::get('/laporan-karyawan', [LaporanBackOfficeController::class, 'laporanKaryawan'])->name('laporan.karyawan');
Route::get('/laporan-jenis-pembayaran', [LaporanBackOfficeController::class, 'laporanJenisPembayaran'])->name('laporan.jenis.pembayaran');
Route::get('/laporan-diskon', [LaporanBackOfficeController::class, 'laporanDiskon'])->name('laporan.diskon');
Route::get('/laporan-pajak', [LaporanBackOfficeController::class, 'laporanPajak'])->name('laporan.pajak');
Route::get('/laporan-shift', [LaporanBackOfficeController::class, 'laporanShift'])->name('laporan.shift');
Route::get('/daftar-karyawan', [App\Http\Controllers\BackOffice\KaryawanController::class, 'index'])->name('daftar.karyawan');
Route::get('/tambah-karyawan', [App\Http\Controllers\BackOffice\KaryawanController::class, 'formTambahKaryawan'])->name('tambah.karyawan');
Route::get('/view-produk', [ViewController::class, 'produk'])->name('view.produk');
Route::get('/view-diskon', [ViewController::class, 'diskon'])->name('view.diskon');
Route::get('/view-kategori', [ViewController::class, 'kategori'])->name('view.kategori');
Route::get('/view-pajak', [ViewController::class, 'pajak'])->name('view.pajak');
Route::get('/view-transaksi', [ViewController::class, 'transaksi'])->name('view.transaksi');



// store karyawan 
Route::post('/employees', [KaryawanController::class, 'store'])->name('employee.store');
Route::put('/employee/{id}', [App\Http\Controllers\BackOffice\KaryawanController::class, 'edit'])->name('employee.edit');
Route::delete('/employee/{id}/delete', [App\Http\Controllers\BackOffice\KaryawanController::class, 'delete'])->name('employee.delete');



// management
Route::get('/management-karyawan', [App\Http\Controllers\Management\KaryawanController::class, 'index'])->name('management.karyawan');
Route::get('/input-karyawan', [App\Http\Controllers\Management\KaryawanController::class, 'formTambahKaryawan'])->name('input.karyawan');
Route::put('/karyawan/{id}', [App\Http\Controllers\Management\KaryawanController::class, 'edit'])->name('karyawan.edit');
Route::post('/karyawan/{id}/update', [App\Http\Controllers\BackOffice\KaryawanController::class, 'update'])->name('karyawan.update');
Route::delete('/karyawan/{id}/delete', [App\Http\Controllers\Management\KaryawanController::class, 'delete'])->name('karyawan.delete');
//shift Master
Route::get('/shift-master', [ShiftController::class, 'index'])->name('shift.master');
Route::get('/shift-master-create', [ShiftController::class, 'show'])->name('shift.master.create');
Route::POST('/shift-create', [ShiftController::class, 'store'])->name('shift.store');
Route::POST('/shiftmaster/{id}/update', [ShiftController::class, 'update'])->name('shift.update');
Route::delete('/shift-delete/{id}', [ShiftController::class, 'delete'])->name('shift.delete');
Route::put('/shift-edit/{id}', [ShiftController::class, 'editForm'])->name('shift.master.edit');


// diskon
Route::get('/diskon', [DiskonController::class, 'index'])->name('diskon');
Route::get('/tambah-diskon', [DiskonController::class, 'formTambahDiskon'])->name('tambah.diskon');
Route::post('/discount', [DiskonController::class, 'store'])->name('discount.store');
Route::put('/discount/{id}', [DiskonController::class, 'edit'])->name('discount.edit');
Route::post('/discount/{id}/update', [DiskonController::class, 'update'])->name('discount.update');
Route::delete('/discount/{id}/delete', [DiskonController::class, 'delete'])->name('discount.delete');

// kategori 
Route::get('/kategori', [KategoriController::class, 'index'])->name('kategori');
Route::get('/tambah-kategori', [KategoriController::class, 'formTambahKategori'])->name('tambah.kategori');
Route::post('/categories', [KategoriController::class, 'store'])->name('kategori.store');
Route::put('/categories/{id}', [KategoriController::class, 'edit'])->name('kategori.edit');
Route::post('/categories/{id}/update', [KategoriController::class, 'update'])->name('kategori.update');
Route::delete('/categories/{id}/delete', [KategoriController::class, 'delete'])->name('kategori.delete');

// pajak 
Route::get('/pajak', [PajakController::class, 'index'])->name('pajak');
Route::get('/tambah-pajak', [PajakController::class, 'formTambahPajak'])->name('tambah.pajak');
Route::post('/tax', [PajakController::class, 'store'])->name('pajak.store');
Route::put('/tax/{id}', [PajakController::class, 'edit'])->name('pajak.edit');
Route::post('/tax/{id}/update', [PajakController::class, 'update'])->name('pajak.update');
Route::delete('/tax/{id}/delete', [PajakController::class, 'delete'])->name('pajak.delete');

// barang
Route::get('/produk', [BarangController::class, 'index'])->name('produk');
Route::get('/tambah-produk', [BarangController::class, 'formTambahProduk'])->name('tambah.produk');
Route::post('/products', [BarangController::class, 'store'])->name('product.store');
Route::put('/products/{id}', [BarangController::class, 'edit'])->name('product.edit');
Route::post('/products/{id}/update', [BarangController::class, 'update'])->name('product.update');
Route::delete('/products/{id}/delete', [BarangController::class, 'delete'])->name('product.delete');


// transfer stok
Route::get('/transferstok', [TransferStokController::class, 'index'])->name('transfer.stok');  
Route::get('/transferstok-tambah', [TransferStokController::class, 'Create'])->name('tambah.transfer');                                                                                                                                
Route::post('/transferstok/tambah', [TransferStokController::class, 'store'])->name('store.transfer');                                                                                                                                         
Route::put('/detail-transfer/{id}', [TransferStokController::class, 'detail'])->name('detail.transfer');                                                                                                                                         
Route::get('/transferstok/edit', [TransferStokController::class, 'Edit'])->name('edit.transfer');                                                                                                                                       
Route::get('/metode-pembayaran', [PembayaranController::class, 'index'])->name('metode.pembayaran');                                                                                                                                





// transfer stok
Route::get('/transferstok', [App\Http\Controllers\Management\TransferStokController::class, 'index'])->name('transfer.stok');  
Route::get('/transferstok/tambah', [App\Http\Controllers\Management\TransferStokController::class, 'Create'])->name('tambah.transfer');                                                                                                                                
Route::post('/transferstok/tambah', [App\Http\Controllers\Management\TransferStokController::class, 'store'])->name('store.transfer');                                                                                                                                         
Route::get('/detail/{id}', [TransferStokController::class, 'detail'])->name('detail.transfer');   
Route::get('/edit/{id}', [TransferStokController::class, 'edit'])->name('transfer.edit');  
Route::put('/edit/{id}', [TransferStokController::class, 'edit'])->name('transfer.edit');  
Route::post('/edit/{id}/update', [TransferStokController::class, 'update'])->name('transfer.update');                                                                                                                                     
Route::get('/metode-pembayaran', [App\Http\Controllers\Management\PembayaranController::class, 'index'])->name('metode.pembayaran');                                                                                                                                

//laporan persediaan
Route::get('/laporan-persediaan', [App\Http\Controllers\Management\LaporanPersediaanController::class, 'laporanPersediaan'])->name('laporan.persediaan');   
// hpp
Route::get('/harga-produksi-produk', [App\Http\Controllers\Management\HPPController::class, 'hargaProduksiProduk'])->name('harga.produksi');   


//kasir
Route::get('/kasir-transaksi', [KasirController::class, 'index'])->name('kasir.transaksi');
Route::post('/kasir-store', [KasirController::class, 'store'])->name('kasir.store');
Route::get('/kasir-pilih-varian/{id}', [KasirController::class, 'kasirVarian'])->name('kasir.varian');
Route::get('/kasir-tansaksi/{id}', [KasirController::class, 'show'])->name('kasir.show');
Route::get('/kasir-tansaksi/{id}/bayar', [KasirController::class, 'update'])->name('kasir.update');
Route::POST('/kasir-tansaksi/{id}/varians', [KasirController::class, 'updateVarians'])->name('kasir.varians');

Route::get('/Cetak-Ulang/{id}', [KasirController::class, 'cetakUlang'])->name('cetak.ulang');

Route::get('/Transaksi-Simpan', [KasirController::class, 'showSimpan'])->name('transaksi.simpan');
Route::put('/Transaksi-edit/{id}', [KasirController::class, 'edit'])->name('transaksi.edit');
Route::delete('/Transaksi-delete/{id}', [KasirController::class, 'delete'])->name('transaksi.delete');

//kasir product
Route::get('/kasir-produk', [ProductController::class, 'produk'])->name('kasir.produk');
Route::get('/kasir-diskon', [ProductController::class, 'diskon'])->name('kasir.diskon');
Route::get('/kasir-kategori', [ProductController::class, 'kategori'])->name('kasir.kategori');
Route::get('/kasir-pajak', [ProductController::class, 'pajak'])->name('kasir.pajak');


//laporanManagement
Route::get('/ringkasan-penjualan-management', [LaporanManagementController::class, 'index'])->name('ringkasan.penjualan.management');
Route::get('/laporan-kategori-management', [LaporanManagementController::class, 'laporanKategori'])->name('laporan.kategori.management');
Route::get('/laporan-barang-management', [LaporanManagementController::class, 'laporanBarang'])->name('laporan.barang.management');
Route::get('/laporan-karyawan-management', [LaporanManagementController::class, 'laporanKaryawan'])->name('laporan.karyawan.management');
Route::get('/laporan-jenis-pembayaran-management', [LaporanManagementController::class, 'laporanJenisPembayaran'])->name('laporan.jenis.pembayaran.management');
Route::get('/laporan-diskon-management', [LaporanManagementController::class, 'laporanDiskon'])->name('laporan.diskon.management');
Route::get('/laporan-pajak-management', [LaporanManagementController::class, 'laporanPajak'])->name('laporan.pajak.management');
Route::get('/laporan-shift-management', [LaporanManagementController::class, 'laporanShift'])->name('laporan.shift.management');

//ridwan


// Route::get('/', [HomeController::class, 'index']);
// Route::get('/tambah-user', [HomeController::class, 'create'])->name('tambah');
Route::get('/Pemindai', function () {
    return view('Basic.PemindaiBarcode.Index');
});
Route::get('/Sinkron', function () {
    return view('Basic.SycrnData.Index');
});
Route::get('/Bayar1', function () {
    return view('Basic.Pembayaran.Pembayaran');
});

Route::get('/Bayar2', function () {
    return view('Basic.Pembayaran.Edit');
});

Route::get('/Bayar', [PembayaranController::class,'bayar']);

Route::get('/Diskon', function () {
    return view('Basic.Diskon.Index');
});

Route::get('/KonfirmasiBayar', function () {
    return view('Basic.Pembayaran.Edit');
});

Route::get('/makanan', function () {
    return view('Basic.PengubahItem.Index');
});

// Route::get('/Makanan', 'MakananController@index');
Route::get('/minuman', 'MinumanController@Index');
// Route::get('/Pembayaran', 'PembayaranController@index');


Route::get('/pembayaran', function () {
    return view('Basic.Pembayaran.Pembayaran');
});

// Route::get('/pembayaran-2', 'PembayaranController@tahapDua');
Route::get('/pembayaran-edit', 'PembayaranController@edit');
Route::post('simpan-tiket', [PembayaranController::class, 'store']);



