@extends('BackOffice.Layouts.Main')

@section('container')
<div class="content-wrapper" style="font-family: Poppins;">
    <div class="container-fluid">
        <section class="content">
            <div class="row">
                <div class="col">
                    <br>
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">
                                <h2 style="font-weight:bold; ">Data Diskon</h2>
                            </div>
                        </div>
                        <div class="card-body text-center">
                            <table id="tabelDiskonBackoffice" class="table table-striped" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Nilai</th>
                                    <th>Total Diskon</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($discounts))

                                @foreach($discounts AS $discount => $item)
                                <tr>
                                    <td>{{ $item['name'] }}</td>
                                    <td>
                                        @if( $item['type'] == 'jumlah' )
                                            Rp.
                                        @endif
                                        {{ $item['nilai'] }}                                        
                                            @if( $item['type'] == 'Persent' )
                                            %
                                        @endif
                                        </td>
                                        @php
                                        $totalNominalDiskon = 0;
                                        foreach($transaksi as $index => $list){
                                            if($list['status'] && isset($list['discount']) ){
                                                foreach ($list['discount'] as $key => $discountValue) {
                                                    if ($discountValue['namaDiskon'] == $item['name']) {
                                                        $totalNominalDiskon += $discountValue['nominal'];
                                                    }
                                                }
                                            }}
                                            
                                            
                                    @endphp
                                    <td>
                                        {{ $totalNominalDiskon }}                                        
                                    </td>  
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>


@endsection

@section('script')
<script>
    $(function () {
      $("#tabelDiskonBackoffice").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": [ "colvis"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
      
    });
   
  </script>
@endsection