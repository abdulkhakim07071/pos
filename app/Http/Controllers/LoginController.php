<?php

namespace App\Http\Controllers;


use Exception;
use Carbon\Carbon;

use Illuminate\Http\Request;
// use Kreait\Firebase\Auth;
use Kreait\Firebase\Contract\Auth;


use App\Http\Controllers\Controller;
use Kreait\Firebase\Contract\Database;

class LoginController extends Controller
{
    protected $database;
    protected $users;
    protected $auth;
    protected $shift;
    protected $transaksi;
    protected $shiftmaster;
    

    public function __construct(Database  $database, Auth  $auth)
    {
        
        $this->database = $database;
        $this->users = 'users';
        $this->auth = $auth;
        $this->shift = 'shift';
        $this->transaksi = 'transaksi';
        $this->shiftmaster = 'shiftMaster';

        // $auth = app('firebase.auth');

        

    }
    /**
     * Display a listing of the resource.
     */
    public function showLoginBackOffice(){
        $url = $this->loginCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Logout Terlebih dahulu');
        };
        return view('BackOffice.Login.index',[
            'title' => 'Login BackOffice',
            'active'=> 'Login BackOffice'
            ]);
    }
    public function showLoginManagement(){
        $url = $this->loginCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Logout Terlebih dahulu');
        };
        return view('Management.Login.index',[
            'title' => 'Login BackOffice',
            'active'=> 'Login BackOffice'
            ]);
    }
    public function showLoginKasir(){
        $url = $this->loginCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Logout Terlebih dahulu');
        };
        return view('Kasir.Login.index',[
            'title' => 'Login BackOffice',
            'active'=> 'Login BackOffice'
            ]);
    }
    public function showRegister(){
        $url = $this->loginCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Logout Terlebih dahulu');
        };
        return view('Management.Login.register',[
            'title' => 'Register BackOffice',
            'active'=> 'Register BackOffice'
            ]);
    }
    public function auth(Request $request)
    {
        
        $email = $request->email;
        $password = $request->password;
        $role = $request->role;

        
        try {

        $authuser = $this->auth->signInWithEmailAndPassword($email, $password);
        $firebaseUid = $authuser->firebaseUserId();
        $userData = $this->database->getReference($this->users. '/' .  $firebaseUid)->getValue();
        
        $userDataDetail = $this->database->getReference($this->users. '/' .  $firebaseUid)->getValue();
            // Jika ID pengguna cocok dengan ID pengguna yang sedang diautentikasi
            if ( $userData['role'] === $role) {
                $dataLogin = null;
                if(isset($userDataDetail['lastLogin'])){
                    $dataLogin = $this->findShiftLogin($userDataDetail['lastLogin'], $firebaseUid);
                }
                if(!isset($userDataDetail['lastlogin']) || $dataLogin == null ){
                    $dataLogin = [
                        'id' => $firebaseUid,
                        'nama' => $userData['name'],
                        'role' => $role,
                        'tanggal' => Carbon::now()->toDateString(),
                        'login' =>  Carbon::now()->toTimeString(),
                        'logout' => null,
                    ];
                    $lastLogin = Carbon::parse($dataLogin['login'] . ' ' . $dataLogin['tanggal']);
                    $login =[
                        'lastLogin' => $lastLogin,

                    ];
                    $this->database->getReference($this->shift)->push($dataLogin);
                    $this->database->getReference($this->users. '/' . $firebaseUid)->update($login);
                }
                $combinedDateTime = Carbon::parse($dataLogin['login'] . ' ' . $dataLogin['tanggal']);

                session(['user_id' => $firebaseUid]);
                session(['login' => $combinedDateTime]);
                session(['role' => $role]);
                if($role == 'Backoffice'){

                    return redirect('/ringkasan-penjualan')->with('success', 'Login berhasil!');
                }
                if($role == 'Management'){

                    return redirect('/produk')->with('success', 'Login berhasil!');
                }
                if($role == 'POS'){
                    return redirect('/kasir-transaksi')->with('success', 'Login berhasil!');
                }
                
            }else{
                throw new Exception('No user found matching the given conditions.');
            }
    } catch (\Kreait\Firebase\Auth\SignIn\FailedToSignIn $e) {
        if($role == 'Backoffice'){
            return redirect('/login-backoffice')->with('status', $e->getMessage());
        }
        if($role == 'Management'){
            return redirect('/')->with('status', $e->getMessage());
        }
        if($role == 'POS'){
            return redirect('/login-kasir')->with('status', $e->getMessage());
        }
        } catch(\Exception $e){
        if($role == 'Backoffice'){
                return redirect('/login-backoffice')->with('status', $e->getMessage());
          }
        if($role == 'Management'){
                return redirect('/')->with('status', $e->getMessage());
        }
        if($role == 'POS'){
                return redirect('/login-kasir')->with('status', $e->getMessage());
        }
    }
    }


    /**
     * Store a newly created resource in storage.
     */
    public function logout(Request $request)
    {
        try {
            $combinate = strtotime(session('login'));
    
                    // Memisahkan tanggal
            $tanggalCombinate = date('Y-m-d', $combinate);
            
                            // Memisahkan waktu
             $timeCombinate = date('H:i:s', $combinate);
            $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();

            $shiftId = $this->findShift(session('user_id') ,$tanggalCombinate , $timeCombinate);
            $dataLogin = [
                'logout' => Carbon::now()->toTimeString()
            ];
            $lastLogin = [
                'lastLogin' => null,
            ];
            $this->database->getReference($this->users. '/' . session('user_id'))->update($lastLogin);
            
            $this->database->getReference($this->shift. '/' . $shiftId )->update($dataLogin);
            session()->flush();

            // Mendapatkan instance otentikasi Firebase
            if($userData['role'] === "Backoffice"){
                
                
                return redirect('/login-backoffice')->with('status', 'Berhasil Logout' );

            }
            if($userData['role'] === "Management"){
                

                return redirect('/')->with('status', 'Berhasil Logout' );

            }
            if($userData['role'] === "POS"){
                
                return redirect('/login-kasir')->with('status', 'Berhasil Logout' );

            }
    
            // Redirect ke halaman login atau halaman lain setelah logout berhasil
    
        } catch (\Exception $e) {
            // Tangani kesalahan jika terjadi
            if($userData['role'] === "Management"){

            return redirect('/')->with('status', $e->getMessage());
            }
            if($userData['role'] === "Backoffice"){

            return redirect('/login-backoffice')->with('status', $e->getMessage());
            }
            if($userData['role'] === "POS"){

            return redirect('/login-kasir')->with('status', $e->getMessage());
            }
        }
    }
    public function store(Request $request)
    {
        try{
        $userProperties = [
            'email' => $request->email,
            'password' => $request->password,
        ];
        
        $createdUser = $this->auth->createUser($userProperties);
            $userData = [
                'idAuth'=> $createdUser->uid,
                'name' => $request->name,
                'noTelphone' => $request->nohp,
                'email' => $request->email,
                'nameStore' => $request->nameStore,
                'role' => $request->role,
                'alamat' => $request->alamat,
            ];
            $this->database->getReference('users/'. $createdUser->uid)->set($userData);


    return redirect()->route('login.management')->with('status', 'Registration successful! Please login');
    }catch(\Exception $e){
        $message = $e->getMessage();
        return redirect('/register-management')->with('error', $e->getMessage());
    };
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function cash(Request $request)
    {
        // $transactions = $this->database
        // ->getReference($this->transaksi)
        // ->orderByChild('tanggal')
        // ->startAt(now()->toDateTimeString()) // Timestamp dalam milidetik
        // ->endAt(now()->toDateTimeString())
        // ->getValue();
        $combinate = strtotime(session('login'));
    
                    // Memisahkan tanggal
        $tanggalCombinate = date('Y-m-d', $combinate);
    
                    // Memisahkan waktu
        $timeCombinate = date('H:i:s', $combinate);
        $transactions = $this->database->getReference($this->transaksi)->getValue();
        $shifts = $this->database->getReference($this->shiftmaster)->getValue();
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();
        $shiftId = $this->findShift(session('user_id'),$tanggalCombinate, $timeCombinate);
        $shiftDetail = $this->database->getReference($this->shift. '/' .  $shiftId)->getValue();


        $totalUang = 0;
        foreach($shifts as $index => $shift){
            foreach($transactions as $id => $transaksi){
                if(isset($transaksi['payment']) && session('login') >= $shift['waktuMulai'] && session('login') <= $shift['waktuSelesai']){
                    $transactionTime = strtotime($transaksi['tanggal']);
    
                    // Memisahkan tanggal
                    $tanggal = date('Y-m-d', $transactionTime);
    
                    // Memisahkan waktu
                    $waktu = date('H:i:s', $transactionTime);
                    if($transaksi['payment'] == 'Tunai' && $transaksi['status'] == 'Lunas' && $tanggal == $shiftDetail['tanggal']){
                        $start = $shiftDetail['login'];
                        $end = Carbon::now()->toTimeString();
                        if ($transactionTime >= $start && $transactionTime <=  $end) {
                            $totalUang += $transaksi['totalBayar'];
    
                    }
                }
            }
            }
        }
        $dataShift = [
            'uangPendapatanTunai' => $totalUang,
            'uangTunai' => $request->cash
        ];
        $lastLogin = [
            'lastLogin' => null,
        ];
        
        $this->database->getReference($this->shift. '/' . $shiftId )->update($dataShift);
        $this->database->getReference($this->users. '/' . session('user_id'))->update($lastLogin);
        return redirect('/logout');

    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
