@extends('Management.Layouts.Main')



@section('container')

<div class="content-wrapper">
    <div class="container-fluid">
        <section class="content">
            <div class="row">
                <div class="col-6">
                    <br>
                    @if(session('status'))
                <h5 class="alert alert-warning mb-3">{{ session('status') }}</h5>
                @endif
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">
                                <a href="{{ route('shift.master.create') }}"><button class="btn-utama">+ Tambah Shift </button></a>
                            </div>
                        </div>
                        <div class="card-body">
                            <table id="tabelDiskon" class="table table-striped text-center" style="width:100%" >
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Waktu Mulai</th>
                                    <th>Waktu Selesai</th>
                                    <th colspan="2">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($shifts))

                                @foreach($shifts AS $index => $shift)
                                <tr>
                                    <td>{{ $shift['name'] }}</td>
                                    <td>{{ $shift['waktuMulai'] }}</td>
                                    <td>{{ $shift['waktuSelesai'] }}</td>
                                    <td>
                                            <form method="POST" action="{{ route('shift.master.edit', ['id' => $index ]) }}">
                                                @csrf
                                                @method('PUT')
                                                <!-- Isi formulir untuk mengupdate produk -->
                                                <button type="submit" class="btn-utama">Edit</button>
                                            </form>
                                    </td>
                                        <td>
                                            <form method="POST" action="{{ route('shift.delete', ['id' => $index ]) }}">
                                                @csrf
                                                @method('DELETE')
                                                <!-- Isi formulir untuk mengupdate produk -->
                                                <button type="submit" class="btn-utama">Delete</button>
                                            </form>
                                        </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@endsection

@section('script')

<script>
    $(function () {
      $("#tabelDiskon").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": [ "colvis"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
      
    });
  </script>
@endsection