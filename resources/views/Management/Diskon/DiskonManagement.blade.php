@extends('Management.Layouts.Main')



@section('container')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- /.content-header -->

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1> </h1>
                    </div>

                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid" style="font-family: 'Poppins' ">
                <div class="card" style="width: 50vw;"><br><br>
                    <div class="card-body">
                        <div class="tab-content p-0 text-center">
                            <span class="rounded-circle"
                                style="width: 70px; height: 70px; background: #162E80; display: inline-flex; align-items: center; justify-content: center;">
                                <i class="fas fa-tag" style="font-size: 30px; color: #fff;"></i>
                            </span>
                        </div><br><br>
                        <div class="form-group">
                            <label>Nama Bahan</label>
                            <input class="form-control select2" style="width: 100%;" placeholder="Silakan isi disini">
                        </div>

                        <!-- radio -->
                        <div class="form-group d-flex flex-row">
                            <div class="form-group mr-5">
                                <label>Jenis</label>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio1"
                                        value="option1">
                                    <label class="form-check-label" for="inlineRadio1">Presentase</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="inlineRadioOptions" id="inlineRadio2"
                                        value="option2">
                                    <label class="form-check-label" for="inlineRadio2">Jumlah</label>
                                </div>
                            </div>
                            
                        </div>

                        <div class="form-group">
                            <label>Nilai</label>
                            <input class="form-control select2" style="width: 100%;" placeholder="Silakan isi disini">
                        </div>
                        <br><br>
                        <div class="row no-print">
                            <div class="col-12">
                                <a href="{{ route('tambah.diskon') }}">
                                    <button type="button" class="btn btn-default float-right" style="background-color:#162E80; color:white;">
                                        Simpan
                                    </button>
                                </a>
                                <a href=" ">
                                    <button type="button" class="btn btn-default float-right" style="margin-right: 5px;">
                                    Batal
                                    </button>
                                </a>
                                
                            </div>
                        </div>
                    </div>
                    <!-- /.card-body -->
                </div>



                <!-- /.row (main row) -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection





@section('script')
@endsection
