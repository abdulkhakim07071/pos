@extends('Management.Layouts.Main')



@section('container')

<div class="content-wrapper">
    <div class="container-fluid">
        <section class="content">
            <div class="row">
                <div class="col">
                    <br>
                    @if(session('status'))
                <h5 class="alert alert-warning mb-3">{{ session('status') }}</h5>
                @endif
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">
                                <a href="{{ route('tambah.produk') }}"><button class="btn-utama">+ Tambah Produk </button></a>
                                <button class="btn-second">Expor </button>
                            </div>
                        </div>
                        <div class="card-body">
                            <table id="tabelBarang" class="table table-striped" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Barang</th>
                                    <th>Harga</th>
                                    <th>Biaya</th>
                                    <th>margin</th>
                                    <th>Stok</th>
                                    <th>status</th>
                                    <th>aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($products))

                                @foreach($products AS $produk => $item)
                            <tr>
                                <td>{{ $item['name'] }}</td>
                                <td>Rp.@if(isset($item['price'])){{ $item['price'] }} @endif</td>
                                
                                <td>
                                    Rp.
                                    @if(isset($item['hpp']['harga']) )
                                    {{ $item['hpp']['harga'] }}
                                    @else
                                    @php
                                $totalHpp = 0 ;
                                @endphp
                                @foreach($item['hpp'] as $hppid => $hpp)
                                    @if($hpp['harga'])

                                    @php
                                    $totalHpp += intval($hpp['harga']);
                                    @endphp
                                    @endif
                                    @endforeach
                                        {{ $totalHpp }}
                                @endif
                                    
                                    </td>
                                <td>
                                    Rp.
                                @if(isset($item['hpp']['harga']))
                                    {{$item['price'] - $item['hpp']['harga'] }}
                                @else
                                    {{ $item['price'] - $totalHpp }}
                                @endif
                                </td>
                                <td>@if(isset($item['stok'])){{ $item['stok'] }} @endif</td>
                                @if(isset($item['stok']))
                                @if($item['stok'] == 0)
                                    <td class="poppins" style="color:red;"> Stok Habis</td>
                                @elseif($item['stok'] < $item['minStok'])
                                    <td class="poppins" style="color:rgb(244, 240, 10);"> Stok Menipis</td>
                                @else
                                    <td class="poppins" style="color:green;"> Stok Aman </td>
                                @endif
                                @else
                                <td></td>
                                @endif
                                <td>
                                    <form method="POST" action="{{ route('product.edit', ['id' => $produk ]) }}">
                                        @csrf
                                        @method('PUT')
                                        <!-- Isi formulir untuk mengupdate produk -->
                                        <button type="submit" class="btn-utama">Edit</button>
                                    </form>
                                    
                                </td>
                                <td>

                                    <form method="POST" action="{{ route('product.delete', ['id' => $produk ]) }}">
                                        @csrf
                                        @method('DELETE')
                                        <!-- Isi formulir untuk mengupdate produk -->
                                        <button type="submit" class="btn-utama">Delete</button>
                                    </form>
                                </td>
                                
                            </tr>
                            @endforeach
                            @endif
                                
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@endsection


@section('script')

<script>
    $(function () {
      $("#tabelBarang").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": [ "colvis"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
      
    });
  </script>
@endsection