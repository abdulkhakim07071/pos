<?php

namespace App\Http\Controllers\Management;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kreait\Firebase\Contract\Database;

class ShiftController extends Controller
{
    protected $database;
    protected $users;
    protected $shiftMaster;
    public function __construct(Database $database)
    {
        $this->database = $database;
        $this->users = 'users';
        $this->shiftMaster = 'shiftMaster';
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $url = $this->managementCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };

        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();
        $shifts = $this->database->getReference($this->shiftMaster)->getValue();

        return view('Management.Shift.index',[
            'title' => 'Shift Master',
            'active'=> 'Shift Master'
            ], compact('userData', 'shifts')); 
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try{
            $data= [
                'name' => $request->name,
                'waktuMulai' => $request->starttime,
                'waktuSelesai' => $request->endtime
            ];
            $this->database->getReference($this->shiftMaster)->push($data);
            return redirect( route('shift.master') )->with('status', 'Berhasil Menambahkan Shift');
        }catch(\Exception $e){
            return redirect( route('shift.master.create') )->with('status', 'Gagal Menambahkan Shift');
        }
        
    }

    /**
     * Display the specified resource.
     */
    public function show()
    {
        $url = $this->managementCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();

        return view('Management.Shift.create',[
            'title' => 'Shift Master',
            'active'=> 'Shift Master'
            ], compact('userData')); 
    }
    public function editForm(string $id)
    {
        $url = $this->managementCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();
        $shift = $this->database->getReference($this->shiftMaster. '/'. $id)->getValue();

        return view('Management.Shift.edit',[
            'title' => 'Shift Master',
            'active'=> 'Shift Master'
            ], compact('userData','shift', 'id')); 
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        try{
            $data= [
                'name' => $request->name,
                'waktuMulai' => $request->starttime,
                'waktuSelesai' => $request->endtime
            ];
            $this->database->getReference($this->shiftMaster. '/'. $id)->update($data);
            return redirect( route('shift.master') )->with('status', 'Berhasil Mengedit Shift');
        }catch(\Exception $e){
            return redirect( route('shift.master.create') )->with('status', 'Gagal Mengedit Shift');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function delete(string $id)
    {
        try{
            $this->database->getReference($this->shiftMaster . '/' . $id)->remove();
            return redirect( route('shift.master') )->with('status', 'Berhasil Menghapus Shift');
        }catch(\Exception $e){
            return redirect( route('shift.master') )->with('status', 'Berhasil Menghapus Shift');

        }

        //
    }
}
