<aside class="main-sidebar sidebar-light-primary elevation-4">
    <!-- Brand Logo -->
    <a href="index3.html" class="brand-link text-center">
      <span class="brand-text font-weight-light poppins"> Kasir</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          
          <li class="nav-item">
            <a href="{{ route('kasir.transaksi')}}" id="karyawan" class="nav-link">
            <i class="nav-icon fas fa-address-card"></i>
              <p>
                Transaksi
              </p>
            </a>
          </li> 
          <li class="nav-item">
            <a href="{{ route('transaksi.simpan')}}" id="karyawan" class="nav-link">
            <i class="nav-icon fas fa-address-card"></i>
              <p>
                Transaksi Simpan
              </p>
            </a>
          </li> 
          <li class="nav-item">
            <a href="#" id="management" class="nav-link">
            <i class=" "></i>
              <p>
                Management
                <i class="right fas fa-angle-left"></i>

              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="{{ route('kasir.produk')}}" id="viewProduk" class="nav-link">
                  <p>Produk</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('kasir.diskon')}}" id="viewDiskon" class="nav-link">
                  <p>Diskon</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('kasir.kategori')}}" id="viewKategori" class="nav-link">
                  <p>Kategori</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('kasir.pajak')}}" id="viewPajak" class="nav-link">
                  <p>Pajak</p>
                </a>
              </li>
            </ul>
          </li>                     
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
