@extends('Management.Layouts.Main')



@section('container')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- /.content-header -->

        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1> </h1>
                    </div>

                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content" style="font-family: 'Poppins' ">
            <form action="{{ route('transfer.update', ['id' => $id ]) }}" method="POST">
                @csrf
                @method('PUT')
                
                <div class="container-fluid" >
                    <!-- SELECT2 EXAMPLE -->
                    <div class="card" style="width: 50vw;"><br>
                        
                        <div class="card-body">
                            <label style="font-size: 30px; ">Kode Transfer</label><br><br>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Toko Asal</label>
                                        <input class="form-control select2" name="toko_asal" value="{{$editdata['tasal']}}" style="width: 100%;"
                                            placeholder="Silakan isi disini">
                                        {{-- <select class="form-control select2" style="width: 100%;">
                                            <option selected="selected"> </option>
                                            <option> </option>
                                            <option disabled="disabled"> </option>
                                            <option> </option>
                                            <option> </option>
                                            <option> </option>
                                            <option> </option>
                                        </select> --}}
                                    </div>
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        
                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                                <div class="col-md-6">
                                    <!-- /.form-group -->
                                    <div class="form-group">
                                        <label>Toko Tujuan</label>
                                        <input class="form-control select2" name="toko_tujuan" value="{{$editdata['ttujuan']}}" style="width: 100%;"
                                            placeholder="Silakan isi disini">
                                        {{-- <select class="form-control select2" style="width: 100%;">
                                            <option selected="selected"> </option>
                                            <option> </option>
                                            <option disabled="disabled"> </option>
                                            <option> </option>
                                            <option> </option>
                                            <option> </option>
                                            <option> </option>
                                        </select> --}}
                                    </div>
                                    <div class="form-group">
                                        

                                    </div>
                                    <!-- /.form-group -->
                                </div>
                                <!-- /.col -->
                            </div>
                            <div class="col-12 col-sm-6">
                                <div class="form-group">
                                    <label>Date:</label>
                                    <div class="input-group date" id="reservationdate" data-target-input="nearest">
                                        <input type="date" name="date" value="{{$editdata['tanggal']}}" class="form-control datetimepicker-input"
                                            data-target="#reservationdate" />
                                        <div class="input-group-append" data-target="#reservationdate" data-toggle="datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                                <!-- /.form-group -->
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label>Catatan</label>
                                    <input class="form-control select2" name="notes" value="{{$editdata['catatan']}}" style="width: 100%;"
                                        placeholder="Silakan isi disini">
                                </div>
                                <!-- /.form-group -->
                            </div>


                        </div>
                        <!-- /.card-body -->
                    </div>

                    <!-- Barang -->
                    <div class="card" style="width: 50vw;"><br><br>
                        <div class="card-header">
                            <h3 class="card-title" style="font-size: 25px; font-weight:600;">Barang</h3>

                            <div class="card-tools">
                                <h5 class="card-title"><a href=" ">IMPOR</a> IMPOR</h5>
                            </div>
                        </div>
                        
                        <div class="card-body">
                                <!-- Table row -->
                                <div class="row">
                                    <div class="col-12 table-responsive">
                                        <table class="table table-striped">
                                            <thead>
                                                <tr>
                                                    <th class="col-sm-6">Barang</th>
                                                    <th class="col-sm-3">Stok Asal</th>
                                                    <th class="col-sm-3">Stok Tujuan</th>
                                                    <th class="col-sm-12">Kuantitas</th>
                                                    <th></th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td> 
                                                        <input class="form-control select2" name="nama_barang" value="{{$editdata['namaBarang']}}" style="width: 100%;"
                                                                placeholder="Nama Barang"> <br>
                                                        <small>
                                                            <input class="form-control select2" name="kode_barang" value="{{$editdata['kodeBarang']}}" style="width: 100%;"
                                                                placeholder="Kode Barang">
                                                        </small>
                                                    </td>
                                                    <td>
                                                        <input type="text" name="jml_asal" value="{{$editdata['asalStok']}}" class="form-control" style="border: none; border-bottom: 1px solid black;">
                                                    </td>
                                                    <td>
                                                        <input type="text" name="jml_tujuan" value="{{$editdata['tujuanStok']}}" class="form-control" style="border: none; border-bottom: 1px solid black;">
                                                    </td>
                                                    <td>
                                                        <input type="text" name="jml_barang" value="{{$editdata['kuantitas']}}" class="form-control" style="border: none; border-bottom: 1px solid black;">
                                                    </td>
                                                    <td>
                                                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path fill="currentColor" d="M19 4h-3.5l-1-1h-5l-1 1H5v2h14M6 19a2 2 0 0 0 2 2h8a2 2 0 0 0 2-2V7H6z"/></svg>
                                                    </td>
                                                    
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                    <!-- /.col -->
                                </div>
                                <!-- /.row -->
                                
                        </div>
                        <!-- /.card-body -->
                        
                    </div>
                    <!-- /.row (main row) -->

                    <!-- button -->
                    <div class="row no-print" style="width: 50vw; margin-left: 10px;">
                        <div class="col-12" >
                            <a href="{{ route('rincian.transfer') }}"></a>
                                <button type="submit" class="btn btn-default float-right"
                                    style="background-color:#162E80; color:white; border-radius:5px; ">
                                    SIMPAN 
                                </button>
                            <a href="{{ route('rincian.transfer') }}">
                                <button type="submit" class="btn btn-default float-right"
                                style="background-color:#162E80; color:white; margin-right: 20px; border-radius:5px; ">
                                SIMPAN DAN TERIMA
                                </button>
                            </a>
                            
                            <button type="button" class="btn btn-default float-right" style="margin-right: 20px; border-radius:5px; ">
                                BATAL
                            </button>
                        </div>
                    </div>
                </div><!-- /.container-fluid -->
            </form>
        </section>
        
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection





@section('script')
@endsection
