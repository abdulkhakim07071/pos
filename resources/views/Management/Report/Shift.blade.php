@extends('Management.Layouts.Main')

@section('container')

<div class="content-wrapper">
  <div class="container-fluid">
 <!-- Tabel Karyawan -->
        <section class="content">
         



        {{-- end filter --}}

                   <!-- Tabel Riwayat Shift-->

                   <div class="row">
                    <section class="col">
                        <br>
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">
                                <h5 class="poppins">  SHIFT KASIR </h5>
                                </div>
                            </div>
                            <div class="card-body">
                                <table id="tabelShiftPOSBackoffice" class="table table-striped" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Tanggal</th>
                                        <th>Shift</th>
                                        <th>Pos</th>
                                        <th>Jam Buka</th>
                                        <th>Jam Tutup</th>
                                        <th>Jumlah Uang Tunai Yang diharapkan</th>
                                        <th>Jumlah Uang Tunai Sebenarnya</th>
                                        <th>Selisih</th>
                                    </tr>
                                </thead>
                                <tbody class="text-center">
                                  @foreach($shiftDetails as $indexDetail => $detail)
                                  @foreach($shifts as $indexShift => $shift)
                                  
@if(is_array($detail) && is_array($shift) && isset($detail['login']) && isset($shift['waktuMulai']) && isset($shift['waktuSelesai']) && $detail['login'] >= $shift['waktuMulai'] && $detail['login'] <= $shift['waktuSelesai'] && $detail['role'] == 'POS')
                                  
                                  <tr>
                                    <td>@if(isset($detail['tanggal'] )){{ $detail['tanggal']  }} @endif</td>
                                    <td>{{ $shift['name'] }}</td>
                                    <td>@if(isset($detail['nama'] )){{ $detail['nama']  }} @endif</td>
                                    <td>@if(isset($detail['login'] )){{ $detail['login']  }} @endif</td>
                                    <td>@if(isset($detail['logout'] )){{ $detail['logout']  }} @endif</td>
                                    <td>@if(isset($detail['uangPendapatanTunai'] )){{ $detail['uangPendapatanTunai']  }} @endif</td>
                                    <td>@if(isset($detail['uangTunai'] )){{ $detail['uangTunai']  }} @endif</td>
<td>
    @if(isset($detail['uangPendapatanTunai']) && isset($detail['uangTunai']))
        @php
            $uangPendapatanTunai = is_numeric($detail['uangPendapatanTunai']) ? $detail['uangPendapatanTunai'] : 0;
            $uangTunai = is_numeric($detail['uangTunai']) ? $detail['uangTunai'] : 0;
            $difference = $uangTunai - $uangPendapatanTunai;
        @endphp
        {{ $difference }}
    @endif
</td>

                                    
                                    
                                  </tr>
                                  @endif
                                  @endforeach
                                  @endforeach
                                                          
                                  </tbody>
                                </table>
                            </div>
                        </div>
                        <br>
                        <div class="card text-center" >
                            <div class="card-header">
                                <div class="card-title">
                                <h5 class="poppins">Data Management dan Backoffice</h5>
                                </div>
                            </div>
                            <div class="card-body">
                                <table id="tabelShiftBackoffice" class="table table-striped" style="width:100%">
                                    <thead>
                                    <tr>
                                        <th>Tanggal</th>
                                        <th>Login</th>
                                        <th>Logout</th>
                                        <th>Peran</th>
                                    </tr>
                                </thead>
                                <tbody >
                                  @foreach($shiftDetails as $indexDetail => $detail)
@if(is_array($detail) && $detail['role'] != "POS")
                                  <tr>
                                    <td>@if(isset($detail['nama'] )){{ $detail['nama']  }} @endif</td>
                                    <td>@if(isset($detail['login'] )){{ $detail['login']  }} @endif</td>
                                    <td>@if(isset($detail['logout'] )){{ $detail['logout']  }} @endif</td>                            
                                    <td>@if(isset($detail['role'] )){{ $detail['role']  }} @endif</td>                            
                                    
                                  </tr>
                                  @endif
                                  @endforeach
                                  {{-- @foreach($shifts as $shiftId => $shift)
        
                                  <tr>
                                    <td>@if(isset($shift['login'] )){{ $shift['login']  }} @endif</td>
                                    <td>@if(isset($shift['logout'] )){{ $shift['logout']  }} @endif</td>
                                    <td>@if(isset($shift['uangPendapatanTunai'] )){{ $shift['uangPendapatanTunai']  }} @endif</td>
                                    <td>@if(isset($shift['uangTunai'] )){{ $shift['uangTunai']  }} @endif</td>
        
                                    <td>@if(isset($shift['uangPendapatanTunai'] ) && isset($shift['uangTunai'])){{ $shift['uangTunai'] - $shift['uangPendapatanTunai']  }} @endif</td>
                                  </tr> --}}
                                  {{-- @endforeach                           --}}
                                  </tbody>
                                </table>
                            </div>
                        </div>
                    </section>
                </div>       
        </section>
  </div>
<!-- end tabel riwayat penjualan karyawan -->
    </div>
</div>
  

@endsection



  
@section('script')

<script>
  $(function () {
    $("#tabelShiftPOSBackoffice").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": [ "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    
  });
  $(function () {
    $("#tabelShiftBackoffice").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": [ "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    
  });
</script>

@endsection
