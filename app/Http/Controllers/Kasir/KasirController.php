<?php

namespace App\Http\Controllers\Kasir;


use PDF;
use Carbon\Carbon;
use Illuminate\Http\Request;
use function Laravel\Prompts\alert;

use App\Http\Controllers\Controller;
use Kreait\Firebase\Contract\Database;

class KasirController extends Controller
{
    protected $database;
    protected $tablecategorie;
    protected $tablepajak;
    protected $tableproduct;
    protected $tablediskon;
    protected $tabletransaksi;
    protected $users;
    public function __construct(Database $database)
    {
        
        $this->database = $database;
        $this->tablecategorie = 'categories';
        $this->tablepajak = 'pajak';
        $this->tableproduct = 'products';
        $this->tablediskon = 'discount';
        $this->tabletransaksi = 'transaksi';
        $this->users = 'users';
    }

    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $url = $this->kasirCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();

        $products = $this->database->getReference($this->tableproduct)->getValue();
        $categories = $this->database->getReference($this->tablecategorie)->getValue();
        $pajak = $this->database->getReference($this->tablepajak)->getValue();
        $discounts = $this->database->getReference($this->tablediskon)->getValue();


        return view('Kasir.Transaksi.Index',[
            'title' => 'Transaksi',
            'active'=> 'Transaksi',
            'products' => $products,
            'categories' => $categories,
            'pajak' => $pajak,
            'discounts' => $discounts,

            ],compact('products','categories', 'pajak', 'discounts', 'userData')); 
    }
    public function edit($id){
        $url = $this->kasirCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();

        $reference = $this->database->getReference('transaksi/' . $id);
        $snapshot = $reference->getSnapshot();
        $transaksi = $snapshot->getValue();
        $products = $this->database->getReference($this->tableproduct)->getValue();
        $categories = $this->database->getReference($this->tablecategorie)->getValue();
        $pajak = $this->database->getReference($this->tablepajak)->getValue();
        $discounts = $this->database->getReference($this->tablediskon)->getValue();

        return view('Kasir.Transaksi.Edit',[
            'title' => 'Edit Produk',
            'active'=> 'Produk',
            'data' => $transaksi,

            ], compact('transaksi', 'discounts','products','categories', 'pajak', 'id', 'userData') ); 
    }

    
    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {

        
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();
        $namaKasir = $userData['name'];
        $grandtotal = $request->grandTotal;
        $totalBayar = $request->totalBayar;
        $namaPelanggan = $request->namaPelanggan??"-" ;
        $status = $request->Status;
        if(isset( $request->productname)){
            $nameproducts = $request->productname;
            $keys = $request->key;
            $quantityproducts = $request->quantity;
            $priceproducts = $request->productprice;
            $subtotalproducts = $request->productsubtotal;};
        
        if(isset($request->discountname)){
        $namediscounts = $request->discountname;
        $valuediscounts = $request->discountvalue;
        $typediscounts = $request->discounttype;
        $nominaldiscounts = $request->discountnominal;};
        $dateTime = Carbon::now();
        $date = $dateTime->toDateTimeString();

        $products = [];
        $taxs = [];
        $discounts = [];
        
        if(isset($request->productname)) {
            foreach($nameproducts as $index => $nameproduct) {
                $references = $this->database->getReference('products/' . $keys[$index]);
                $dataproduk =$references->getSnapshot()->getValue();

                $product = [
                    'key' => $keys[$index],
                    'kuantitas' => $quantityproducts[$index],
                    'subtotal' => $subtotalproducts[$index],
                ];
                $products[] = array_merge($product, $dataproduk );

            }
        }
        if(isset($request->discountname)) {
            foreach($namediscounts as $index => $namediscount) {
                $discounts[] = [
                    'namaDiskon' => $namediscount,
                    'nilai' => $valuediscounts[$index],
                    'type' => $typediscounts[$index],
                    'nominal' => $nominaldiscounts[$index],
                    
                ];
            };
        }

        $dataTransaksi = [
            'grandtotal' => $grandtotal,
            'products' => $products,
            'discount' => $discounts,
            'tax' => $taxs,
            'totalBayar' => $totalBayar,
            'status' => $status,
            'tanggal' => $date,
            'nama' => $namaPelanggan,
            'kasir' => $namaKasir
        ];

        if(isset($request->id)){
            $this->database->getReference($this->tabletransaksi. '/' . $request->id)->update($dataTransaksi);
            $id = $request->id;
        }else{
            
        // Mendapatkan referensi ke tabel transaksi di database Firebase
                $reference = $this->database->getReference($this->tabletransaksi);
        
                // Menambahkan data transaksi ke dalam tabel dan mendapatkan ID baru
                $newPostRef = $reference->push($dataTransaksi);
                $id = $newPostRef->getKey();

        }

            // Mendapatkan data menggunakan ID
            if($status == "Bayar"){
                return redirect(route('kasir.varian',['id'=> $id]));
            }else{
                return redirect(route('transaksi.simpan',));
            }


            
        }
            /**
             * Display the specified resource.
             */
    public function show(string $id)
            {
                  $url = $this->kasirCheck();

                if ($url !== true) {
                    return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
                };
            $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();

            $references = $this->database->getReference('transaksi/' . $id);
            $dataproduk =$references->getSnapshot()->getValue();
            $taxs = $this->database->getReference('pajak/')->getSnapshot()->getValue();

            return view('Kasir.Transaksi.Bayar',[
            'title' => 'Transaksi',
            'active'=> 'Transaksi - bayar'],compact('dataproduk', 'id', 'userData', 'taxs'));
        }
    public function kasirVarian(string $id)
            {
                $url = $this->kasirCheck();

                if ($url !== true) {
                    return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
                };

            $references = $this->database->getReference('transaksi/' . $id);
            $dataproduk =$references->getSnapshot()->getValue();

            return view('Kasir.Transaksi.Varian',[
            'title' => 'Transaksi',
            'active'=> 'Transaksi - Varian'],compact('dataproduk', 'id'));
        }

    public function showSimpan(){
        $url = $this->kasirCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();

        $transaksi = $this->database->getReference('transaksi')->getValue();

        return view('Kasir.Transaksi.Simpan',[
            'title' => 'Transaksi',
            'active'=> 'Transaksi',],compact('transaksi', 'userData'));
    }



    /**
     * Update the specified resource in storage.
     */
    public function updateVarians(Request $request, string $id)
    {
        $dataTransaksi =$this->database->getReference($this->tabletransaksi. '/' . $id)->getValue();

        $quantity = $request->kuantitas;
        $indexs = $request->indexs;
        $namaVarian = $request->namevar;
        $detailProduk = $request->produkdetail;
        $namaProduk = $request->nameProduk;
        $hargaVarian = $request->pilihVarian;
        $varianPilihan = [];
        $HargaVarianProduk = 0;
            foreach ($indexs as $key => $index) {
                $varianPilihan = [];
                $HargaVarianProduk = 0;

                foreach($namaProduk as $indexproduk => $produk){
                    if($detailProduk[$index] == $produk){
                        $varianPilihan[] = [
                            'nama' => $namaVarian[$indexproduk],
                            'harga' => $hargaVarian[$indexproduk],
                        ];
                        $HargaVarianProduk += $hargaVarian[$indexproduk];
                    }
                    
                }
                $newHargaProduk = $dataTransaksi['products'][$index]['price']*$dataTransaksi['products'][$index]['kuantitas'] + $HargaVarianProduk;
                $hargaSubtotal = [
                    'subtotal' => $newHargaProduk,
                ];
                $databasePath = $this->tabletransaksi . '/' . $id . '/products/' . $index;
                $currentData = $this->database->getReference($databasePath)->getValue();
                $currentData['varianPilihan'] = $varianPilihan;
            
                $this->database->getReference($databasePath)->update($currentData);
                $this->database->getReference($databasePath)->update($hargaSubtotal);
                }

        return redirect(route('kasir.show',['id'=> $id]));

    }


        

    
    public function update(Request $request, string $id)
    {
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();
        $nameTaxs = $request->namePajak;
        $nominalTaxs = $request->nominalPajak;
        $nilaiTaxs = $request->nilaiPajak;
        $indexTaxs = $request->indexPajak;
        $taxs = [];

        if(isset($request->namePajak)) {
            foreach($nameTaxs as $index => $nameTax) {
                $taxs[] = [
                    'namaPajak' => $nameTax,
                    'nilai' => $nilaiTaxs[$index],
                    'nominal' => $nominalTaxs[$index],
                    'id' => $indexTaxs[$index],
                    
                ];
            };
        }
        $status = [
            'status' => 'Lunas',
            'payment' => $request->payment,
            'uangCustomer' => $request->uangmasuk,
            'kembalian' => $request->kembalian,
            'grandtotal' => $request->grandtotal,
            'totalBayar' => $request->totalbayar,
            'tax' => $taxs,
        ];
        $keyDiskon = $request->indexDiskon;
        $keyPajak = $request->indexPajak;
        if(isset($keyDiskon)){

            foreach($keyDiskon as $index => $key){
                $newDiskon =[
                    'nominal' => $request->nominalDiskon[$index],
                ];
                $this->database->getReference($this->tabletransaksi. '/' . $id . '/discount/' . $key )->update($newDiskon);
            }
        }
        if(isset($keyPajak)){
            foreach($keyPajak as $index => $key){
                $newPajak =[
                    'nominal' => $request->nominalPajak[$index],
                ];
                $this->database->getReference($this->tabletransaksi. '/' . $id . '/tax/' . $key )->update($newPajak);
            }
        }
        
        $postData =$this->database->getReference($this->tabletransaksi. '/' . $id)->update($status);
        $filePath = public_path("Struk/{$id}.pdf");
        if (file_exists($filePath)) {
            return redirect()->route('kasir.transaksi');
        }else{
                $barangdetails = $request->produkdetail;
                $kuantitas = $request->kuantitas;
                $keys = $request->keys;
                
                foreach($barangdetails as $key => $product){

                    $references = $this->database->getReference('products/' . $keys[$key]);
                    $produk =$references->getSnapshot()->getValue();
                    if(isset($produk['stok'])){
                        $barang=[
                            'stok' => $produk['stok']-$kuantitas[$key] 
                        ];
                    $update = $this->database->getReference('products/' . $keys[$key])->update($barang);
                    };
                }

            
                $references = $this->database->getReference('transaksi/' . $id);
                $dataproduk =$references->getSnapshot()->getValue();
            
            
                $pdf = PDF::loadView('Kasir.Transaksi.Struk', ['dataproduk' => $dataproduk , 'userData' => $userData ]);
    
                $pdf->save("Struk/$id.pdf");
    
                // Download the PDF
                return $pdf->download("$id.pdf");   
        };



    }
    public function delete(string $id )
    {


        // Append $id to the reference path
        $postData = $this->database->getReference($this->tabletransaksi . '/' . $id)->remove();

        if ($postData){
            return redirect(route('transaksi.simpan'))->with('status', 'Transaksi berhasil dihapus');
        } else {
            return redirect(route('transaksi.simpan'))->with('status', 'Transaksi gagal dihapus');
        }
    }

    public function cetakUlang($id){
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();

        $filePath = public_path("Struk/{$id}.pdf");

        if (file_exists($filePath)) {
        $references = $this->database->getReference('transaksi/' . $id);
        $dataproduk =$references->getSnapshot()->getValue();
            
            
        $pdf = PDF::loadView('Kasir.Transaksi.Struk', ['dataproduk' => $dataproduk, 'userData' => $userData]);
    
        $pdf->save("Struk/$id.pdf");
    
                // Download the PDF
        return $pdf->download("$id.pdf");  
        }else{
            return redirect()->back()->with('alert', 'cetak nota awal terlebih dahulu!');
        }
    }
    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
