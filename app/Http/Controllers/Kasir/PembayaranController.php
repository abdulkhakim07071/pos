<?php

namespace App\Http\Controllers\Kasir;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kreait\Firebase\Contract\Database;


class PembayaranController extends Controller
{

    protected $database;
    protected $tablename;
    public function __construct(Database $database)
    {
        $this->database = $database;
        $this->tablename = 'tiket';
    }

    public function index()
    {
        return view('Basic.Pembayaran.Pembayaran'); // Return view makanan.blade.php
    }

//     public function tahapDua()
// {
//     return view('Basic.Pembayaran.TahapDua');
// }

    public function edit()
    {
        return view('Basic.Pembayaran.Edit');
    }

//     public function prosesForm(Request $request)
// {
//     // Logika validasi dan pemrosesan form

//     // Simpan data ke sesi
//     session(['cart' => $dataCart]);

//     // Redirect ke halaman selanjutnya
//     return redirect('/Bayar');
// }

    public function store(Request $request)
    {
        // dd($request->all());
        $ref_tablename = 'tiket';
        
        $totalPesanan = count($request->name);

        // simpan pesanan ke sesi
        $pesanan = $request->session()->put('pesanan', $request->all());

        for ($i=0; $i < $totalPesanan; $i++) {
            $postData = [
                'nama' => $request->name[$i],
                'quantity' => $request->quantity[$i],
                'total' => $request->total[$i],
            ];
            if ($request->tombol == 'simpan') {
                # code...
                $postRef = $this->database->getReference($this->tablename)->push($postData);
            } else{
                $postRef = $this->database->getReference('transaksi')->push($postData);
            }
        }
    }

    public function bayar(Request $request){
        $data = [
            'sessions' => $request->session()->get('pesanan')
        ];

        return view('Basic.Pembayaran.Bayarawal',$data);
    }
}

