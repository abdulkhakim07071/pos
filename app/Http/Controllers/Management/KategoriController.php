<?php

namespace App\Http\Controllers\Management;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kreait\Firebase\Contract\Database;

class KategoriController extends Controller
{
    protected $tablename;
    protected $tableproducts;
    protected $users;
    public function __construct(Database $database)
    {
        
        $this->database = $database;
        $this->tablename = 'categories';
        $this->tableproducts = 'products';
        $this->users = 'users';
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $url = $this->managementCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();

        
        $categories = $this->database->getReference($this->tablename)->getValue();
        $products = $this->database->getReference($this->tableproducts)->getValue();
        return view('Management.Kategori.Index',[
            'title' => 'Kategori',
            'active'=> 'Kategori'
            ], compact('categories','products', 'userData')); 
    }
    public function formTambahKategori()
    {
        $url = $this->managementCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();

        return view('Management.Kategori.Create',[
            'title' => 'Tambah Kategori',
            'active'=> 'Kategori'
            ],compact('userData')); 
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $dataKategori= [
            'name' => $request->name,
        ];
        
        $postData= $this->database->getReference($this->tablename)->push($dataKategori);
        if($postData){
             return redirect('kategori')->with('status','Karyawan berhasil di tambahkan');
        }else{
            return redirect('tambah-kategori')->with('status','Karyawan Gagal di tambahkan');
        }
    }

    public function edit($id)
    {
        $url = $this->managementCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();


         // Mengambil data produk dari Firebase Realtime Database
         $reference = $this->database->getReference('categories/' . $id);
         $snapshot = $reference->getSnapshot();
 
        //  if (!$snapshot->exists()) {
        //      return abort(404);
        //  }
 
         $kategori = $snapshot->getValue();
        // dd($produk);
         return view('Management.Kategori.Edit',[
            'title' => 'Edit Kategori',
            'active'=> 'Kategori',
            ], compact('kategori','id','userData') ); 
 
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $dataKategori= [
            'name' => $request->name,
        ];
        
        $postData= $this->database->getReference($this->tablename . '/' . $id)->update($dataKategori);
        if($postData){
             return redirect('kategori')->with('status','Karyawan berhasil di tambahkan');
        }else{
            return redirect('kategori.edit')->with('status','Karyawan Gagal di tambahkan');
        }
    }
    public function delete(string $id )
    {
       

        // Append $id to the reference path
        $postData = $this->database->getReference($this->tablename . '/' . $id)->remove();

        if ($postData){
            return redirect('kategori')->with('status', 'Kategori berhasil dihapus');
        } else {
            return redirect('kategori')->with('status', 'Kategori gagal dihapus');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
