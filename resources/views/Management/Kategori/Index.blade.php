@extends('Management.Layouts.Main')



@section('container')

<div class="content-wrapper">
    <div class="container-fluid">
        <section class="content">
            <div class="row">
                <div class="col-6">
                    <br>
                    @if(session('status'))
                <h5 class="alert alert-warning mb-3">{{ session('status') }}</h5>
                @endif
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">
                                <a href="{{ route('tambah.kategori') }}"><button class="btn-utama">+ Tambah Kategori </button></a>
                            </div>
                        </div>
                        <div class="card-body">
                            <table id="tabelKategori" class="table table-striped" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Banyak</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($categories))
                                @foreach($categories as $item => $category)
                                <tr>
                                        <td>{{ $category['name'] }}</td>
                                        <td>{{ count(array_filter($products, function($product) use ($category) {
                                            return $product['category'] == $category['name'];
                                        })) }}</td>
                                        <td>
                                            <form method="POST" action="{{ route('kategori.edit', ['id' => $item ]) }}">
                                                @csrf
                                                @method('PUT')
                                                <!-- Isi formulir untuk mengupdate produk -->
                                                <button type="submit" class="btn-utama">Edit</button>
                                            </form>
                                        </td>
                                        <td>

                                            <form method="POST" action="{{ route('kategori.delete', ['id' => $item ]) }}">
                                                @csrf
                                                @method('DELETE')
                                                <!-- Isi formulir untuk mengupdate produk -->
                                                <button type="submit" class="btn-utama">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                                @endif

                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@endsection


@section('script')

<script>
    $(function () {
      $("#tabelKategori").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": [ "colvis"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
      
    });
  </script>
@endsection