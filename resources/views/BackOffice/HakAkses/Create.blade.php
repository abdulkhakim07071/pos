@extends('BackOffice.Layouts.Main')

@section('container')
<div class="content-wrapper">
    <div class="container-fluid">
        <br>
        <div class="row">
            <div class="col-lg-6">
               
                @if(session('status'))
                <h5 class="alert alert-warning mb-3">{{ session('status') }}</h5>
                @endif
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col text-center">
                                <svg xmlns="http://www.w3.org/2000/svg" width="125px" height="125px" class="img-circle elevation-2 fill="currentColor" class="bi bi-person-fill" viewBox="0 0 16 16">
                                    <path d="M3 14s-1 0-1-1 1-4 6-4 6 3 6 4-1 1-1 1H3Zm5-6a3 3 0 1 0 0-6 3 3 0 0 0 0 6Z"/>
                                </svg>
                            </div>
                        </div>
                    </div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('employee.store') }}">
                            @csrf

                            <div class="form-floating mb-3 row">
                                <label for="name" class="font">Nama</label>
                                <input type="text" class="form-control" id="name"name="name" placeholder="Masukkan Nama Karyawan" required>
                            </div>

                            <div class="mb-3 row align-items-center">
                                <div class="col-md-1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="30px" height="30px" fill="currentColor" class="bi bi-envelope" viewBox="0 0 16 16">
                                    <path d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4Zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2Zm13 2.383-4.708 2.825L15 11.105V5.383Zm-.034 6.876-5.64-3.471L8 9.583l-1.326-.795-5.64 3.47A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.741ZM1 11.105l4.708-2.897L1 5.383v5.722Z"/>
                                </svg>
                                </div>
                                <div class="col form-floating">
                                    <label for="email" class="font">Email address</label>
                                    <input type="email" class="form-control" id="email" name="email" placeholder="Masukkan Email Karyawan" required>
                                </div>
                            </div>
                            <div class="mb-3 row align-items-center">
                                <div class="col-md-1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="30px" height="30px" fill="currentColor" class="bi bi-telephone" viewBox="0 0 16 16">
                                    <path d="M3.654 1.328a.678.678 0 0 0-1.015-.063L1.605 2.3c-.483.484-.661 1.169-.45 1.77a17.568 17.568 0 0 0 4.168 6.608 17.569 17.569 0 0 0 6.608 4.168c.601.211 1.286.033 1.77-.45l1.034-1.034a.678.678 0 0 0-.063-1.015l-2.307-1.794a.678.678 0 0 0-.58-.122l-2.19.547a1.745 1.745 0 0 1-1.657-.459L5.482 8.062a1.745 1.745 0 0 1-.46-1.657l.548-2.19a.678.678 0 0 0-.122-.58L3.654 1.328zM1.884.511a1.745 1.745 0 0 1 2.612.163L6.29 2.98c.329.423.445.974.315 1.494l-.547 2.19a.678.678 0 0 0 .178.643l2.457 2.457a.678.678 0 0 0 .644.178l2.189-.547a1.745 1.745 0 0 1 1.494.315l2.306 1.794c.829.645.905 1.87.163 2.611l-1.034 1.034c-.74.74-1.846 1.065-2.877.702a18.634 18.634 0 0 1-7.01-4.42 18.634 18.634 0 0 1-4.42-7.009c-.362-1.03-.037-2.137.703-2.877L1.885.511z"/>
                                </svg>
                                </div>
                                <div class="form-floating col">
                                    <label for="noTelepon" class="font">No Telepone</label>
                                    <input type="text" class="form-control" id="noTelepon" name="HP" placeholder="MasukkaTelephone Karyawan" required>
                                </div>
                            </div>
                            <div class="mb-3 row align-items-center">
                                <div class="col-md-1">
                                <svg xmlns="http://www.w3.org/2000/svg" width="30px" height="30px" fill="currentColor" class="bi bi-people" viewBox="0 0 16 16">
                                    <path d="M15 14s1 0 1-1-1-4-5-4-5 3-5 4 1 1 1 1h8Zm-7.978-1A.261.261 0 0 1 7 12.996c.001-.264.167-1.03.76-1.72C8.312 10.629 9.282 10 11 10c1.717 0 2.687.63 3.24 1.276.593.69.758 1.457.76 1.72l-.008.002a.274.274 0 0 1-.014.002H7.022ZM11 7a2 2 0 1 0 0-4 2 2 0 0 0 0 4Zm3-2a3 3 0 1 1-6 0 3 3 0 0 1 6 0ZM6.936 9.28a5.88 5.88 0 0 0-1.23-.247A7.35 7.35 0 0 0 5 9c-4 0-5 3-5 4 0 .667.333 1 1 1h4.216A2.238 2.238 0 0 1 5 13c0-1.01.377-2.042 1.09-2.904.243-.294.526-.569.846-.816ZM4.92 10A5.493 5.493 0 0 0 4 13H1c0-.26.164-1.03.76-1.724.545-.636 1.492-1.256 3.16-1.275ZM1.5 5.5a3 3 0 1 1 6 0 3 3 0 0 1-6 0Zm3-2a2 2 0 1 0 0 4 2 2 0 0 0 0-4Z"/>
                                </svg>
                                </div>
                                <div class="col">
                                    <label for="Role">Pilih Role</label>
                                    <select class="form-control" name="role" id="Role" aria-label="Large select" class="font" required>
                                        <option value="POS" >POS</option>
                                    </select>
                                </div>
                            </div>
                            <div class="mb-3 row">
                                <div class="col-md-1">

                                </div>
                                <div class="form-floating col">
                                    <label for="floatingPassword" class="font">Password</label>
                                    <input type="password" class="form-control" name="password" id="floatingPassword" placeholder="Buat Password" required>
                                </div>
                            </div>
                            <div class="row">
                                <br>
                                <div class="col-md-5 ml-auto">
                                    <div class="row">
                                        <div class="col">
                                            <a href="{{ route('daftar.karyawan') }}"> <p class="btn-second"> Batal </p></a>
                                        </div>
                                        <div class="col">
                                            <button class="btn-utama" type="submit" > Simpan </button>
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection