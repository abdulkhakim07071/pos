<aside class="main-sidebar sidebar-light-primary elevation-4">
  <!-- Brand Logo -->
  <a href="index3.html" class="brand-link text-center">
    <span class="brand-text font-weight-light poppins"> BackOffice</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
        <li class="nav-item menu-open">
          <a href="#" id="laporan" class="nav-link">
            <i class="fa-solid fa-chart-column"></i>              <p>
              Laporan
              <i class="right fas fa-angle-left"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="/ringkasan-penjualan" id="ringkasanPenjualan" class="nav-link">
                <p>Ringkasan Penjualan</p>
              </a>
            </li>
            <li class="nav-item">
            <a href="{{ route('laporan.barang')}}" id="laporanBarang" class="nav-link">
                <p>Penjualan Berdasarkan Barang</p>
              </a>
            </li>
            <li class="nav-item">
            <a href="{{ route('laporan.kategori')}}" id="laporanKategori" class="nav-link">
                <p>Penjualan Berdasarkan Kategori</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('laporan.jenis.pembayaran')}}" id="laporanJenisPembayaran" class="nav-link">
                <p>Penjualan Berdasarkan Jenis Pembayaran</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('laporan.diskon')}}" id="diskon" class="nav-link">
                <p>Diskon</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('laporan.pajak')}}" id="pajak" class="nav-link">
                <p>Pajak</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('laporan.shift')}}" id="shift" class="nav-link">
                <p>Shift</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item">
          <a href="#" id="karyawan" class="nav-link">
          <i class="nav-icon fas fa-address-card"></i>
            <p>
              Karyawan
              <i class="right fas fa-angle-left"></i>

            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{ route('daftar.karyawan')}}" id="daftarKaryawan" class="nav-link">
                <p>Daftar Karyawan</p>
              </a>
            </li>
          </ul>
        </li> 
        <li class="nav-item">
          <a href="#" id="management" class="nav-link">
          <i class=" "></i>
            <p>
              Kasir
              <i class="right fas fa-angle-left"></i>

            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{ route('view.transaksi')}}" id="viewTransaksi" class="nav-link">
                <p>Transaksi</p>
              </a>
            </li>
          </ul>
        </li> 
        <li class="nav-item">
          <a href="#" id="management" class="nav-link">
          <i class=" "></i>
            <p>
              Management
              <i class="right fas fa-angle-left"></i>

            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="{{ route('view.produk')}}" id="viewProduk" class="nav-link">
                <p>Produk</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('view.diskon')}}" id="viewDiskon" class="nav-link">
                <p>Diskon</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('view.kategori')}}" id="viewKategori" class="nav-link">
                <p>Kategori</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="{{ route('view.pajak')}}" id="viewPajak" class="nav-link">
                <p>Pajak</p>
              </a>
            </li>
          </ul>
        </li>                    
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>
