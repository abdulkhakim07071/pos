@extends('Management.Layouts.Main')



@section('container')

<div class="content-wrapper">
    <div class="container-fluid">
        <section class="content">
            <div class="row">
                <div class="col-6">
                    <br>
                    @if(session('status'))
                <h5 class="alert alert-warning mb-3">{{ session('status') }}</h5>
                @endif
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">
                                <a href="{{ route('tambah.diskon') }}"><button class="btn-utama">+ Tambah Diskon </button></a>
                            </div>
                        </div>
                        <div class="card-body">
                            <table id="tabelDiskon" class="table table-striped" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Nilai</th>
                                    <th></th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($discounts))

                                @foreach($discounts AS $discount => $item)
                                <tr>
                                    <td>{{ $item['name'] }}</td>
                                    <td>
                                        @if( $item['type'] == 'jumlah' )
                                            Rp.
                                        @endif
                                        {{ $item['nilai'] }}                                        
                                            @if( $item['type'] == 'Persent' )
                                            %
                                        @endif
                                        </td>
                                        <td>
                                            <form method="POST" action="{{ route('discount.edit', ['id' => $discount ]) }}">
                                                @csrf
                                                @method('PUT')
                                                <!-- Isi formulir untuk mengupdate produk -->
                                                <button type="submit" class="btn-utama">Edit</button>
                                            </form>
                                        </td>
                                        <td>
                                            <form method="POST" action="{{ route('discount.delete', ['id' => $discount ]) }}">
                                                @csrf
                                                @method('DELETE')
                                                <!-- Isi formulir untuk mengupdate produk -->
                                                <button type="submit" class="btn-utama">Delete</button>
                                            </form>
                                        </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@endsection

@section('script')

<script>
    $(function () {
      $("#tabelDiskon").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": [ "colvis"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
      
    });
  </script>
@endsection