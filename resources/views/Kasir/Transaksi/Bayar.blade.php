<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>{{ $title }}</title>
    
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ asset('plugins/fontawesome-free/css/all.min.css')}}">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" />

  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="{{asset('plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{ asset('plugins/jqvmap/jqvmap.min.css') }}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ asset('dist/css/adminlte.min.css') }}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{ asset('plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{ asset('plugins/summernote/summernote-bs4.min.css') }}">
  <!-- Custom style -->
  <link rel="stylesheet" type="text/css" href="{{asset('css/StyleBackOffice.css')}}">
  <!-- font Poppins -->
  <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Poppins" />
  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  
  <!-- Select2 -->
  <link rel="stylesheet" href="{{ asset('plugins/select2/css/select2.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">

   <!-- daterange picker -->
   <link rel="stylesheet" href="{{ asset('plugins/daterangepicker/daterangepicker.css') }}">
   <!-- iCheck for checkboxes and radio inputs -->
   <link rel="stylesheet" href="{{ asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css') }}">

   <!-- DataTables -->
  <link rel="stylesheet" href="{{ asset('plugins/datatables-bs4/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/datatables-responsive/css/responsive.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('plugins/datatables-buttons/css/buttons.bootstrap4.min.css') }}">
  <style>
    hr {
      margin: 5px;    /* Menghapus margin atas dan bawah */
      padding: 0;   /* Menghapus padding */
    }
  </style>
</head>
<body>
    <div class="container-fluid">
     <!-- Tabel Karyawan -->
            <section class="content">
    
                       <!-- Tabel Riwayat penjualan -->
    
            <div class="row">
                <div class="col">
                    <br><br><br>
                    <div class="card">
                        <div class="card-header">
                            <div class="row align-items-center">
                                <div class="col text-center">

                                    <h1>Pesanan</h1>
                                    <br>
                                </div>

                            </div>
                            <div class="row">
                                 <div class="col">
                                    <p>Nama Produk</p>
                                </div> 
                                 <div class="col">
                                    <p>Kuantitas</p>
                                </div> 
                                 <div class="col-3">
                                    <p>harga</p>
                                </div> 
                            </div>
                        </div>
                        <form action="{{ route('kasir.update',['id' => $id]) }}">
                        <div class="card-body">
                            @php
                                $grandtotal = 0;
                                $newPajak = 0;
                                $newDiskon = 0;
                                $newTotalBayar = 0;
                            @endphp
                            @foreach($dataproduk['products'] as $index => $produkdetail)
                            <div class="row">
                                <div class="col">
                                    <div class="row">
                                        <div class="col"><p>{{ $produkdetail['name'] }}</p></div>
                                        <input type="hidden" name="produkdetail[]" value="{{ $produkdetail['name']}}" >
                                        <input type="hidden" name="keys[]" value="{{ $produkdetail['key']}}" >
                                        <div class="col"><p>{{ $produkdetail['kuantitas'] }}</p></div>
                                        <input type="hidden" name="kuantitas[]" value="{{ $produkdetail['kuantitas']}}" >
                                        <div class="col-3"><p>{{ $produkdetail['subtotal'] }}</p></div>
                                        @php

                                           $grandtotal +=  $produkdetail['subtotal'];

                                        @endphp
                                    </div>
                                    <div class="row">
                                        @foreach($produkdetail['varianPilihan'] as $index => $varian)
                                        <p><small>. {{ $varian['nama'] }}</small></p>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <hr>
                            @endforeach
                        </div>
                        <div class="card-footer">
                            <div class="row">

                                <div class="col"><p>Total</p> </div>
                                <div class="col-3"><p>{{ $grandtotal }}</p> </div>
                                <input type="hidden" name="grandtotal" value="{{ $grandtotal }}">

                            </div>
                            <hr>
                            @php
                                
                                $totalNilaiPajak = 0;
                            @endphp
                            @if(isset($taxs))
                            @foreach($taxs as $key => $tax)
                            @php
                                $newPajak = 0;
                                if($tax['type'] == "Persen"){
                                    $newPajak = $tax['nilai'] * $grandtotal / 100;
                                } else {
                                    $newPajak = $tax['nominal'];
                                }
                                $totalNilaiPajak += $newPajak;
                            @endphp


                            <div class="row">

                                <div class="col"><p>{{ $tax['name'] }}</p> </div>
                                <div class="col"><p>{{ $tax['nilai'] }}</p> </div>
                                <div class="col-3"><p>{{ $newPajak }} </p></div>
                                <input type="hidden" name="namePajak[]" value="{{ $tax['name']  }}">
                                <input type="hidden" name="nilaiPajak[]" value="{{ $tax['nilai']  }}">
                                <input type="hidden" name="nominalPajak[]" value="{{ $newPajak }}">
                                <input type="hidden" name="indexPajak[]" value="{{ $key }}">

                            </div>
                            {{-- <hr> --}}
                            @endforeach
                            @endif
                            @if(isset($dataproduk['discount']))
                            @foreach($dataproduk['discount'] as $key => $diskon)
                            @php
                            if($diskon['type'] == "Persent"){
                                $newDiskon = $diskon['nilai'] * $grandtotal / 100;
                            }else{
                                $newDiskon = $diskon['nominal'];
                            }

                            @endphp
                            <div class="row">

                                <div class="col"><p>{{ $diskon['namaDiskon'] }} </p></div>
                                <div class="col"><p>{{ $diskon['nilai'] }}</p> </div>
                                <div class="col-3"><p>{{ $newDiskon }}</p> </div>
                                <input type="hidden" name="nominalDiskon[]" value="{{ $newDiskon }}">
                                <input type="hidden" name="indexDiskon[]" value="{{ $key }}">
                            </div>
                            {{-- <hr> --}}
                            @endforeach
                            @endif
                            @php
                                $newTotalBayar = $grandtotal - $newDiskon + $totalNilaiPajak;
                            @endphp
                            <div class="row">
                                <div class="col"><p>Total Bayar</p></div>
                                <div class="col-3"><p>{{ $newTotalBayar }}</p></div>
                                <input type="hidden" name="totalbayar" value="{{ $newTotalBayar }}">

                            </div>
                            {{-- <hr> --}}
                            <div class="row">
                                <div class="col"><p>Uang</p></div>
                                <div class="col-3 uangbayar"></div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col"><p>Kembalian</p></div>
                                <div class="col-3 kembalianstruk"></div>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col">
                    <br><br><br>
                    <div class="card">
                        <div class="card-body">
                            @if(session('alert'))
                                <script>alert('{{ session('alert') }}');</script>
                            @endif

                            <div class="row">
                                <br><br><br>
                                <div class=" col text-center">
                                    <div class="price">

                                    </div>
                                    <p>jumlah keseluruhan</p>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <input type="number" class="form-control" id="uang_masuk" oninput="hitungKembalian()" name="uangmasuk" aria-describedby="uang_masuk">
                                    <small id="uang_masuk_description" class="form-text text-muted">Jumlah Uang customer</small>
                                </div>
                            </div>
                            <br><br>
                            <div class="row">
                                <div class="form-check col text-center">
                                    <input class="form-check-input" type="radio" name="payment" id="tunai" value="Tunai" checked>
                                    <label class="form-check-label" for="tunai">
                                        Tunai
                                    </label>
                                </div>
                                <div class="form-check col text-center">
                                    <input class="form-check-input" type="radio" name="payment" id="NonTunai" value="Non Tunai">
                                    <label class="form-check-label" for="NonTunai">
                                        Non Tunai
                                    </label>
                                </div>
                            </div>
                            <br><br>
                            
                        </div>
                        <div class="card-footer">
                            <div class="row">
                                <div class="col">
                                    <div class="kembalian">
    
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <p class="text-center">
                                        Kembalian
                                    </p>
                                </div>
                            </div>
                            <div class="row items-align-center">
                                <div class="col text-center">

                                    <button class="btn-utama"> Selesai </button>
                                </div>
                            </div>
                            
                        </div>

                    </div>
                    </form>
                    <div class="row">
                        <br>
                        <br>
                        <div class="col text-center">
                            <button class="btn-utama">Hapus</button>
                            
                        </div>
                        <div class="col text-center">
                            <a href="{{ route('cetak.ulang',['id' => $id]) }}"  method="GET">
                                <button class="btn-utama">Cetak Ulang</button>
                            </a>
                        </div>
                    </div>
                </div>
               
            </div>   
            </section>   
    </div>
    
</body>
<script>
    var products = @json($dataproduk);
    // var products = Object.values(arr);
    var $uangMasukInput = document.getElementById('uang_masuk');
    var $uangMasukDescription = document.getElementById('uang_masuk_description');




    var $price = document.querySelector('.price');
    var $uangbayar = document.querySelector('.uangbayar');
    var $kembalian = document.querySelector('.kembalian');
    var $kembalianstruk = document.querySelector('.kembalianstruk');
    
    $price.innerHTML = `<h1 class="text-center">{{ $newTotalBayar }}</h1>`;
    
    // var kembalian = 0;

    function hitungKembalian() {
    var uangMasuk = parseFloat($uangMasukInput.value) || 0; // Get the entered amount or default to 0
    var kembalian = uangMasuk - {{ $newTotalBayar }};
    $kembalian.innerHTML = `<input type="hidden" name="kkk" value="${kembalian.toFixed(2)}"><h1 class="text-center">${kembalian.toFixed(2)}</h1> `;

    $uangMasukDescription.innerHTML = `Jumlah Uang customer: ${uangMasuk}`;
    $uangbayar.innerHTML = `<p> ${uangMasuk} </p>`;

    $kembalianstruk.innerHTML = `<p>${kembalian.toFixed(2)}</p>`;
}



</script>
</html>




