@extends('BackOffice.Layouts.Main')

@section('container')


<div class="content-wrapper">


    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
       {{-- filter --}}
       <form action="{{ route('laporantanggalkategori.index') }}" method="GET">
        <div class="row text-center">
          <div class="col-lg-3 col-4">
            <br>
            <div class="input-group date">
                <input id="start-datepicker" type="date" class="form-control" placeholder="Pilih Tanggal Awal" name="startDate">
            </div>
        </div>
        <div class="col-lg-3 col-4">
            <br>
            <div class="input-group date">
                <input id="end-datepicker" type="date" class="form-control" placeholder="Pilih Tanggal Akhir" name="endDate">
            </div>
        </div>
        <div class="col-lg-3 col-4">
            <br>
            <button type="submit" id="filter-button" class="btn btn-primary">Filter</button>
            <a href="{{ route('laporantanggalkategori.index') }}" class="btn btn-danger">Reset Filter</a>
        </div>
      </form>
        <!-- /.form group -->
        
        {{-- <div class="col-lg-3 col-6">
          <br>
          
            <a href="{{ route('laporan.barang') }}" class="btn btn-danger">Reset Filter</a>

        </div> --}}
         <!-- /.col -->
          <!-- /.form-group -->
        <!-- /.col -->
      </div>
      {{-- end filter --}}
        <div class="row" style="padding-top: 15px">
          <div class="col-12">
            

            <div class="card">
              <div class="card-header">
                <h3 class="card-title poppins">EKSPOR</h3>
              </div>
              <!-- /.card-header -->
              <div class="card-body">
                <table id="tabelKategori" class="table table-bordered table-striped">
                  <thead>
                    <tr>
                        <th>Kategori</th>
                        <th>Barang Terjual</th>
                        <th>Penjualan Bersih</th>
                        <th>Harga Pokok</th>
                        <th>Laba Kotor</th>
                    </tr>
                </thead>
                <tbody>
                  @foreach($categories as $category)
                      <tr>
                          <td>{{ $category['name'] }}</td>
                          <td>
                              <!-- Hitung jumlah barang terjual untuk kategori ini -->
                              <?php
                              $jumlahBarangTerjual = 0;
                              foreach ($transaction as $item) {
                                  foreach ($item['products'] as $product) {
                                      if ($product['category'] === $category['name']) {
                                          $jumlahBarangTerjual += $product['kuantitas']; // Tambahkan jumlah kuantitas produk ke jumlah barang terjual
                                      }
                                  }
                              }
                              echo $jumlahBarangTerjual;
                              ?>
                          </td>
                          <td>
                              <!-- Informasi tentang penjualan bersih -->
                              <?php
                                $penjualanBersih = 0;
                                foreach ($transaction as $item) {
                                    foreach ($item['products'] as $product) {
                                        if ($product['category'] === $category['name']) {
                                            $penjualanBersih += $product['subtotal']; // Hitung subtotal per kuantitas
                                        }
                                    }
                                }
                                echo $penjualanBersih;
                              ?>
                          </td>
                          <td>
                            <?php
                            $HargaPokok = 0;
                            foreach ($transaction as $item) {
                                foreach ($item['products'] as $product) {
                                    if ($product['category'] === $category['name']) {
                                        $HargaPokok += $product['price']; // Hitung subtotal per kuantitas
                                    }
                                }
                            }
                            echo $HargaPokok;
                          ?>
                          </td>
                          <td>
                            <?php
                              $labaKotor = $penjualanBersih - $HargaPokok;
                              echo $labaKotor;
                            ?>
                          </td>
                      </tr>
                  @endforeach
              </tbody>

                  {{-- <tfoot>
                  <tr>
                    <th>Rendering engine</th>
                    <th>Browser</th>
                    <th>Platform(s)</th>
                    <th>Engine version</th>
                    <th>CSS grade</th>
                  </tr>
                  </tfoot> --}}
                </table>
              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

 
 

@endsection

@section('script')
<script>
  $(function () {
    $("#tabelKategori").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": [ "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    
  });
</script>

  

<script>

 
  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()
  
    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })
  
   
  
    //Date picker
    $('#reservationdate').datetimepicker({
        format: 'L'
    });
  
    //Date and time picker
    $('#reservationdatetime').datetimepicker({ icons: { time: 'far fa-clock' } });
  
    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )
  
    //Timepicker
    $('#timepickerstart').datetimepicker({
      format: 'LT'
    })
    //Timepicker
    $('#timepickerend').datetimepicker({
      format: 'LT'
    })
  
    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()
  
    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()
  
    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    })
  
    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    })
  
  })
  
  </script>
@endsection