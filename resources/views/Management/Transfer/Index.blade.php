@extends('Management.Layouts.Main')



@section('container')
    <div class="content-wrapper">
        <div class="container-fluid">
            <section class="content">
                <div class="row">
                    <div class="col-12">
                        <br>
                        @if (session('status'))
                            <h5 class="alert alert-warning mb-3">{{ session('status') }}</h5>
                        @endif
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">
                                    <a href="{{ route('tambah.transfer') }}"><button class="btn-utama">+ Tambah Transfer Stok
                                        </button></a>
                                </div>
                            </div>
                            <div class="card-body">
                                <table id="tabelTransfer" class="table table-striped" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>ID</th>
                                            {{-- <th>Tanggal</th> --}}
                                            <th>Kode Barang</th>
                                            <th>Nama Barang</th>
                                            <th>Stok Asal</th>
                                            <th>Stok Tujuan</th>
                                            <th>Kuantitas</th>
                                            <th>Detail</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @php $i=1; @endphp
                                        @if ($transfer)
                                            @forelse ($transfer as $key => $item)
                                                <tr>
                                                    <td>{{ $i++ }}</td>
                                                    {{-- <td>{{ $item['tanggal'] }}</td> --}}
                                                    <td>{{ $item['kodeBarang'] }}</td>
                                                    <td>{{ $item['namaBarang'] }}</td>
                                                    <td>{{ $item['asalStok'] }}</td>
                                                    <td>{{ $item['tujuanStok'] }}</td>
                                                    <td>{{ $item['kuantitas'] }}</td>
                                                    <td><a href="{{ route('detail.transfer', ['id' => $key]) }}"
                                                            class="btn btn-sm btn-default">Detail</a></td>
                                                </tr>
                                            @empty
                                                <tr>
                                                    <td colspan="7">No Record Found</td>
                                                </tr>
                                            @endforelse
                                        @else
                                            <tr>
                                                <td colspan="7">Data is null</td>
                                            </tr>
                                        @endif

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection


@section('script')
    <script>
        $(function() {
            $("#tabelTransfer").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

        });
    </script>
@endsection
