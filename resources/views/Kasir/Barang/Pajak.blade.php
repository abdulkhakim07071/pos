@extends('Kasir.Layouts.Main')

@section('container')

<div class="content-wrapper" style="font-family: Poppins;">
    <div class="container-fluid">
        <section class="content">
            <div class="row">
                <div class="col">
                    <br>
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">
                                <h2 style="font-weight:bold; ">Data Pajak</h2>
                            </div>
                        </div>
                        <div class="card-body">
                            <table id="tabelPajakKasir" class="table table-striped" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Jenis Pajak</th>
                                    <th>Tarif Pajak</th>
                                    <th>Total Pajak</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($taxs))
                                @foreach($taxs AS $tax => $item)
                                <tr>
                                    <td>{{ $item['name'] }}</td>
                                    <td>
                                        @if( $item['type'] == 'Jumlah' )
                                            Rp.
                                        @endif
                                        {{ $item['nilai'] }}                                        
                                            @if( $item['type'] == 'Persen' )
                                            %
                                        @endif
                                    </td> 
                                    <td></td>   
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

@endsection


@section('script')
<script>
    $(function () {
      $("#tabelPajakKasir").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": [ "colvis"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
      
    });
  </script>
@endsection