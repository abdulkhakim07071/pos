@extends('Management.Layouts.Main')

@section('container')

    <div class="content-wrapper">
        <section class="content">
            <div class="container-fluid">
                {{-- filter --}}
                <form action="{{ route('laporan.diskon.management') }}" method="GET">
                    <div class="row text-center">
                    <div class="col-lg-3 col-4">
                        <br>
                        <div class="input-group date">
                            <input id="start-datepicker" type="date" class="form-control" placeholder="Pilih Tanggal Awal" name="startDate">
                        </div>
                    </div>
                    <div class="col-lg-3 col-4">
                        <br>
                        <div class="input-group date">
                            <input id="end-datepicker" type="date" class="form-control" placeholder="Pilih Tanggal Akhir" name="endDate">
                        </div>
                    </div>
                    <div class="col-lg-3 col-4">
                        <br>
                        <button type="submit" id="filter-button" class="btn btn-primary">Filter</button>
                        <a href="{{ route('laporan.diskon.management') }}" class="btn btn-danger">Reset Filter</a>
                    </div>
                </form>

                    {{-- <div class="col-lg-3 col-6">
                        <br>
                            <a href="{{ route('laporan.barang') }}" class="btn btn-danger">Reset Filter</a> --}}
                
                        {{-- <p class="btn-third" data-toggle="modal" data-target="#modalWaktu">
                            <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 256 256">
                                <path fill="currentColor"
                                    d="M128 24a104 104 0 1 0 104 104A104.11 104.11 0 0 0 128 24Zm0 192a88 88 0 1 1 88-88a88.1 88.1 0 0 1-88 88Zm64-88a8 8 0 0 1-8 8h-56a8 8 0 0 1-8-8V72a8 8 0 0 1 16 0v48h48a8 8 0 0 1 8 8Z" />
                            </svg>
                            Sepanjang hari
                        </p>
                        <!-- Modal -->
                        <div class="modal fade" id="modalWaktu" role="dialog" aria-labelledby="exampleModalLabel"
                            aria-hidden="true">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body text-left">
                                        <div class="form-group clearfix">
                                            <div class="icheck-primary d-inline ">
                                                <input type="radio" id="sepanjangHari" name="r1" checked>
                                                <label for="sepanjangHari" class="poppins">
                                                    Sepanjang Hari
                                                </label>
                                            </div>
                                            <br><br>
                                            <div class="icheck-primary ">
                                                <input type="radio" id="waktuTertentu" name="r1">
                                                <label class="poppins" for="waktuTertentu">
                                                    <!-- time Picker -->
                                                    <div class="bootstrap-timepicker">
                                                        <div class="form-group">
                                                            <label>Waktu Mulai</label>

                                                            <div class="input-group date" id="timepickerstart"
                                                                data-target-input="nearest">
                                                                <input type="text"
                                                                    class="form-control datetimepicker-input"
                                                                    data-target="#timepickerstart" />
                                                                <div class="input-group-append"
                                                                    data-target="#timepickerstart"
                                                                    data-toggle="datetimepicker">
                                                                    <div class="input-group-text"><i
                                                                            class="far fa-clock"></i></div>
                                                                </div>
                                                            </div>
                                                            <!-- /.input group -->
                                                        </div>
                                                        <!-- /.form group -->
                                                    </div>
                                                    <div class="bootstrap-timepicker">
                                                        <div class="form-group">
                                                            <label>Waktu Akhir</label>

                                                            <div class="input-group date" id="timepickerend"
                                                                data-target-input="nearest">
                                                                <input type="text"
                                                                    class="form-control datetimepicker-input"
                                                                    data-target="#timepickerend" />
                                                                <div class="input-group-append" data-target="#timepickerend"
                                                                    data-toggle="datetimepicker">
                                                                    <div class="input-group-text"><i
                                                                            class="far fa-clock"></i></div>
                                                                </div>
                                                            </div>
                                                            <!-- /.input group -->
                                                        </div>
                                                        <!-- /.form group -->
                                                    </div>
                                                </label>
                                            </div>
                                        </div>

                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary">Save changes</button>
                                    </div>
                                </div>
                            </div>
                        </div> --}}
                        {{-- end modal --}}

                    </div>
                    <!-- /.col -->
                    {{-- <div class="col-lg-3 col-6 form-group">
                        <br>
                        <p class="btn-third" data-toggle="modal" data-target="#modalKaryawan">Semua Karyawan </p>
                    </div> --}}
                    <!-- /.form-group -->
                    <!-- /.col -->
                    <!-- Modal -->
                    {{-- <div class="modal fade" id="modalKaryawan" tabindex="-1" role="dialog"
                        aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="exampleModalLabel">Modal title</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body text-left">
                                    <div class="form-group clearfix">
                                        <div class="icheck-primary d-inline ">
                                            <input type="radio" id="semuaKaryawan" name="r1" checked>
                                            <label for="semuaKaryawan" class="poppins">
                                                Semua Karyawan
                                            </label>
                                        </div>
                                        <br><br>
                                        <div class="icheck-primary ">
                                            <input type="radio" id="sebagianKaryawan" name="r1">
                                            <label class="poppins" for="sebagianKaryawan">
                                                <p>Pilih Karyawan</p>
                                                <select class="select2bs4" multiple="multiple"
                                                    data-placeholder="Select a State" style="width: 100%;">
                                                    <option>Alabama</option>
                                                    <option>Alaska</option>
                                                    <option>California</option>
                                                    <option>Delaware</option>
                                                    <option>Tennessee</option>
                                                    <option>Texas</option>
                                                    <option>Washington</option>
                                                </select>
                                            </label>
                                        </div>
                                    </div>

                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    <button type="button" class="btn btn-primary">Save changes</button>
                                </div>
                            </div>
                        </div>
                    </div> --}}
                    {{-- end modal --}}



                </div>
                {{-- end filter --}}

                <!-- Small boxes (Stats box) -->
                <div class="row" style="padding-top: 15px">
                    <div class="col-lg-4 col-6">
                        <!-- small box -->
                        <div class="small-box bg-color-primary">
                            <div class="inner">
                                <div class="row  align-items-center" style="height:100px">

                                    <div class="col-4 text-center">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80"
                                            viewBox="0 0 24 24">
                                            <path fill="currentColor"
                                                d="M6 22q-1.25 0-2.125-.875T3 19v-3h3V2l1.5 1.5L9 2l1.5 1.5L12 2l1.5 1.5L15 2l1.5 1.5L18 2l1.5 1.5L21 2v17q0 1.25-.875 2.125T18 22H6Zm12-2q.425 0 .713-.288T19 19V5H8v11h9v3q0 .425.288.713T18 20ZM9 9V7h6v2H9Zm0 3v-2h6v2H9Zm8-3q-.425 0-.713-.288T16 8q0-.425.288-.713T17 7q.425 0 .713.288T18 8q0 .425-.288.713T17 9Zm0 3q-.425 0-.713-.288T16 11q0-.425.288-.713T17 10q.425 0 .713.288T18 11q0 .425-.288.713T17 12Z" />
                                        </svg>
                                    </div>
                                    <div class="col-8">

                                        <p>Semua Struk</p>

                                        <h4 class="poppins">Rp. {{ number_format($totalDiskon, 2) }}</h3>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-4 col-6">
                        <!-- small box -->
                        <div class="small-box bg-color-primary">
                            <div class="inner">
                                <div class="row  align-items-center" style="height:100px">

                                    <div class="col-4 text-center">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80"
                                            viewBox="0 0 24 24">
                                            <path fill="currentColor"
                                                d="M13.26 20.74L12 22l-1.5-1.5L9 22l-1.5-1.5L6 22l-1.5-1.5L3 22V2l1.5 1.5L6 2l1.5 1.5L9 2l1.5 1.5L12 2l1.5 1.5L15 2l1.5 1.5L18 2l1.5 1.5L21 2v11.35c-.63-.22-1.3-.35-2-.35V5H5v14h8c0 .57.1 1.22.26 1.74M6 15v2h7.35c.26-.75.65-1.42 1.19-2H6m0-2h12v-2H6v2m0-4h12V7H6v2m17 8.23l-1.16-1.41l-3.59 3.59l-1.59-1.59L15.5 19l2.75 3" />
                                        </svg>
                                    </div>
                                    <div class="col-8">

                                        <p>Penjualan</p>

                                        <h4 class="poppins">Rp. {{ number_format($totalPenjualanBersih, 2) }}</h3>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-4 col-6">
                        <!-- small box -->
                        <div class="small-box bg-color-primary">
                            <div class="inner">
                                <div class="row  align-items-center" style="height:100px">

                                    <div class="col-4 text-center">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="80" height="80"
                                            viewBox="0 0 24 24">
                                            <path fill="currentColor"
                                                d="M15 18v2h8v-2h-8m-1.74 2.74L12 22l-1.5-1.5L9 22l-1.5-1.5L6 22l-1.5-1.5L3 22V2l1.5 1.5L6 2l1.5 1.5L9 2l1.5 1.5L12 2l1.5 1.5L15 2l1.5 1.5L18 2l1.5 1.5L21 2v11.35c-.63-.22-1.3-.35-2-.35V5H5v14h8c0 .57.1 1.22.26 1.74M14.54 15c-.54.58-.93 1.25-1.19 2H6v-2h8.54M6 11h12v2H6v-2m0-4h12v2H6V7Z" />
                                        </svg>
                                    </div>
                                    <div class="col-8">

                                        <p>Pengembalian</p>

                                        <h4 class="poppins">Rp. {{ number_format($totalPengembalian, 2) }}</h4>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- ./col -->


                </div>

                <!-- end stats box -->
                <!-- /.row -->

                <!-- Tabel Riwayat penjualan Diskon-->

                <div class="row">
                    <section class="col">
                        <br>
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">
                                    <h5 class="poppins">EKSPOR </h5>
                                </div>
                            </div>
                            <div class="card-body">
                                <table id="myTable" class="table table-striped" style="width:100%">
                                    <thead>
                                        <tr>
                                            <th>Nama</th>
                                            <th>Nilai</th>
                                            <th>Pendapatan Kena Diskon</th>
                                        </tr>
                                        {{-- <tr>
                                            <th>Jenis Pembayaran</th>
                                            <th>Jumalah Pembayaran</th>
                                            <th>Total Pembayaran</th>
                                            <th>Transaksi Pengembalian Dana</th>
                                            <th>Jumlah Pengembalian Dana</th>
                                            <th>Pembayaran Bersih</th>
                                        </tr> --}}
                                    </thead>
                                    <tbody>
                                        @if(isset($discounts))
        
                                        @foreach($discounts AS $discount => $item)
                                        <tr>    
                                            <td>{{ $item['name'] }}</td>
                                            <td>
                                                @if( $item['type'] == 'jumlah' )
                                                    Rp.
                                                @endif
                                                {{ $item['nilai'] }}                                        
                                                    @if( $item['type'] == 'Persent' )
                                                    %
                                                @endif
                                            </td>
                                            <td>Rp. {{ number_format($totalDiskon, 2) }}</td>
                                        </tr>
                                        @endforeach
                                        @endif
                                    </tbody>
                                    
                                </table>
                            </div>
                        </div>
                    </section>
                </div>
            </div>

            
            

        </section>
        <!-- end tabel riwayat penjualan karyawan -->
    </div>
    </div>


@endsection


@section('script')
    <script>
        $(function() {
            $("#myTable").DataTable({
                "responsive": true,
                "lengthChange": false,
                "autoWidth": false,
                "buttons": ["colvis"]
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

        });
    </script>
    <script>
        $(function() {
            //Initialize Select2 Elements
            $('.select2').select2()

            //Initialize Select2 Elements
            $('.select2bs4').select2({
                theme: 'bootstrap4'
            })



            //Date picker
            $('#reservationdate').datetimepicker({
                format: 'L'
            });

            //Date and time picker
            $('#reservationdatetime').datetimepicker({
                icons: {
                    time: 'far fa-clock'
                }
            });

            //Date range picker
            $('#reservation').daterangepicker()
            //Date range picker with time picker
            $('#reservationtime').daterangepicker({
                timePicker: true,
                timePickerIncrement: 30,
                locale: {
                    format: 'MM/DD/YYYY hh:mm A'
                }
            })
            //Date range as a button
            $('#daterange-btn').daterangepicker({
                    ranges: {
                        'Today': [moment(), moment()],
                        'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                        'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                        'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                        'This Month': [moment().startOf('month'), moment().endOf('month')],
                        'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1,
                            'month').endOf('month')]
                    },
                    startDate: moment().subtract(29, 'days'),
                    endDate: moment()
                },
                function(start, end) {
                    $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format(
                        'MMMM D, YYYY'))
                }
            )

            //Timepicker
            $('#timepickerstart').datetimepicker({
                format: 'LT'
            })
            //Timepicker
            $('#timepickerend').datetimepicker({
                format: 'LT'
            })

            //Bootstrap Duallistbox
            $('.duallistbox').bootstrapDualListbox()

            //Colorpicker
            $('.my-colorpicker1').colorpicker()
            //color picker with addon
            $('.my-colorpicker2').colorpicker()

            $('.my-colorpicker2').on('colorpickerChange', function(event) {
                $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
            })

            $("input[data-bootstrap-switch]").each(function() {
                $(this).bootstrapSwitch('state', $(this).prop('checked'));
            })

        })
    </script>
@endsection
