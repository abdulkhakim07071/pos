@extends('Kasir.Layouts.Main')

@section('container')

<div class="content-wrapper" style="font-family: Poppins;">
    <div class="container-fluid">
        <section class="content">
            <div class="row">
                <div class="col">
                    <br>
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">
                                <h2 style="font-weight:bold; ">Data Kategori</h2>
                            </div>
                        </div>
                        <div class="card-body">
                            <table id="tabelKategoriKasir" class="table table-striped" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Banyak</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($categories))
                                @foreach($categories as $item => $category)
                                <tr>
                                        <td>{{ $category['name'] }}</td>
                                        <td>{{ count(array_filter($products, function($product) use ($category) {
                                            return $product['category'] == $category['name'];
                                        })) }}</td>
                                </tr>
                                @endforeach
                                @endif

                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

@endsection

@section('script')
<script>
    $(function () {
      $("#tabelKategoriKasir").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": [ "colvis"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
      
    });
  </script>
@endsection