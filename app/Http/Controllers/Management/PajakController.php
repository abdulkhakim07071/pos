<?php

namespace App\Http\Controllers\Management;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use FontLib\Table\Table;
use Kreait\Firebase\Contract\Database;

class PajakController extends Controller
{
    protected $tablepajak;
    protected $users;
    protected $tabletransaksi;
    public function __construct(Database $database)
    {
        
        $this->database = $database;
        $this->tablepajak = 'pajak';
        $this->users = 'users';
        $this->tabletransaksi = 'transaksi';
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $url = $this->managementCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };
        $transaksi = $this->database->getReference($this->tabletransaksi)->getValue();

        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();

        $taxs = $this->database->getReference($this->tablepajak)->getValue();

        return view('Management.Pajak.Index',[
            'title' => 'Pajak',
            'active'=> 'Pajak'
            ],compact('taxs', 'userData', 'transaksi')); 
    }
    public function formTambahPajak()
    {
        $url = $this->managementCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();

        return view('Management.Pajak.Create',[
            'title' => 'Tambah Pajak',
            'active'=> 'Pajak'
            ],compact('userData')); 
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        
        $data= [
            'name' => $request->name,
            'type' => $request->type,
            'nilai' => $request->nilai
        ];
        $postData =$this->database->getReference($this->tablepajak)->push($data);
        
        if($postData){
            return redirect('/pajak')->with('status','Produk berhasil di tambahkan');
        }else{
            return redirect('/tambah-pajak')->with('status','Produk Gagal di tambahkan');
        }
    }

    public function edit($id)
    {
        $url = $this->managementCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();

         // Mengambil data produk dari Firebase Realtime Database
         $reference = $this->database->getReference('pajak/' . $id);
         $snapshot = $reference->getSnapshot();
 
        //  if (!$snapshot->exists()) {
        //      return abort(404);
        //  }
 
         $pajak = $snapshot->getValue();
        // dd($produk);
         return view('Management.Pajak.Edit',[
            'title' => 'Edit Pajak',
            'active'=> 'Pajak',
            ], compact('pajak','id', 'userData') ); 
 
    }


    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $data= [
            'name' => $request->name,
            'type' => $request->type,
            'nilai' => $request->nilai
        ];
        $postData =$this->database->getReference($this->tablepajak . '/' . $id)->update($data);
        
        if($postData){
            return redirect('pajak')->with('status','Produk berhasil di tambahkan');
        }else{
            return redirect('tambah-pajak')->with('status','Produk Gagal di tambahkan');
        }
    }
    public function delete(string $id )
    {
       

        // Append $id to the reference path
        $postData = $this->database->getReference($this->tablepajak . '/' . $id)->remove();

        if ($postData){
            return redirect('pajak')->with('status', 'Pajak berhasil dihapus');
        } else {
            return redirect('pajak')->with('status', 'Pajak gagal dihapus');
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
