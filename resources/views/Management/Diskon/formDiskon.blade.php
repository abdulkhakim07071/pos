@extends('Management.Layouts.Main')



@section('container')

<div class="content-wrapper">
<div class="container-fluid">
 <!-- Tabel Diskon -->
        <section class="content">

                   <!-- Tabel Diskon -->

        <div class="row" style="font-family: 'Poppins' ">
            <section class="col">
                <br>
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                        <a href="{{ route('diskon.management') }}"><button class="btn-utama">+ Tambah Diskon </button></a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table id="daftarTabelDiskon" class="table table-striped" style="width:100%">
                            <thead>
                            <tr>
                                <th>Check</th>
                                <th>Nama</th>
                                <th>Nilai</th>
                            </tr>
                        </thead>
                        <tbody>
                            
                            
                        </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>      
        </section>
        </div>
<!-- end tabel diskon -->
    </div>
</div>

@endsection

@section('script')
<script>
    $(function () {
      $("#daftarTabelDiskon").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": [ "colvis"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
      
    });
  </script>

@endsection