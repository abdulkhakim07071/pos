@extends('BackOffice.Layouts.Main')



@section('container')

<div class="content-wrapper">
<div class="container-fluid">
 <!-- Tabel Karyawan -->
        <section class="content">

                   <!-- Tabel Riwayat penjualan -->

        <div class="row">
            <section class="col">
                <br>
                <div class="card">
                    <div class="card-header">
                        <div class="card-title">
                        <a href="{{ route('tambah.karyawan') }}"><button class="btn-utama">+ Tambah Karyawan </button></a>
                        </div>
                    </div>
                    <div class="card-body">
                        <table id="daftarTabelKaryawan" class="table table-striped" style="width:100%">
                            <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Email</th>
                                <th>No Telephone</th>
                                <th>Peran</th>
                                <th>aksi</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            @if(isset($employee))
                            @foreach($employee AS $employe => $item)
                            @if($item['idAuth'] != session('user_id'))
                            <tr>
                                <td>{{ $item['name'] }}</td>
                                <td>{{ $item['email'] }}</td>
                                <td>{{ $item['noTelphone'] }}</td>
                                <td>{{ $item['role'] }}</td>
                                <td>
                                    <form method="POST" action="{{ route('employee.edit', ['id' => $employe ]) }}">
                                        @csrf
                                        @method('PUT')
                                        <!-- Isi formulir untuk mengupdate produk -->
                                        <button type="submit" class="btn-utama">Edit</button>
                                    </form>
                                </td>
                                <td>
                                    <form method="POST" action="{{ route('employee.delete', ['id' => $employe ]) }}">
                                        @csrf
                                        @method('DELETE')
                                        <!-- Isi formulir untuk mengupdate produk -->
                                        <button type="submit" class="btn-utama">Delete</button>
                                    </form>
                                </td>
                                
                            </tr>
                            @endif
                            @endforeach
                            @endif
                        </tbody>
                        </table>
                    </div>
                </div>
            </section>
        </div>      
        </section>
        </div>
<!-- end tabel riwayat penjualan -->
    </div>
</div>

@endsection

@section('script')
<script>
    $(function () {
      $("#daftarTabelKaryawan").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": [ "colvis"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
      
    });
  </script>

@endsection