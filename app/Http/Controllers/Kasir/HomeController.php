<?php

namespace App\Http\Controllers\Kasir;
use App\Http\Controllers\Controller;
use Kreait\Firebase\Contract\Database;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('Basic.User.Data-user');
    }

    public function create(){
        return view('Basic.User.Tambah-user');
    }

    public function edit(){
        return view('Basic.Pembayaran.Pembayaran');
    }

    public function submit(Request $request){
        return redirect()->route('edit.pembayaran');
    }
}
