<?php

namespace App\Http\Controllers\Management;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Kreait\Firebase\Contract\Database;
use Kreait\Firebase\Database as FirebaseDatabase;
class BarangController extends Controller
{
    protected $database;
    protected $tablecategorie;
    protected $tableproduct;
    protected $users;
    public function __construct(Database $database)
    {
        
        $this->database = $database;
        $this->tablecategorie = 'categories';
        $this->tableproduct = 'products';
        $this->users = 'users';
    }
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $url = $this->managementCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();

        $products = $this->database->getReference($this->tableproduct)->getValue();

        return view('Management.Barang.Index',[
            'title' => 'Produk',
            'active'=> 'Produk',
            ],compact('products', 'userData')); 
    }
    //$categories = $this->database->getReference($this->tablecategorie)->getValue();

    public function formTambahProduk()
    {
        $url = $this->managementCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();


        $categories = $this->database->getReference($this->tablecategorie)->getValue();
        $products = $this->database->getReference($this->tableproduct)->getValue();
        return view('Management.Barang.Create',[
            'title' => 'Tambah Produk',
            'active'=> 'Produk'
            ], compact('categories','products', 'userData') ); 
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
         // Initialize variables
         $ready = $request->has('ready') ? $request->ready : false;

         // Main product data
         $data = [
             'name' => $request->name,
             'category' => $request->categorie,
             'description' => $request->description,
             'ready' => $ready,
             'price' => $request->price,
         ];
     
         // Options
         $option = $request->has('varian') ? [] : [
             'sku' => $request->sku,
             'barcode' => $request->barcode,
         ];
     
         // Stock
         $stok = $request->has('stok') ? [
             'stok' => $request->stok,
             'minStok' => $request->minStok,
         ] : [];
     
         $files = $request->file('foto_produk');
     
         if ($files) {
     
             $fileName = time().'.'.$files->getClientOriginalExtension();
     
             $files->move(public_path('img'), $fileName);
             
             $foto =[
                 'foto' => $fileName,
             ];
           
     
             
         }else{
             $foto = [];
         }
     
         
         
         // Variants
         
         $dataVarian = $request->has('varian') ? $this->processVariants($request->only('varian', 'skuVarian', 'barkodeVarian', 'priceVarian')) : [];
         
         // Composites
         $dataKomposit = $request->has('nameKomposit') ? $this->processComposites($request->only('nameKomposit', 'kuantityKomposit', 'priceKomposit')) : [];
         
         // HPP details
         $dataHpp = $request->has('priceHpp') ? ['harga' => $request->priceHpp] : ($request->has('nameHpp') ? $this->processHppDetails($request->only('nameHpp', 'kuantitasHpp', 'satuanhpp', 'hargadetailhpp')) : []);
         $dataTambahan = [
             'varians' => $dataVarian,
             'composites'=> $dataKomposit,
             'hpp'=> $dataHpp,
         ];
         $dataProduct = array_merge($stok, $option, $data, $foto, $dataTambahan);
        
        
        $postData =$this->database->getReference($this->tableproduct)->push($dataProduct);

        if($postData){
            return redirect('produk')->with('status','Produk berhasil di tambahkan');
        }else{
            return redirect('tambah-produk')->with('status','Produk Gagal di tambahkan');
        }
    }
    public function edit($id)
    {
        $url = $this->managementCheck();

        if ($url !== true) {
            return redirect('/'.$url)->with('status', 'Anda tidak memiliki akses');
        };
        $userData = $this->database->getReference($this->users. '/' .  session('user_id'))->getValue();

        $categories = $this->database->getReference($this->tablecategorie)->getValue();
        $products = $this->database->getReference($this->tableproduct)->getValue();

         // Mengambil data produk dari Firebase Realtime Database
        $reference = $this->database->getReference('products/' . $id);
        $snapshot = $reference->getSnapshot();

        if (!$snapshot->exists()) {
             // Handle jika produk tidak ditemukan
            return abort(404);
        }

        $produk = $snapshot->getValue();
        //  dd($produk);
        return view('Management.Barang.Edit',[
            'title' => 'Edit Produk',
            'active'=> 'Produk',
            'data' => $produk,

            ], compact('categories','produk', 'id','userData') ); 

    }
    public function delete(string $id )
    {


        // Append $id to the reference path
        $postData = $this->database->getReference($this->tableproduct . '/' . $id)->remove();

        if ($postData){
            return redirect('produk')->with('status', 'Produk berhasil dihapus');
        } else {
            return redirect('produk')->with('status', 'Pajak gagal dihapus');
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    
    public function update(Request $request, string $id)
    {
        // Initialize variables
        $ready = $request->has('ready') ? $request->ready : false;

    // Main product data
    $data = [
        'name' => $request->name,
        'category' => $request->categorie,
        'description' => $request->description,
        'ready' => $ready,
        'price' => $request->price,
    ];

    // Options
    $option = $request->has('varian') ? [] : [
        'sku' => $request->sku,
        'barcode' => $request->barcode,
    ];

    // Stock
    $stok = $request->has('stok') ? [
        'stok' => $request->stok,
        'minStok' => $request->minStok,
    ] : [];

    $files = $request->file('foto_produk');

    if ($files) {

        $fileName = time().'.'.$files->getClientOriginalExtension();

        $files->move(public_path('img'), $fileName);
        
        $foto =[
            'foto' => $fileName,
        ];
      

        
    }else{
        $foto = [];
    }

    
    
    // Variants
    
    $dataVarian = $request->has('varian') ? $this->processVariants($request->only('varian', 'skuVarian', 'barkodeVarian', 'priceVarian')) : [];
    
    // Composites
    $dataKomposit = $request->has('nameKomposit') ? $this->processComposites($request->only('nameKomposit', 'kuantityKomposit', 'priceKomposit')) : [];
    
    // HPP details
    $dataHpp = $request->has('priceHpp') ? ['harga' => $request->priceHpp] : ($request->has('nameHpp') ? $this->processHppDetails($request->only('nameHpp', 'kuantitasHpp', 'satuanhpp', 'hargadetailhpp')) : []);
    
    $dataTambahan = [
        'varians' => $dataVarian,
        'composites'=> $dataKomposit,
        'hpp'=> $dataHpp,
    ];
    $dataProduct = array_merge($stok, $option, $data, $foto, $dataTambahan);
   

    // Update main product data
    $postData =$this->database->getReference($this->tableproduct. '/' . $id)->update($dataProduct);
      

    // Redirect based on the result
    if ($postData) {
        return redirect('produk')->with('status', 'Produk berhasil diubah');
    } else {
        return redirect()->back()->with('status', 'Produk gagal diubah');
    }
}

// Helper methods for processing data

private function processVariants(array $varians)
{
    $dataVarian = [];
    foreach ($varians['varian'] as $index => $varian) {
        $dataVarian[] = [
            'varian' => $varian,
            'sku' => $varians['skuVarian'][$index],
            'barkode' => $varians['barkodeVarian'][$index],
            'harga' => $varians['priceVarian'][$index],
        ];
    }
    return $dataVarian;
}

private function processComposites(array $composites)
{
    $dataKomposit = [];
    foreach ($composites['nameKomposit'] as $index => $komposit) {
        $dataKomposit[] = [
            'Komposit' => $komposit,
            'kuantitas' => $composites['kuantityKomposit'][$index],
            'harga' => $composites['priceKomposit'][$index],
        ];
    }
    return $dataKomposit;
}

private function processHppDetails(array $hppDetails)
{
    $dataHpp = [];
    foreach ($hppDetails['nameHpp'] as $index => $hppDetail) {
        $dataHpp[] = [
            'nama' => $hppDetail,
            'kuantitas' => $hppDetails['kuantitasHpp'][$index],
            'satuan' => $hppDetails['satuanhpp'][$index],
            'harga' => $hppDetails['hargadetailhpp'][$index],
        ];
    }
    return $dataHpp;
}


    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
