@extends('Management.Layouts.Main')



@section('container')

<div class="content-wrapper">
    <div class="container-fluid">
        <section class="content">
            <div class="row">
                <div class="col-lg-8 col-10">
                    <br>
                    @if(session('status'))
                <h5 class="alert alert-warning mb-3">{{ session('status') }}</h5>
                @endif
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">
                                <a href="{{ route('tambah.pajak') }}"><button class="btn-utama">+ Tambah Pajak </button></a>
                            </div>
                        </div>
                        <div class="card-body">
                            <table id="tabelPajak" class="table table-striped" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Jenis Pajak</th>
                                    <th>Tarif Pajak</th>
                                    <th>Total Pajak</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($taxs))
                                @foreach($taxs AS $tax => $item)
                                <tr>
                                    <td>{{ $item['name'] }}</td>
                                    <td>
                                        @if( $item['type'] == 'Jumlah' )
                                            Rp.
                                        @endif
                                        {{ $item['nilai'] }}                                        
                                            @if( $item['type'] == 'Persen' )
                                            %
                                        @endif
                                        </td>
                                        @php
                                        $totalNominalPajak = 0;
                                        foreach($transaksi as $index => $list){
                                            if($list['status'] == 'Lunas' && isset($list['tax'])){
                                                foreach ($list['tax'] as $key => $taxValue) {
                                                    if ($taxValue['namaPajak'] == $item['name']) {
                                                        $totalNominalPajak += $taxValue['nominal'];
                                                    }
                                                }
                                            }}
                                            
                                            
                                    @endphp
                                    <td>
                                        {{ $totalNominalPajak }}                                        
                                    </td>  
                                        <td>
                                            <form method="POST" action="{{ route('pajak.edit', ['id' => $tax ]) }}">
                                                @csrf
                                                @method('PUT')
                                                <!-- Isi formulir untuk mengupdate produk -->
                                                <button type="submit" class="btn-utama">Edit</button>
                                            </form>
                                        </td>
                                        <td>

                                            <form method="POST" action="{{ route('pajak.delete', ['id' => $tax ]) }}">
                                                @csrf
                                                @method('DELETE')
                                                <!-- Isi formulir untuk mengupdate produk -->
                                                <button type="submit" class="btn-utama">Delete</button>
                                            </form>
                                        </td>
                                </tr>
                                @endforeach
                                @endif
                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
@endsection


@section('script')

<script>
    $(function () {
      $("#tabelPajak").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": [ "colvis"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
      
    });
  </script>
@endsection