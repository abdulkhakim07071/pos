@extends('Kasir.Layouts.Main')



@section('container')
<div class="content-wrapper">
    <div class="container-fluid">
     <!-- Tabel Karyawan -->
            <section class="content">
                <div class="row">
                    <div class="col">
                        <br>
                        <div class="card">
                            <div class="card-header">
                                <div class="card-title">
                                    <h2 style="font-weight:bold; ">Data Simpan Transaksi</h2>
                                </div>
                            </div>
                            <div class="card-body">
                                <table id="tabelProdukBackoffice" class="table table-striped" style="width:100%">
                                <thead>
                                    <tr>
                                        <th>Tanggal</th>
                                        <th>Harga</th>
                                        <th>Nama</th>
                                        <th>Discount</th>
                                        <th>Total Bayar</th>
                                        <th>status</th>
                                        <th colspan="2"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @if(isset($transaksi))
    
                                    @foreach($transaksi AS $key => $item)
                                    @if( $item['status'] == "Simpan")
                                    <tr>
                                        <td>{{ $item['tanggal'] }}</td>
                                        <td>{{ $item['grandtotal'] }}</td>
                                        <td>@if(isset($item['nama'])){{ $item['nama'] }} @endif</td>
                                        @if(isset($item['discount']))
                                        <td>{{ $item['discount']['0']['nominal'] }}</td>
                                        @else
                                        <td>0</td>
                                        @endif
                                        <td>{{ $item['totalBayar'] }}</td>
                                        <td>{{ $item['status'] }}</td>
                                        <td>
                                            <form method="POST" action="{{ route('transaksi.edit', ['id' => $key ]) }}">
                                                @csrf
                                                @method('PUT')
                                                <!-- Isi formulir untuk mengupdate produk -->
                                                <button type="submit" class="btn-utama">Edit</button>
                                            </form>
                                        </td> 
                                        <td>
                                            <form method="POST" action="{{ route('transaksi.delete', ['id' => $key ]) }}">
                                                @csrf
                                                @method('delete')
                                                <!-- Isi formulir untuk mengupdate produk -->
                                                <button type="submit" class="btn-utama">Delete</button>
                                            </form>
                                        </td>                              
                                    </tr>
                                    @endif
                                @endforeach
                                @endif
                                    
                                </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                                
                </div>
            </section>
    </div>
</div>

@endsection