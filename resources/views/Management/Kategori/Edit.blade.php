@extends('Management.Layouts.Main')



@section('container')
<div class="content-wrapper">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-4 col-8">
                <br>
                @if(session('status'))
                <h5 class="alert alert-warning mb-3">{{ session('status') }}</h5>
                @endif

                <div class="card">
                    <form method="POST" action="{{ route('kategori.update',['id'=> $id]) }}">
                        @csrf
                    <div class="card-header text-center">
                        <br>
                        <br><br>
                        <svg xmlns="http://www.w3.org/2000/svg" height="46" width="46" viewBox="0 0 448 512"><!--!Font Awesome Free 6.5.1 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license/free Copyright 2023 Fonticons, Inc.--><path d="M160 112c0-35.3 28.7-64 64-64s64 28.7 64 64v48H160V112zm-48 48H48c-26.5 0-48 21.5-48 48V416c0 53 43 96 96 96H352c53 0 96-43 96-96V208c0-26.5-21.5-48-48-48H336V112C336 50.1 285.9 0 224 0S112 50.1 112 112v48zm24 48a24 24 0 1 1 0 48 24 24 0 1 1 0-48zm152 24a24 24 0 1 1 48 0 24 24 0 1 1 -48 0z"/></svg>
                        <br>
                        <br>
                        <br>
                    </div>
                    <div class="card-body">
                        <div class="form-floating mb-3 row">
                            <label for="name" class="font">Nama Kategori</label>
                            <input type="text" class="form-control" id="name" name="name" value="{{ $kategori['name'] }}" placeholder="Masukkan Nama Kategori" required>
                        </div>
                        
                        <div class="row">
                            <br>
                            <div class="col-lg-8 ml-auto">
                                <div class="row">
                                    <div class="col">
                                        <button class="btn-utama"> Simpan </button>
                                    </div>
                                    <div class="col">
                                        <a href="{{ route('diskon') }}"> <p class="btn-second"> Batal </p></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection