@extends('Management.Layouts.Main')






@section('container')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- /.content-header -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        @if(session('status'))
                        <h5 class="alert alert-warning mb-3">{{ session('status') }}</h5>
                        @endif
                    </div>


                </div>
            </div><!-- /.container-fluid -->
        </section>


        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <!-- SELECT2 EXAMPLE -->
                <div class="card card-default" style="font-family: 'Poppins' ">
                    <div class="card-header" style="display: flex; align-items: center;">
                        <a href="{{ route('transfer.stok') }}">
                            <svg style="margin-right: 10px;" xmlns="http://www.w3.org/2000/svg" width="24"
                                height="24" viewBox="0 0 1024 1024">
                                <path fill="currentColor"
                                    d="M685.248 104.704a64 64 0 0 1 0 90.496L368.448 512l316.8 316.8a64 64 0 0 1-90.496 90.496L232.704 557.248a64 64 0 0 1 0-90.496l362.048-362.048a64 64 0 0 1 90.496 0" />
                            </svg>
                        </a>
                        <div class="nav-text" style="margin-right: auto;">Transfer Stok</div>

                        

                        <!-- Modal navbar Kirim -->
                        <div class="modal fade" id="modal-kirim">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" style="font-weight: bold;">Kirim transfer stok melalui email</h5>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Dari</label>
                                            <input type="email" class="form-control" id="exampleInputEmail1"
                                                placeholder=" ">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Kepada</label>
                                            <input type="password" class="form-control" id="exampleInputPassword1"
                                                placeholder=" ">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Tembusan</label>
                                            <input type="email" class="form-control" id="exampleInputEmail1"
                                                placeholder=" ">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Perihal</label>
                                            <input type="password" class="form-control" id="exampleInputPassword1"
                                                placeholder=" ">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputEmail1">Pesan</label>
                                            <input type="email" class="form-control" id="exampleInputEmail1"
                                                placeholder=" ">
                                        </div>
                                        <div class="form-group">
                                            <label for="exampleInputPassword1">Lampiran</label>
                                            <input type="password" class="form-control" id="exampleInputPassword1"
                                                placeholder=" ">
                                        </div>
                                    </div>
                                    <div class="modal-footer justify-content-between">
                                        <button type="button" class="btn btn-default"
                                            data-dismiss="modal">Close</button>
                                        <button type="button" class="btn btn-primary">Kirim</button>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>

                        <!-- Modal navbar Diterima -->
                        <div class="modal fade" id="terimaModalCenter" tabindex="-1" role="dialog"
                            aria-labelledby="terimaModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="terimaModalLongTitle" style="font-weight: bold;">
                                            Terima transfer stok</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Apakah anda yakin menerima semua barang?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">BATAL</button>
                                        <button type="button" class="btn btn-primary">TERIMA</button>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>

                        <!-- Modal navbar Lainnya > Hapus -->
                        <div class="modal fade" id="hapusModalCenter" tabindex="-1" role="dialog"
                            aria-labelledby="hapusModalCenterTitle" aria-hidden="true">
                            <div class="modal-dialog modal-dialog-centered" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="terimaModalLongTitle" style="font-weight: bold;">
                                            Hapus rincian transfer stok</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        Apakah anda yakin ingin menghapus transfer stok?
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">TIDAK</button>
                                        <button type="button" class="btn btn-primary">YA</button>
                                    </div>
                                </div>
                                <!-- /.modal-content -->
                            </div>
                            <!-- /.modal-dialog -->
                        </div>
                    </div>
                    
                    <div class="card-body" style="margin-left: 33px; margin-right: 20px;">
                        <div class="row invoice-info">
                            <div class="col-sm-4 invoice-col">
                                
                                <div style="font-size: 30px; ">{{ $transfer['kodeBarang'] }}</div>
                                <small>Sedang transit</small><br><br>
                                <div style="font-weight: bold;">Tanggal : {{ $transfer['tanggal'] }}</div>
                                <div style="font-weight: bold;">Dipesan oleh : </div>
                                
                            </div>
                        </div><br>
                        <div class="row invoice-info">
                            
                            <div class="col-sm-4 invoice-col">
                                <div style="font-weight: bold;">Toko Asal : </div>
                                <address>
                                    {{ $transfer['tasal'] }}
                                </address>
                            </div>
                            
                            
                            <div class="col-sm-4 invoice-col" style="margin-left: 150px;">
                                <div style="font-weight: bold;">Toko Tujuan :</div>
                                <address>
                                    {{ $transfer['ttujuan'] }}<br>
                                </address>
                            </div>
                            
                        </div><br><br>
                        <div class="col invoice-info">
                            <div class="row-sm-4 invoice-col">
                                <h5 style="font-weight: bold;">Barang</h5>
                            </div>
                        </div>
                        <div class="container mt-4">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th scope="col" style="font-weight: normal;">Barang</th>
                                        <th scope="col" style="font-weight: normal;" class="text-right">Kuantitas</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <!-- Isi tabel (data) akan ditambahkan di sini -->
                                    {{-- rumus foreach  --}}
                                    <tr>
                                        
                                        <td>{{ $transfer['namaBarang'] }}
                                            <br>
                                            <small>{{ $transfer['kodeBarang'] }}</small>
                                        </td>

                                        <td class="text-right">{{ $transfer['asalStok'] }}</td>
                                    </tr>
                                    <tr>
                                        <td>{{ $transfer['namaBarang'] }}</td>
                                        <td class="text-right">{{ $transfer['kuantitas'] }}</td>
                                        
                                    </tr>
                                    <!-- Tambahkan baris baru untuk setiap barang -->
                                </tbody>
                            </table>
                        </div>
                        

                    </div>
                    
                </div>
                <!-- /.row (main row) -->
            </div><!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
@endsection



@section('script')
@endsection
