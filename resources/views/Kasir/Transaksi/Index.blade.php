@extends('Kasir.Layouts.Main')



@section('container')

<div class="content-wrapper">
<div class="container-fluid">
 <!-- Tabel Karyawan -->
        <section class="content">

                   <!-- Tabel Riwayat penjualan -->

        <div class="row">
            <section class="col-6">
                
                
                
                <br>
                <h1>Menu</h1>
                <div>
                    @foreach($products as $index => $product)
                    <div class="card" id="card{{ $index }}" onClick="addProduct('{{ $product['name'] }}', '{{ $index }}', {{ $product['price'] }})">
                        <div class="row align-items-center">
                        <div class="col-2">
                        @if (isset($product['foto']))
                            <img src="{{ asset('img/' . $product['foto']) }}" height="55px" alt="">
                        @else
                            {{-- <img src="{{ asset('img/Dasboard.png') }}" height="55px" alt=""> --}}
                        @endif
                                                </div>
                        <div class="col-6  text-left">
                            <p>{{ $product['name'] }}</p>
                        </div>
                        <div class="col-4 text-left">
                            <p>{{ $product['price'] }}</p>
                        </div>

                        </div>
                    </div>
                    @endforeach
                </div>
                         
                    
            </section>
            <section class="col-6">
                <br>
                <form action="{{ route('kasir.store') }}" method="POST" enctype="multipart/form-data">
                    @csrf
                <div class="card">
                    <div class="card-header">
                        <div class="row">
                            <div class="col">

                                <h1>Pesanan</h1>
                            </div>
                            <div class="col">
                                <div class="form-floating mb-3 row">
                                    <input type="text" class="form-control" id="name" name="namaPelanggan" placeholder="Masukkan Nama Pelanggan">
                                </div>  
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-4">
                                Nama Barang
                            </div>
                            <div class="col-2">
                                Kuantitas
                            </div>
                            <div class="col-3">
                                harga Satuan
                            </div>
                            <div class="col-3">
                                harga
                            </div>
    
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="app">

                        </div>
                        
                </div>
                <div class="card-footer">
                        
                        
                        <div class="row">

                            <div class="form-group col-3">
                                        <select id="inputState" class="form-control" onChange="diskonChange()">
                                          <option value="0" data-name="0" data-nilai="0" data-type="0" selected>discount</option>
                                          @foreach($discounts as $key => $discount)
                                          <option value="{{ $key }}" data-name="{{ $discount['name'] }}" data-nilai="{{ $discount['nilai'] }}" data-type="{{ $discount['type'] }}">{{ $discount['name'] }}</option>
                                          @endforeach
                                        </select>
                            </div>
                           <div class="col-3"></div>
                            <div class="col-3 diskon">
                                
                            </div>
                            <div class="col-3  text-left">
                                <p class="diskontotal"></p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-9">
                                <p>Total</p>
                            </div>
                            <div class="col-3">
                                <p class="total"></p>
                            </div>
                            <br>
                        </div>
                        
                        <hr>
                        <br>
                        <br>
 
                        <div class="row items-align-center">
                            <span class="action-button"></span>
                            <div class="col text-center">
                                <button class="btn-second" onclick="actionButton('simpan')">
                                    Simpan
                                </button>
                            </div>
                            <div class="col text-center">
                                <button class="btn-utama" onclick="actionButton('bayar')">
                                    Bayar
                                </button>

                            </div>
                            <br>
                            
                            
                        </div>
                        <br>
                        <br>
                        
                </div>
                    
                </div>
                </form>
            </section>
        </div>      
        </section>
<!-- end tabel riwayat penjualan -->
    </div>
</div>

@endsection

@section('script')
    
    <script>
      
    const arr = @json($products);
    const abb = @json($categories);
    var jak = @json($pajak);
    var pajak = Object.values(jak);
    var $listPajak = document.querySelector('.pajak')

    var $app = document.querySelector('.app');
    var $orderList = document.querySelector('.orderlist');
    var total = 0;
    var products = Object.values(arr);
    var categories = Object.values(abb);
    const key = Object.keys(arr);
    diskonChange(); // Memanggil diskonChange() sebelum mengambil nilai subtotal

    function diskonChange(){
        let values = takeValueDiscount();
        let name = values.name;
        let nilai = values.nilai;
        let type = values.type;
        renderDiscount(name, nilai, type)
        addDiskonValue();
          
    }
    function takeValueDiscount(){
    let select = document.getElementById('inputState');
    let selectedOption = select.options[select.selectedIndex];
    let name = selectedOption.getAttribute('data-name');
    let nilai = parseInt(selectedOption.getAttribute('data-nilai'));
    let type = selectedOption.getAttribute('data-type');
    return { name: name, nilai: nilai, type: type };
}

    function renderDiscount(name, nilai, type){
        let diskonDiv = document.querySelector('.diskon')
        let diskonHtml = document.createElement('div');
        diskonHtml.classList.add('row');
                diskonHtml.innerHTML = `
                <br>
                    <input type="hidden" name="discountname[]" value="${name}">

                    <div class="col-6 text-left">
                        ${nilai}
                </div>
                    <input type="hidden" name="discountvalue[]" value="${nilai}">
                    <input type="hidden" name="discounttype[]" value="${type}">

                
                                            `;
                
        diskonDiv.innerHTML = "";
        diskonDiv.appendChild(diskonHtml);  
    }

    function addDiskonValue(){
        let discountValueNominal = getDiskonValue();
        renderDiskonValue(discountValueNominal);
        addTotal();
        
    }
    function getDiskonValue(){
        let values = takeValueDiscount();
        let name = values.name;
        let nilai = values.nilai;
        let type = values.type;
        let discountValueNominal = 0;
        if(type == 'Persent'){
            let nilaiSubtotal = allSubtotal();
            discountValueNominal = nilaiSubtotal * nilai / 100;
        }else{
            discountValueNominal = nilai;
        }
        return  discountValueNominal;

    }
    function renderDiskonValue(discountValueNominal){
        let diskonTotal = document.querySelector('.diskontotal');

        diskonTotal.innerHTML = `${ currency(discountValueNominal)} <input type="hidden" name="discountnominal[]" value="${discountValueNominal}">`;

    }

function addProduct(name, index, price, quantity = 1){


     renderProduct(name, index, price, quantity);
    subtotal(index, price, quantity);
    offAtributOnclick(index);    
}
function offAtributOnclick( index){
var card = document.querySelector(`#card${index}`);
card.setAttribute('onClick', null);
card.style.cursor = 'not-allowed';

}
function renderProduct(name, index, price, quantity){
var $app = document.querySelector('.app');

var $product = document.createElement('div');
$product.innerHTML = `
<div class="row">
    <div class="col-4 text-left">
        <p>${name}</p>
        <input type="hidden" name="productname[]" value="${name}">
        <input type="hidden" name="key[]" value="${index}">

            </div>
            <div class="col-2">
                <input type="number" name="quantity[]" style="width:45px;" min="1" value="1" onChange="subtotal('${index}', ${price}, this.value)">
            </div>
            <div class="col-3 text-left">
                <p>${price}</p>
                <input type="hidden" name="productprice[]" value="${price}">
                </div>
                <div class="col-3 text-left">
                    <p class="subtotal${index}"></p>
                    </div>
                    </div>
                    <hr>
 `;
 $app.append($product);
 return $product;

}
function addTotal(){
    let diskonValueNominal = getDiskonValue();
    let subtotal = allSubtotal();
    let total = subtotal-diskonValueNominal;
    renderTotal(total, subtotal);
    console.log(total)
}
function renderTotal(total, subtotal){
    var $totalBayar = document.querySelector('.total');
    $totalBayar.innerHTML = `${currency(total)}<input type="hidden" name="totalBayar" value="${total}"><input type="hidden" name="grandTotal" value="${subtotal}">`;
}
function subtotal(index, price, quantity){
console.log(quantity);
var subtotalPrice = document.querySelector(`.subtotal${index}`);

let subtotal = price * quantity;
subtotalPrice.innerHTML = `${currency(subtotal)}<input type="hidden" name="productsubtotal[]" value="${subtotal}">`
allSubtotal();
addDiskonValue();
addTotal();
}
function allSubtotal(){
var allSubtotals = document.querySelectorAll('[name^="productsubtotal[]"]');
var total = 0;
allSubtotals.forEach(subtotal => {
    total += parseFloat(subtotal.value);
});

return total;
}
 
  function actionButton(value){
    var $actionButton = document.querySelector('.action-button');

    if(value == "simpan"){
        $actionButton.innerHTML = `<input type="hidden" name="Status" value="Simpan"> `;

    }else{
        $actionButton.innerHTML = `<input type="hidden" name="Status" value="Bayar">`;
    }
  }
    
   



// Fungsi penunjang

function hasClass(element, product, className) {
    

    return element.classList.contains(className);
}

 
function addClass(element, product, className, key, index, tax, indexstax) {
    
    if (!element.classList.contains('is-active')) {
        element.classList.add(className);
        updateTotal(1, product.price, index, tax, indexstax);

        
        var taxs = Object.values(tax);
        var $product = document.createElement('div');
        $product.innerHTML = `
            <div class="row">
                <div class="col-4 text-left">
                    <p>${product.name}</p>
                    <input type="hidden" name="productname[]" value="${product.name}">
                    <input type="hidden" name="key[]" value="${key[index]}">

                </div>
                <div class="col-2">
                    <input type="number" name="quantity[]" style="width:45px;" value="1" onchange="updateTotal(this, ${product.price}, ${index}, 0 , ${indexstax},  '${tax.type}', ${tax.nilai})">
                </div>
                <div class="col-3 text-left">
                    <p>${product.price}</p>
                    <input type="hidden" name="productprice[]" value="${product.price}">
                </div>
                <div class="col-3 text-left">
                    <p class="subtotal${index}"></p>
                </div>
            </div>
            <hr>
        `;
        $app.appendChild($product);
        var $total = document.querySelector('.subtotal'+index);
        $total.innerHTML = `${currency(sub)}<input type="hidden" name="productsubtotal[]" value="${sub}">`
;
        

    }
    element.classList.add(className);


}




// Assuming you have a currency function defined
function currency(value) {
    return value.toFixed(2);
}

function removeClass(element, className) {
    element.classList.remove(className);
}



function currency(value) {
    // Anda mungkin ingin memformat nilai mata uang sesuai kebutuhan Anda di sini
    return value.toFixed(2);
}

     
    </script>
    <script>

        
        

        
    </script>
    

@endsection 

