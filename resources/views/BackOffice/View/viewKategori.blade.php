@extends('BackOffice.Layouts.Main')

@section('container')

<div class="content-wrapper" style="font-family: Poppins;">
    <div class="container-fluid">
        <section class="content">
            <div class="row">
                <div class="col">
                    <br>
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">
                                <h2 style="font-weight:bold; ">Data Kategori</h2>
                            </div>
                        </div>
                        <div class="card-body">
                            <table id="tabelKategoriBackoffice" class="table table-striped" style="width:100%">
                            <thead>
                                <tr>
                                    <th>Nama</th>
                                    <th>Banyak</th>
                                    <th>Total Penjualan</th>
                                </tr>
                            </thead>
                            <tbody>
                                @if(isset($categories))
                                @foreach($categories as $item => $category)
                                <tr>
                                        <td>{{ $category['name'] }}</td>
                                        <td>{{ count(array_filter($products, function($product) use ($category) {
                                            return $product['category'] == $category['name'];
                                        })) }}</td>
                                         @php
                                         $totalNominal = 0;
                                         foreach($transaksi as $index => $list){
                                             if($list['status'] ){
                                                 foreach ($list['products'] as $key => $produk) {
                                                     if ($produk['category'] == $category['name']) {
                                                         $totalNominal += $produk['subtotal'];
                                                     }
                                                 }
                                             }}
                                             
                                             
                                     @endphp
                                     <td>{{ $totalNominal }}</td>
                                </tr>
                                @endforeach
                                @endif

                            </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

@endsection


@section('script')
<script>
    $(function () {
      $("#tabelKategoriBackoffice").DataTable({
        "responsive": true, "lengthChange": false, "autoWidth": false,
        "buttons": [ "colvis"]
      }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
      
    });
   
  </script>
@endsection