<aside class="main-sidebar sidebar-light-primary elevation-4">
    <!-- Brand Logo -->
    <a class="brand-link text-center">
      <span class="brand-text font-weight-light poppins"> Management</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          
          <li class="nav-item">
            <a href="{{ route('produk')}}" id="karyawan" class="nav-link">
              <p>
                Produk
              </p>
            </a>
          </li>                   
          <li class="nav-item">
            <a href="{{ route('diskon')}}" id="diskon" class="nav-link">
              <p>
                Diskon
              </p>
            </a>
          </li>                   
          <li class="nav-item">
            <a href="{{ route('kategori')}}" id="karyawan" class="nav-link">
              <p>
                Kategori
              </p>
            </a>
          </li>                   
          <li class="nav-item">
            <a href="{{ route('pajak')}}" id="karyawan" class="nav-link">
              <p>
                Pajak
              </p>
            </a>
          </li>                                     
          <li class="nav-item">
            <a href="{{ route('transfer.stok')}}" id="karyawan" class="nav-link">
              <p>
                Transfer Stok
              </p>
            </a>
          </li>                                                    
          <li class="nav-item">
            <a href="{{ route('shift.master')}}" id="karyawan" class="nav-link">
              <p>
                Shift Master
              </p>
            </a>
          </li>                                                    
          <li class="nav-item">
            <a href="{{ route('management.karyawan')}}" id="karyawan" class="nav-link">
              <p>
                Managemen Karyawan
              </p>
            </a>
          </li>
          <li class="nav-item menu-open">
            <a href="#" id="laporan" class="nav-link">
              <i class="fa-solid fa-chart-column"></i>              <p>
                Laporan
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="/ringkasan-penjualan-management" id="ringkasanPenjualan" class="nav-link">
                  <p>Ringkasan Penjualan</p>
                </a>
              </li>
              <li class="nav-item">
              <a href="{{ route('laporan.barang.management')}}" id="laporanBarang" class="nav-link">
                  <p>Penjualan Berdasarkan Barang</p>
                </a>
              </li>
              <li class="nav-item">
              <a href="{{ route('laporan.kategori.management')}}" id="laporanKategori" class="nav-link">
                  <p>Penjualan Berdasarkan Kategori</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('laporan.jenis.pembayaran.management')}}" id="laporanJenisPembayaran" class="nav-link">
                  <p>Penjualan Berdasarkan Jenis Pembayaran</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('laporan.diskon.management')}}" id="diskon" class="nav-link">
                  <p>Diskon</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('laporan.pajak.management')}}" id="pajak" class="nav-link">
                  <p>Pajak</p>
                </a>
              </li>
              <li class="nav-item">
                <a href="{{ route('laporan.shift.management')}}" id="shift" class="nav-link">
                  <p>Shift</p>
                </a>
              </li>
            </ul>
          </li>                   
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
